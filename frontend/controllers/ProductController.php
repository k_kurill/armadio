<?php
namespace frontend\controllers;

use frontend\models\Product;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

class ProductController extends Controller{
    
    public function actionIndex(){
        $products = Product::findAll(['active'=>1]);
        return $this->render('index',['products'=>$products]);
    }
    public function actionView($id){
        $model = $this->findModel($id);
        if(\Yii::$app->request->isAjax){
            return $this->renderAjax('view',['model'=>$model]);
        }
        return $this->render('view',['model'=>$model]);
    }
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}