<?php
namespace frontend\controllers;

use frontend\models\Product;
use Yii;

class CartController extends \yii\web\Controller {

    public function actionIndex() {
        if(Yii::$app->cart->getIsEmpty()){
            return $this->render('emptycart');
        }
        return $this->render('index');
    }

    public function actionUpdateItem($id, $qty) {
        $product = Yii::$app->cart->getPositionById($id);
        Yii::$app->cart->update($product,$qty);
        if(Yii::$app->request->isAjax){
            return ;
        }
        
        return $this->redirect(['index']);
    }

    public function actionAddItem($id, $size=null) {
        $product = Product::find()->andWhere(['id'=>$id])->one();
        $product->sizeId = $size;
        // add an item to the cart
        Yii::$app->cart->put($product);
    }

    public function actionClear() {
        Yii::$app->cart->removeAll();
    }

    public function actionDeleteItem($id) {
        Yii::$app->cart->removeById($id);
        if(Yii::$app->request->isAjax){
            return ;
        }
        $this->redirect(['index']);
    }

}
