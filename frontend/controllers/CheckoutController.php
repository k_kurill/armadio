<?php

namespace frontend\controllers;

use common\models\OrderItem;
use common\models\Shipping;
use common\models\Order;
use common\models\Billing;
use \Braintree_Result_Successful;


use Yii;

class CheckoutController extends \yii\web\Controller
{
    public function actionBilling(){  
        $billing = new Billing;
        $shipping = Yii::$app->session->get('shipping');
        $billing->zip = $shipping->zip;
        $billing->address = $shipping->address;
        $billing->address2 = $shipping->address2;
        $billing->city = $shipping->city;
        $billing->state = $shipping->state;
        //Yii::$app->session->set('billing', null);
        $saved = Yii::$app->session->get('billing');
        if($saved instanceof Billing){
            $billing = $saved;
        }
        if($billing->load(Yii::$app->request->post()) && $billing->validate()){
            Yii::$app->session->set('billing', $billing);
            return $this->redirect(['review']);
        }
        return $this->render('billing', ['model'=>$billing]);
    }
    
    public function actionReview(){    
        $shipping = Yii::$app->session->get('shipping');
        $billing  = Yii::$app->session->get('billing');
        return $this->render('review', ['shipping'=>$shipping, 'billing'=>$billing]);
    }
    
    
    

    public function actionConfirm(){
        $order = Yii::$app->session->get('order');
        $shipping = Yii::$app->session->get('shipping');
        $billing = Yii::$app->session->get('billing');
        if(!$order || !$shipping){
            return $this->redirect(['cart/index']);
        }
        if($order->save()){
            $this->linkItemsToOrder($order);
            
            $shipping->order_id = $order->id;
            if(!$shipping->save()){
                Yii::$app->session->set('shipping', $shipping);
                return $this->redirect(['shipping']);
            }
            
                $result = $this->pay($order, $billing);
                if($result instanceof Braintree_Result_Successful){
                    //Yii::$app->errorHandler->clearOutput();
                    return $this->redirect(\yii\helpers\Url::to(['/checkout/success']));
                    //Yii::$app->errorHandler->clearOutput();
                } else {
                    Yii::$app->session->set('result', $result);
                    return $this->redirect(\yii\helpers\Url::to(['/checkout/fail']));
                }
            
        }
        
        return $this->render('confirm');
       
    }
    
    private function pay($order, $billing){
        //Yii::$app->session->set('result',null);
        $braintree = Yii::$app->braintree;

        $customer = $braintree->call('Customer', 'create', [
            'firstName' => $billing->first_name,
            'lastName' => $billing->last_name,
        ]);
        $response = $braintree->call('Transaction', 'sale', [
            'amount' => $order->getTotalPrice(),
            'orderId' => $order->id,
            'merchantAccountId' => 'ARMADIO_instant',
            'customerId' => $customer->customer->id,
            'creditCard'=>[
                'cardholderName' => $billing->first_name . ' ' . $billing->last_name,
                'cvv' => $billing->cvv,
                'expirationMonth' => $billing->month,
                'expirationYear' => $billing->year,
                'number' => $billing->card_num,
            ],
            //'customFields'=>'order_id'
        ]);
        $billing->order_id = $order->id;
        $billing->responce = $response;
        $billing->save(false);
        return $response;
        
    }


    public function actionFail(){
        $result = Yii::$app->session->get('result');
        return $this->render('fail', ['result'=>$result]);
    }
    public function actionSuccess(){
        Yii::$app->errorHandler->clearOutput();
        
        Yii::$app->session->set('billing', null);
        Yii::$app->session->set('order', null);
        Yii::$app->cart->removeAll();
        Yii::$app->errorHandler->clearOutput();
        $mail = new \frontend\components\MailHelper();
        $shipping = Yii::$app->session->get('shipping');
        $mail->successfulBuy($shipping->email);
        //var_dump($shipping->email);
        Yii::$app->session->set('shipping', null);
        return $this->render('success');
    }
    
    public function actionOrder(){
        if(!Yii::$app->cart->isEmpty){
            $this->makeOrder();
            return $this->redirect(['shipping']);
        }else{
            return $this->redirect(['cart/index']);
        }
    }

    public function actionShipping(){
        $shipping = new Shipping();
        $saved = Yii::$app->session->get('shipping');
        if($saved instanceof Shipping){
            $shipping = $saved;
        }
        $shipping->user_id = 0;
        if(!Yii::$app->user->isGuest && Yii::$app->user->identity->profile && !$saved){
            $profile = Yii::$app->user->identity->profile;
            $shipping->name     = $profile->name;
            $shipping->user_id  = Yii::$app->user->id;
            $shipping->email    = Yii::$app->user->identity->email;
            $shipping->address  = $profile->address;
            $shipping->state    = $profile->state;
            $shipping->zip      = $profile->zip;
        }
        if($shipping->load(Yii::$app->request->post()) && $shipping->validate()){
            Yii::$app->session->set('shipping', $shipping);
            $this->redirect(['billing']);
        }
        return $this->render('shipping', ['model'=>$shipping]);
    }
    
    protected function makeOrder(){
        $order = new Order();
        $order->user_id = (Yii::$app->user->isGuest) ? '0' : Yii::$app->user->id;
        $order->created = new \yii\db\Expression('NOW()');
        $order->status = Order::ON_HOLD;
        Yii::$app->session->set('order', $order);
        return $order->id;
    }
    protected function linkItemsToOrder(Order $order){
        $products = Yii::$app->cart->getPositions();
        $this->unlinkItems($order);
        foreach ($products as $product) {
            $orderItem = new OrderItem();
            $orderItem->order_id = $order->id;
            $orderItem->product_id = $product->id;
            $orderItem->name = $product->name;
            $orderItem->price = $product->getPrice();
            $orderItem->qty = $product->getQuantity();
            $orderItem->size = $product->getSize();
            $orderItem->save();
        }
    }
    protected function unlinkItems(Order $order){
        OrderItem::deleteAll(['order_id'=>$order->id]);
    }

}
