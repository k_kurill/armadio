<?php
namespace frontend\controllers;

use common\modules\shop\models\Brands;
use common\modules\shop\models\BrandsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class ArtisanController extends Controller
{
    public function actionIndex()
    {   
        $searchModel = new BrandsSearch;
        $dataProvider = $searchModel->search([]);
        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    public function actionProfile($id = null)
    {
       
        if($model = Brands::find()->andWhere(['id' => $id])->one())
            return $this->render('profile', ['model' => $model]);
       throw new NotFoundHttpException('The requested page does not exist.');
    }
    public function actionSend(){
        $mail = new \frontend\components\MailHelper();
        var_dump($mail->successfulBuy('kindras1@gmail.com'));
    }
}
