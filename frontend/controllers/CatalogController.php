<?php
namespace frontend\controllers;

use frontend\models\ProductSearch;

use frontend\models\Category;
use yii\web\Controller;
use Yii;
use yii\web\NotFoundHttpException;
class CatalogController extends Controller{
    public function actionIndex(){
      return $this->render('index');
    }
    public function actionView($id){
        $searchModel = new ProductSearch();
       
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);
        $category = $this->findModel($id);
        return $this->render('view',[
            'dataProvider'=>$dataProvider,
            'searchModel'=>$searchModel,
            'category'=>$category,
        ]);
    }
    
    
    protected function findModel($id) {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

