<?php
namespace frontend\components;
use common\modules\Mailjet\Client;
use common\modules\Mailjet\Resources;
use dektrium\user\Mailer;
use yii\debug\models\search\Mail;
use Yii;
class MailHelper{
    private $client;
    private $email;
    function __construct(){
        $this->client = new Client(\Yii::$app->components['mailjet']['API_KEY'],\Yii::$app->components['mailjet']['SECRET_KEY']);
        $this->email = Yii::$app->components['mailjet']['email'];
    }

	function successfulBuy($subject,$values=null){  
        $response = $this->client->get(Resources::$TemplateDetailcontent,['id'=>Yii::$app->components['mailjet']['config_pay_template_id']]);  
       
        if($response->success()){
            $header = json_decode($response->getData()[0]['Headers']);           
            $body = [
                'FromEmail' =>  $header->SenderEmail,   
                'FromName' =>   $header->SenderName,   
                "Subject" =>  $header->Subject,
                "MJ-TemplateID"=> Yii::$app->components['mailjet']['config_pay_template_id'],
                "MJ-TemplateLanguage"=> true,
                'Recipients' => [['Email' => $subject]]
            ];
            if($values && is_array($values))
                $body['Vars']=$values;
            
          
            $response = $this->client->post(Resources::$Email, ['body' => $body]);
       }
        return $response->success();

	} 
    function addNewsletter ($subject){
    $body = [ 'Email' => $subject];
    $response = $this->client->post(Resources::$Contact, ['body' => $body]);     
    
    if($response->success()) { 
        $body = [    
            'ContactsLists' => [
                [
                    'ListID' =>Yii::$app->components['mailjet']['newsletter_list_id'],
                    'Action' => "addnoforce"
                ],
            ]
        ];
    
        $response = $this->client->post(Resources::$ContactManagecontactslists, ['id' => $response->getData()[0]['ID'], 'body' => $body]);
    }
    return $response->success();
    }
    function getNewsletter (){

    }
    function getStatistic(){
        $response = $this->client->get(Resources::$Contactstatistics);
        $response->success() && var_dump($response->getData());
    }

}