function add2cart(itemId, sizeId){
    $.ajax({
        type: "GET",
        url: add2cartUrl,
        data: { id: itemId, size: sizeId},
        success: function(data){
            console.log('item ' + itemId + ' added to cart');
            var count = parseInt($('#cart-count-items span').html().replace(/\D+/g,""));
            if(isNaN(count)){
                console.log(count);
                count = '(1)';
            }else{
                console.log(count);
                count+=1;
                count = '('+count+')';
            }
            console.log(count);
            $('#cart-count-items>span').html(count);
            $('#modalAddToCart').modal('show');
        },
        dataType: 'html'
      });
}
$(document).on('click','.size-item',function(e){
        e.preventDefault();
        $('.buy-warning').hide();
        if($(this).hasClass('inactive')){
            return false;
        }
        $(this).siblings('li').removeClass('active-size');
        $(this).addClass('active-size');
        $('.add-cart-button').attr('data-size',$(this).data('size'));
    });
$(document).on('click','.add-cart-button', function(e){
        e.preventDefault();
        if($(this).hasClass('solded')){
            return false;
        }
        if($('.size-item').size()>0 && $('.active-size').size() == 0){
            $('.buy-warning').show();
            return false;
        }
        var size = $('.size-item.active-size').data('size');
        add2cart($(this).data('id'), size);
    });
$(document).ready(function(){
    
});
