<?php
namespace frontend\widgets;

use yii\base\Widget;
use yii\data\ArrayDataProvider;

/**
 * Class Cart
 * @package yii2mod\cart\widgets
 */
class CartGrid extends Widget
{

    /**
     * @var
     */
    public $items;
    
    public $itemsProvider;


    /**
     * Setting defaults
     */
    public function init()
    {
        $cart = \Yii::$app->get('cart');
        $this->items = $cart->getPositions();
        $this->itemsProvider = $this->getArrayProvider();
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('cart', [
            'itemsProvider'=>$this->itemsProvider,
        ]);
    }
    
    public function getArrayProvider() {
        return new ArrayDataProvider([
            'allModels' => $this->items,
        ]);
    }

    
}
