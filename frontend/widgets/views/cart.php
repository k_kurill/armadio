<?php 
use yii\widgets\ListView;
?>

<?php \yii\widgets\Pjax::begin(['timeout' => 3000, 'id' => 'pjax-cart-container']); ?>

<?= ListView::widget([
    'layout' => "{items}",
    'dataProvider'=>$itemsProvider,
    'itemView' => '_cartItem'
]);
?>

<?php \yii\widgets\Pjax::end(); ?>