<?php
use yii\bootstrap\Html;
use yii\helpers\Url;
?>

<?php
$deleteLnkText = Html::tag('i', '', ['class'=>'fa fa-remove',]);
?>
<div class="row cart-item">
    <div class="col-md-2 col-sm-2 cart-item-image">
        <?= Html::img($model->getImage()->getUrl('150x'));?>
    </div>
    <div class="col-md-10 col-sm-10 cart-item-properties">
        <div class="row">
            <div class="col-md-4 col-sm-4 cart-item-name">
                <p><?= $model->name;?></p>
                <p><?= ($model->brand)?$model->brand->name:'';?></p>
            </div>
            <div class="col-md-2 col-sm-2 cart-item-color">
                <p><?=$model->color_title?></p>
            </div>
            <div class="col-md-1 col-sm-1 cart-item-size">
                <p><?=$model->size?></p>
            </div>
            <div class="col-md-1 col-sm-1 cart-item-qty">
               <?=Html::input('text', 'count', $model->getQuantity(), ['data-id'=>$model->getId(), 'onchange'=>"changeItemCount('{$model->getId()}', this.value)"])?>
            </div>
            <div class="col-md-2 col-sm-2 cart-item-actions">
                <a data-pjax="1" href="<?=Url::to(['/cart'])?>">Update</a>
                <?=Html::a(Yii::t('cart','Remove'),
                        Url::to(['cart/delete-item','id'=>$model->getId()]),['data-id'=>$model->getId(), 'class'=>'cart-item-delete']
                )?>
                <a style="display: none" href="#"><?=Yii::t('cart','Move to Favorites');?></a>
            </div>
            <div class="col-md-2 col-sm-2 cart-item-price">    
              <p><?=Yii::$app->formatter->asCurrency($model->price * $model->getQuantity() )?></p>
            </div>
        </div>
    </div>
</div>
