<?php
namespace frontend\models;

use yii\data\ActiveDataProvider;


class ProductSearch extends Product{
    
    public function search($params, $categoryId){
        
        $this->load($params);
        
        $query = Product::find();
        
        $query->joinWith('productCategories');
        $query->groupBy('product.id');
        $query->andWhere(['product_category.category_id'=>$categoryId]);
        $query->andWhere(['variant_of'=>null]);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
       return $dataProvider;
    }
}