<?php

namespace frontend\models;

use lajax\translatemanager\models\Language;
use common\modules\shop\models\ProductCategory;

/**
 * This is the model class for table "product_i18n_data".
 *
 * @property string $id
 * @property integer $product_id
 * @property string $language_id
 * @property string $name
 * @property string $meta_title
 * @property string $meta_keyword
 * @property string $meta_description
 * @property string $short_description
 * @property string $description
 *
 * @property Product $product
 * @property Language $language
 */
class ProductI18nData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_i18n_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'language_id', 'name', 'meta_title', 'meta_keyword', 'meta_description', 'short_description', 'description'], 'required'],
            [['product_id'], 'integer'],
            [['description'], 'string'],
            [['language_id'], 'string', 'max' => 5],
            [['name', 'meta_title', 'meta_keyword', 'meta_description', 'short_description', 'color_title'], 'string', 'max' => 255],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'language_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'language_id' => 'Language ID',
            'name' => 'Name',
            'meta_title' => 'Meta Title',
            'meta_keyword' => 'Meta Keyword',
            'meta_description' => 'Meta Description',
            'short_description' => 'Short Description',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::className(), ['language_id' => 'language_id']);
    }

    /**
     * @inheritdoc
     * @return ProductI18nDataQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductI18nDataQuery(get_called_class());
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategories()
    {
        return $this->hasMany(ProductCategory::className(), ['product_id' => 'product_id']);
    }
    
}
