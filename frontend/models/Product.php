<?php

namespace frontend\models;

use yz\shoppingcart\CartPositionInterface;
use Yii;
use rico\yii2images\models\Image;
use frontend\models\ProductI18nData;
use backend\models\ProductSize;
use common\modules\shop\models\ProductCategory;
use common\modules\shop\models\Brands;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property integer $active
 * @property integer $variant_of
 * @property string $sku_code
 * @property string $price
 * @property string $name
 * @property string $language_id
 * @property string $published_at
 * @property string $meta_title
 * @property string $meta_keyword
 * @property string $meta_description
 * @property string $short_description
 * @property string $description
 *
 * @property ProductCategory[] $productCategories
 */
class Product extends \yii\db\ActiveRecord implements CartPositionInterface
{
    public $name, $meta_title, $meta_keyword, $meta_description, $short_description, $description, $color_title;
    
    private $_sizeId;
    use \yz\shoppingcart\CartPositionTrait;
    /**
     * @inheritdoc
     */
    
    /**
     * create_time, update_time to now()
     * crate_user_id, update_user_id to current login user id
     */
    public function behaviors() {
        return [           
            'image' => [
                'class' => \rico\yii2images\behaviors\ImageBehave::className(),
            ],
        ];
    }
    
    public function getSolded(){
        return (bool)$this->count;
    }


    public function afterFind() {
        $this->name = $this->data->name;
        $this->meta_title = $this->data->meta_title;
        $this->meta_keyword = $this->data->meta_keyword;
        $this->meta_description = $this->data->meta_description;
        $this->short_description = $this->data->short_description;
        $this->short_description = $this->data->short_description;
        $this->description = $this->data->description;
        $this->color_title = $this->data->color_title;
        parent::afterFind();
    }
    public function getData($lng=null){
        return $this->hasOne(ProductI18nData::className(), ['product_id' => 'id'])->andFilterWhere(['language_id'=>$lng]);
    }


    public static function tableName()
    {
        return 'product';
    }
    
    public function getSizeId(){
        return $this->_sizeId;
    }
    public function setSizeId($id){
        $this->_sizeId = $id;
    }
    
    public function getSize(){
        $size = ProductSize::find()->andFilterWhere(['id' => $this->_sizeId, 'product_id' => $this->id])->one();
        if(!$size)
            return '';
        else return $size->size;
    }
    
    
    public function getPrice()
    {
        return $this->price;
    }

    public function getId()
    {
        return $this->id.'_'.$this->sizeId;
    }
    
    public function getImages()
    {
        $finder = [
            'itemId' => $this->id,
            'modelName' => 'Product',
        ];
        $imageQuery = Image::find();
        $imageQuery->where($finder);
        $imageQuery->orderBy(['isMain' => SORT_DESC, 'id' => SORT_ASC]);

        $imageRecords = $imageQuery->all();
        
        return $imageRecords;
    }
    public function getSliderImages()
    {
        $finder = [
            'itemId' => $this->id,
            'modelName' => 'Product',
        ];
        $imageQuery = Image::find();
        $imageQuery->where($finder);
        $imageQuery->andWhere(['role'=>null]);
        $imageQuery->orderBy(['isMain' => SORT_DESC, 'id' => SORT_ASC]);

        $imageRecords = $imageQuery->all();
        
        return $imageRecords;
    }
    
    public function getLeather(){
        if($this->isNewRecord){
            return $this->_leather;
        }
        if(!$this->data){
            $this->createI18nRecord();            
        }
        return $this->data->leather;
    }
    
    public function getMeasurements(){
        if($this->isNewRecord){
            return $this->_measurements;
        }
        if(!$this->data){
            $this->createI18nRecord();            
        }
        return $this->data->measurements;
    }
    
    public function getImage()
    {
        $finder = [
            'itemId' => $this->id,
            'modelName' => 'Product',
            'role'=>'main',
        ];
        $imageQuery = Image::find();
        $imageQuery->where($finder);
        $imageQuery->orderBy(['isMain' => SORT_DESC, 'id' => SORT_ASC]);

        $img = $imageQuery->one();
        if(!$img){
            return Yii::$app->getModule('yii2images')->getPlaceHolder();
        }
        return $img;
    }
    
    public function getImageByRole($role){
        
        $finder = [
            'itemId' => $this->id,
            'modelName' => 'Product',
            'role'=>$role,
        ];
        $imageQuery = Image::find();
        $imageQuery->where($finder);

        $img = $imageQuery->one();
        if(!$img){
            return Yii::$app->getModule('yii2images')->getPlaceHolder();
        }
        return $img;
    }
    
    public function getSizesInCart(){
        $items = [];
        foreach($this->sizes as $size){
            $this->_sizeId = $size->id;
            if(Yii::$app->cart->hasPosition($this->getId())){
                $items[] = $this->getId();
            }
        }
        return $items;
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['active', 'variant_of'], 'integer'],
            [['sku_code', 'price', 'name', 'language_id', 'meta_title', 'meta_keyword', 'meta_description', 'short_description', 'description'], 'required'],
            [['price'], 'number'],
            [['published_at'], 'safe'],
            [['description'], 'string'],
            [['sku_code', 'name', 'meta_title', 'meta_keyword', 'meta_description'], 'string', 'max' => 255],
            [['language_id'], 'string', 'max' => 5],
            [['short_description'], 'string', 'max' => 511],
            [['sku_code', 'language_id'], 'unique', 'targetAttribute' => ['sku_code', 'language_id'], 'message' => 'The combination of Sku Code and Language ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('product', 'ID'),
            'active' => Yii::t('product', 'Active'),
            'variant_of' => Yii::t('product', 'Variant Of'),
            'sku_code' => Yii::t('product', 'Sku Code'),
            'price' => Yii::t('product', 'Price'),
            'name' => Yii::t('product', 'Name'),
            'language_id' => Yii::t('product', 'Language ID'),
            'published_at' => Yii::t('product', 'Published At'),
            'meta_title' => Yii::t('product', 'Meta Title'),
            'meta_keyword' => Yii::t('product', 'Meta Keyword'),
            'meta_description' => Yii::t('product', 'Meta Description'),
            'short_description' => Yii::t('product', 'Short Description'),
            'description' => Yii::t('product', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategories()
    {
        return $this->hasMany(ProductCategory::className(), ['product_id' => 'id']);
    }
    
    public function getChilds(){
         return $this->hasMany(self::className(), ['variant_of' => 'id']);
    }
    public function getRelatedProducts(){
        if($this->variant_of){
        return self::find()->andWhere(['variant_of' => $this->id])
            ->orWhere(['id' => $this->id])
            ->orWhere(['id' => $this->variant_of])
            ->orWhere(['variant_of' => $this->variant_of])->orderBy('id')->all();
        }else{
            return self::find()->andWhere(['variant_of' => $this->id])
            ->orWhere(['id' => $this->id])->orderBy('id')->all();
        }
    }
    /**
     * @inheritdoc
     * @return ProductQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductQuery(get_called_class());
    }
    public function getSizes(){
        return $this->hasMany(ProductSize::className(), ['product_id' => 'id']);
    }
    public function getBrand(){
        return $this->hasOne(Brands::className(),['id'=>'brand_id']);
    }
    
    public static function getBrandsArray($catId){
        $query = self::find();
        $query->joinWith('productCategories');
        $query->andWhere(['product_category.category_id'=>$catId]);
        $query->select('brand_id');
        
        return Brands::find()->andWhere(['in', 'id', $query])->all();
    }
    public static function getColorsArray($catId){
        $query = ProductI18nData::find();
        $query->groupBy('color_title');
        $query->joinWith('productCategories');
        $query->andWhere(['product_category.category_id'=>$catId]);
        $query->select('color_title');
        $query->andWhere(['language_id'=>Yii::$app->language]);
        return $query->all();
    }
    public static function getLeathersArray($catId){
        $query = ProductI18nData::find();
        $query->groupBy('leather_type');
        $query->joinWith('productCategories');
        $query->andWhere(['product_category.category_id'=>$catId]);
        $query->select('leather_type');
        $query->andWhere(['language_id'=>Yii::$app->language]);
        return $query->all();
    }
    
}
