<?php 
use yii\widgets\ListView;
$this->title =Yii::t('artisans','The Artisans');
$this->registerMetaTag(['description' => Yii::t('app', 'The best Italian leather artisans')]);
$this->registerMetaTag(['keywords' => Yii::t('app', 'Artisans')]);
?>
<div class="container-fluid ">

        <div class="row">
            <div class="col-md-12 no-padding">
                <div id="cover" style="background-image: url('/images/Cover_Artisan.png')">
                </div>       
            </div>
    </div>
    <h1 id="atisans-h1" ><?=Yii::t('artisans','Our craftsmen')?></h1>
    <div class="container-fluid">
        <div class="row artisans-row catalog">
            <div class="col-sm-12 col-lg-12 col-md-12 welcome">
                <p class="roman"><span>-</span><?=Yii::t('artisans','Welcome')?><span>-</span></p>
                <p class="roman"><?=Yii::t('artisans','Meet the best Italian leather artisans')?></p>
            </div>
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'summary' => '',
                'itemOptions' => ['class' => 'item'],
                'itemView' => '_item'
            ]) ?>
            </div>
    </div>
</div>
