
<?php 
$this->title =$model->name;

?>
<div class="container-fluid">

    <div class="row">
       <div class="col-md-12 no-padding">
           <div id="cover" style="background-image:url('<?=isset($model->getImageByRole('cover')->role)?$model->getImagebyRole('cover')->getUrl():'/images/Cover_Artisan.png'?>')">
           </div>                 
        </div>
    </div>
    </div>
    <div class="container-fluid bg-fuid">
    <div class="text-row row">
        <div class="col-sm-12 col-md-12 col-lg-12 head-2">
            <p class="title">
                <span>-</span>
                <?= Yii::t('artisans', 'meet the craftsman') ?>
                <span>-</span>
            </p>

            <h2><?= $model->name ?></h2>

            <p class="desc"><?= $model->data->description ?>
            </p>
        </div>
    </div>
    <?php if($model->video):?>
    <div class="row video-wrap">

        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe allowfullscreen frameborder="0" class="embed-responsive-item"
                        src="<?php echo $model->video[0]->getVideoEmbedLnk(); ?>"></iframe>

            </div>
        </div>
    </div>
<?php endif;?>
<?php
if($model->alternative_contend):?>
    <div class="row text-row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <p class="desc"><?= $model->alternative_contend ?>
            </p>
        </div>
    </div>
<?php
else:
    ?>
<?php if($model->description1):?>
    <div class="row text-row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <p class="desc"><?= $model->description1 ?>
            </p>
        </div>
    </div>
    <?php endif;?>
  
    <?php if(isset($model->getSliderImages()[0]) || isset($model->getSliderImages()[1])):?>
    <div class="row profile-photos">
        <div class="col-sm-6 col-lg-6 col-md-6">

            <img src="<?=isset($model->getSliderImages()[0])?$model->getSliderImages()[0]->getUrl():''?>">
        </div>
        <div class="col-sm-6 col-lg-6 col-md-6">
            <img src="<?=isset($model->getSliderImages()[1])?$model->getSliderImages()[1]->getUrl():''?>">

        </div>
    </div>
<?php endif;?>
<?php if($model->description2):?>
    <div class="row text-row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <p class="desc"><?=$model->description2;?></p>
        </div>
    </div>
    <?php endif;?>
<?php if(isset($model->getSliderImages()[2])):?>
    <div class="row profile-photos">
        <div class="col-sm-12 col-md-12 col-lg-12">

            <img src="<?=$model->getSliderImages()[2]->getUrl()?>">

        </div>
    </div>
    <?php endif;?>
<?php if($model->description3):?>
    <div class="row text-row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <p class="desc"><?=$model->description3;?></p>
        </div>
    </div>
    <?php endif;?>
<?php if(isset($model->getSliderImages()[3])):?>
    <div class="row profile-one-photo">
        <div class="col-sm-12 col-md-12 col-lg-12">

            <img src="<?=$model->getSliderImages()[3]->getUrl()?>">

        </div>
    </div>
    <?php endif;?>
<?php if($model->description4):?>
    <div class="row text-row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <p class="desc"><?=$model->description4?></p>
        </div>
    </div>
    <?php endif;?>
<?php if(isset($model->getSliderImages()[4])):?>
    <div class="row profile-photos">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <img src="<?=$model->getSliderImages()[4]->getUrl()?>">

        </div>
    </div>
    <?php endif;?>
<?php if(isset($model->getSliderImages()[5])):?>
    <div class="row profile-one-photo">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <img src="<?=$model->getSliderImages()[5]->getUrl()?>">

        </div>
    </div>
    <?php endif;?>

    <div class="row profile-photos">
        <?php if(isset($model->getSliderImages()[6])):?>
        <div class="col-sm-6 col-lg-6 col-md-6">
            <img src="<?=isset($model->getSliderImages()[6])?$model->getSliderImages()[6]->getUrl():''?>">

        </div>
    <?php endif;?>
    <?php if(isset($model->getSliderImages()[7])):?>
        <div class="col-sm-6 col-lg-6 col-md-6">

            <img src="<?=isset($model->getSliderImages()[7])?$model->getSliderImages()[7]->getUrl():''?>">

        </div>
        <?php endif;?>
    <?php if(isset($model->getSliderImages()[8])):?>
        <div class="col-sm-12 col-md-12 col-lg-12">

            <img src="<?=isset($model->getSliderImages()[8])?$model->getSliderImages()[8]->getUrl():''?>">

        </div>
        <?php endif;?>
    <?php if(isset($model->getSliderImages()[9])):?>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <img src="<?=isset($model->getSliderImages()[9])?$model->getSliderImages()[9]->getUrl():''?>">

        </div>
        <?php endif;?>
   
    </div>
    <div class="row profile-one-photo">
    <?php if(isset($model->getSliderImages()[10])):?>
        <div class="col-sm-12 col-md-12 col-lg-12">
            <img src="<?=isset($model->getSliderImages()[10])?$model->getSliderImages()[10]->getUrl():''?>">

        </div>
        <?php endif;?>
    <?php if(isset($model->getSliderImages()[11])):?>
        <div class="col-sm-12 col-md-12 col-lg-12">

            <img src="<?=isset($model->getSliderImages()[11])?$model->getSliderImages()[11]->getUrl():''?>">

        </div>
        <?php endif;?>
    <?php if(isset($model->getSliderImages()[12])):?>
        <div class="col-sm-12 col-md-12 col-lg-12">

            <img src="<?=isset($model->getSliderImages()[12])?$model->getSliderImages()[12]->getUrl():''?>">

        </div>
        <?php endif;?>
    </div>
    <div class="row profile-photos">

    <?php if(isset($model->getSliderImages()[13])):?>
        <div class="col-sm-6 col-lg-6 col-md-6">

            <img src="<?=isset($model->getSliderImages()[13])?$model->getSliderImages()[13]->getUrl():''?>">

        </div>
        <?php endif;?>
    <?php if(isset($model->getSliderImages()[14])):?>
        <div class="col-sm-6 col-lg-6 col-md-6">
            <img src="<?=isset($model->getSliderImages()[14])?$model->getSliderImages()[14]->getUrl():''?>">

        </div>
        <?php endif;?>
   
    </div>
<?php endif;?>
</div>