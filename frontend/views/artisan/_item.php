<?php 
use yii\helpers\Url;
$products = $model->products;
?>

<div class="col-sm-4 col-lg-4 col-md-4">
                <div class="thumbnail artisan">
                     <a class="ws-link" href="<?=Url::to(['profile','id'=>$model->id])?>">
                    <img src="<?=$model->image->getUrl('350x')?>" alt="">  
                     </a>
                    <p class="name"><?= $model->name ?></p>
                    <p class="place roman"><?=$model->location?></p>
                    <p class="a-desc"><?=  strip_tags($model->description)?></p>
                    <a class="ws-link" href="<?=Url::to(['profile','id'=>$model->id])?>"><?=Yii::t('artisans','Enter {name}`s workshop', ['name'=>$model->title])?><span>></span></a>
                    <div class="row art-carousel-holder">
                        <?php if(count($products)==1):?>                        
                        <div class="single"><!--- если товар только один, слайдер не выводим -->
                            <a href="<?=Url::to(['product/view','id'=>$products[0]->id])?>"><img src="<?=$products[0]->image->getUrl('160x')?>" /></a>
                        </div>
                        <?php elseif($products>1):?>
                        <?php $this->registerJs("$('#slider{$model->id}').tinycarousel();")?>
                        <div id="slider<?=$model->id?>" class="art-slider">
                            <a class="buttons prev" href="#"></a>
                            <div class="viewport">
                                <ul class="overview">
                                    <?php foreach($products as $product):?>
                                    <li><a href="<?= Url::to(['product/view','id'=>$product->id])?>"><img src="<?=$product->image->getUrl('160x')?>" /></a></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                            <a class="buttons next" href="#"></a>
                        </div>
                        <?php endif;?>
                    </div>
                </div>
            </div>