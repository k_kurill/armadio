<?php
use frontend\models\Product;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
$this->title =$category->name;
?>

<div class="row filters">
    <div class="col-sm-12 col-lg-12 col-md-12">
        <h1><?= $category->name ?></h1>

        <div class="btn-group f-none">
            <?php
            $brands = ArrayHelper::map(Product::getBrandsArray($category->id), 'id', 'name');
            $colors = ArrayHelper::map(Product::getColorsArray($category->id), 'color_title', 'color_title');
            $leathers = ArrayHelper::map(Product::getLeathersArray($category->id), 'leather_type', 'leather_type');
            $params = Yii::$app->request->get();
            $params[0]='/catalog/view';
            if ($category->childrens)
                
            ?>
        </div>
        <div class="btn-group f-right">
            <div class="btn-group">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                    Brands<span class="caret"></span></button>
                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                    <?php 
                        $params = Yii::$app->request->get();
                        $params[0]='/catalog/view';
                        unset($params['brand']);
                    ?>
                    <li><a href="<?=Url::to($params)?>"><?=Yii::t('catalog','All')?></a></li>
                    <?php foreach($brands as $key=>$brand):?>
                    <?php 
                    $params['brand'] = $key;
                    ?>
                    <li><a href="<?=Url::to($params)?>"><?=$brand?></a></li>
                    <?php endforeach;?>
                </ul>
            </div>
            <div class="btn-group">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                    Colors<span class="caret"></span></button>
                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                    <?php 
                        $params = Yii::$app->request->get();
                        $params[0]='/catalog/view';
                        unset($params['color']);
                    ?>
                    <li><a href="<?=Url::to($params)?>"><?=Yii::t('catalog','All')?></a></li>
                    <?php foreach($colors as $key=>$color):?>
                    <?php
                    $params['color'] = $key;
                    ?>
                    <li><a href="<?=Url::to($params)?>"><?=$color?></a></li>
                    <?php endforeach;?>
                </ul>
            </div>
            <div class="btn-group">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                    <?=Yii::t('catalog','Leather')?> <span class="caret"></span></button>
                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                    <?php 
                        $params = Yii::$app->request->get();
                        $params[0]='/catalog/view';
                        unset($params['leather']);
                    ?>
                    <li><a href="<?=Url::to($params)?>"><?=Yii::t('catalog','All')?></a></li>
                    <?php foreach($leathers as $key=>$leather):?>
                    <?php 
                    $params = Yii::$app->request->get();
                    $params['leather'] = $key;
                    $params[0]='/catalog/view';
                    ?>
                    <li><a href="<?=Url::to($params)?>"><?=$leather?></a></li>
                    <?php endforeach;?>
                </ul>
            </div>
        </div>
    </div>
</div>
