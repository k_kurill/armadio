<div class="container-fluid ">

        <div class="row">
            <div class="col-md-12 no-padding">

                <div class="row carousel-holder">

                    <div id="cover">
                             <img class="slide-image" src="images/Cover_bags.png" alt="">
                        </div>

                </div>
            </div>
    </div>
    <div class="container-fluid">
                <div class="row filters">
                    <div class="col-sm-12 col-lg-12 col-md-12">

                    <h1>Bags</h1>
                    <div class="btn-group f-none">
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                        All<span class="caret"></span></button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">first</a></li>
                            <li><a href="#">second</a></li>
                        </ul>
                    </div>
                    <div class="btn-group f-right">
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                Brands<span class="caret"></span></button>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li><a href="#">first</a></li>
                                <li><a href="#">second</a></li>
                            </ul>
                        </div>
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                Colors<span class="caret"></span></button>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li><a href="#">first</a></li>
                                <li><a href="#">second</a></li>
                            </ul>
                        </div>
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                <?=Yii::t('catalog','Leather')?> <span class="caret"></span></button>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                <li><a href="#">first</a></li>
                                <li><a href="#">second</a></li>
                            </ul>
                        </div>
                        </div>
                    </div>
                 </div>

                <div class="row catalog">
                    <div class="col-sm-12 col-lg-12 col-md-12 head-2">
                        <h2>Augusta</h2>
                        <p>Handcrafted by Schoeneberg in Florence, Tuscany</p>
                    </div>
                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail cat-tem">
                           <img src="/images/bag-1.png" alt="">
                           <p>Color camel</p>
                           <p><span>$</span>369</p>
                        </div>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail cat-tem">
                            <img src="/images/bag-2.png" alt="">
                            <p>Color camel</p>
                            <p><span>$</span>369</p>
                        </div>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail cat-tem">
                            <img src="/images/bag-3.png" alt="">
                            <p>Color camel</p>
                            <p><span>$</span>369</p>
                        </div>
                    </div>

                    <div class="col-sm-12 col-lg-12 col-md-12 head-2">
                        <h2>Adora</h2>
                        <p>Handcrafted by Pietrantonio in Ienno in Vinci, Florence</p>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail cat-tem">
                            <img src="/images/bag-7.png" alt="">
                            <p>Color camel</p>
                            <p><span>$</span>369</p>
                        </div>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail cat-tem">
                            <img src="/images/bag-6.png" alt="">
                            <p>Color camel</p>
                            <p><span>$</span>369</p>
                        </div>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail cat-tem">
                            <img src="/images/bag-8.png" alt="">
                            <p>Color camel</p>
                            <p><span>$</span>369</p>
                        </div>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail cat-tem">
                            <img src="/images/bag-9.png" alt="">
                            <p>Color camel</p>
                            <p><span>$</span>369</p>
                        </div>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail cat-tem">
                            <img src="/images/bag-10.png" alt="">
                            <p>Color camel</p>
                            <p><span>$</span>369</p>
                        </div>
                    </div>

                    <div class="col-sm-12 col-lg-12 col-md-12 head-2">
                        <h2>Siora</h2>
                        <p>Handcrafted by Raixe in Venice, Italy</p>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail cat-tem">
                            <img src="/images/bag-4.png" alt="">
                            <p>Color camel</p>
                            <p><span>$</span>369</p>
                        </div>
                    </div>

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail cat-tem">
                            <img src="/images/bag-5.png" alt="">
                            <p>Color camel</p>
                            <p><span>$</span>369</p>
                        </div>
                    </div>

                </div>

        </div>

    </div>