<?php
use yii\widgets\Pjax;
use yii\widgets\ListView;
?>



<?php Pjax::begin()?>
<div class="container-fluid ">

<div class="row">
    <div class="col-md-12 no-padding">

                <?php if($category->image):?>
                    <div id="cover" style="background-image: url('<?=$category->image->getUrl('1120x')?>')">
                    </div> 
          <?php endif;?>

       
    </div>
</div>
    <div class="container-fluid">
        <?=$this->render('_filter',['category'=>$category]);?>
        <div class="row catalog">
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'summary' => '',
                'itemOptions' => ['class' => 'item'],
                'itemView' => '_item'
            ]) ?>
        </div>

    </div>
</div>
<?php Pjax::end();?>