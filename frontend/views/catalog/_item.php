<?php
use yii\helpers\Url;
?>
<div class="col-sm-12 col-lg-12 col-md-12 head-2">
    <h2><?=$model->name?></h2>
    <p><?=Yii::t('artisans','Handcrafted by {brand} in {local}',['brand'=>$model->brand->title,'local'=>$model->brand->location]);?></p>
</div>
<?php foreach($model->relatedProducts as $variant):?>
<div class="col-sm-4 col-lg-4 col-md-4">
    <div class="thumbnail cat-tem">
        <a href="<?=Url::to(['product/view','id'=>$variant->id])?>" data-pjax="0">
            <img src="<?=$variant->getImage()->getUrl('280x')?>" alt="<?=$variant->name?>">
            <p><?=Yii::t('catalog', 'Color')?> <?=$variant->color_title?></p>
            <p><?=Yii::$app->formatter->asCurrency($variant->price)?></p>
        </a>
    </div>
</div>
<?php endforeach ?>
