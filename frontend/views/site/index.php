<?php

/* @var $this yii\web\View */


$this->title = Yii::t('landing', 'Armadio');

?>
 <div class="container-fluid part-2">
        <div class="row">
            <div class="col-sm-12 col-lg-12 col-md-12">
                <div class="main-item-2">
                    <h2>SIORA</h2>
                    <p class="roman">Handcrafted by Raixe in Venice, Italy</p>
                    <img src="/images/bag-5.png" alt="">
                    <p class="desc">Leather tote bag, no stiches</p>
                    <p class="link-to-all"><a href="<?=Yii::$app->urlManager->createUrl(['catalog/view','id'=>3])?>">Shop Bags</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid part-3">
        <div class="row">
            <div class="col-sm-12 col-lg-12 col-md-12">
                <div class="main-item-3">
                    <p>luxury brands have outsourced to small italian artisans for generations applying unreasonable markups</p>
                    <img src="/images/logo_icon.png" alt="">
                    <p>We got rid of the middlemen and crazy markups to get your fair prices at the same high quailty of materials</p>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid part-4">
        <div class="row">
            <div class="col-sm-12 col-lg-12 col-md-12">
                <div class="main-item-4">
                    <h2>Fede</h2>
                    <p class="roman">Handcrafted in Venice, Italy</p>
                    <img src="/images/shoe_pic.png" alt="">
                    <p class="desc">Traditional Italian craftsmanship</p>
                    <p class="link-to-all"><a href="<?=Yii::$app->urlManager->createUrl(['catalog/view','id'=>5])?>">Shop Shoes</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid part-5">
        <div class="row">
            <div class="col-sm-12 col-lg-12 col-md-12">
                <div class="main-item-5">
                    <h2>Just like<br> shopping in Florence</h2>
                    <p>The top italian artisans, one click away</p>
                    <p><a href="<?=Yii::$app->urlManager->createUrl('site/collection')?>">See Collection</a></p>
                </div>
            </div>
        </div>
    </div>