<?php
$this->title =Yii::t('collection', 'Collection');
?>
<div class="container-fluid">

                <div class="row coll-items">
                    <div class="col-sm-12 col-lg-12 col-md-12">
                        <div class="table-in">
                            <img src="/images/Bag_Coll.png">
                            <h2><?=Yii::t('collection', 'Bags')?></h2>
                            <a href="<?=Yii::$app->urlManager->createUrl(['catalog/view','id'=>3])?>"><?=Yii::t('collection', 'Shop All')?><span>></span></a>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6 col-md-6 big">
                        <img src="/images/Dunia_Coll.png">
                        <div class="text-holder">
                            <div>
                                <h2><?=Yii::t('collection', 'Dunia')?></h2>
                                <a href="<?=Yii::$app->urlManager->createUrl(['product/view','id'=>35])?>"><?=Yii::t('collection', 'Shop now')?><span>></span></a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-sm-6 col-lg-6 col-md-6 composite">
                        <div class="part-1">
                            <img src="/images/Moto_Coll.png">
                            <div class="text-holder">
                                <div>
                                    <h2><?=Yii::t('collection', 'Moto')?></h2>
                                    <a href="<?=Yii::$app->urlManager->createUrl(['product/view','id'=>34])?>"><?=Yii::t('collection', 'Shop now')?><span>></span></a>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="part-2">
                            <img src="/images/Shoe_Coll.png">
                            <h2><?=Yii::t('collection', 'Shoes')?></h2>
                            <a href="<?=Yii::$app->urlManager->createUrl(['catalog/view','id'=>5])?>"><?=Yii::t('collection', 'Shop now')?><span>></span></a>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-12 col-md-12">
                        <img src="/images/Cardholder_Coll.png">
                        <h2><?=Yii::t('collection', 'Small Accessories')?></h2>
                        <a href="<?=Yii::$app->urlManager->createUrl(['catalog/view','id'=>9])?>"><?=Yii::t('collection', 'Shop All')?><span>></span></a>
                    </div>
                </div>
        </div>


