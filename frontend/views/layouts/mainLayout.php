<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;
use kartik\icons\Icon;
use yii\web\View;
use yii\widgets\Pjax;


use yii\widgets\ActiveForm;

Icon::map($this);  
AppAsset::register($this);
$add2cartUrl = Url::to(["/cart/add-item"]);
$js = <<<JS
   var add2cartUrl = '$add2cartUrl';
JS;
$this->registerJs($js, View::POS_HEAD);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
     <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">
     <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-71075356-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '475618042646841');
fbq('track', "PageView");
</script>
<noscript>
<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=475618042646841&ev=PageView&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
</head>
<body>
<?php $this->beginBody() ?>
<div class="slidemenumain">
  
<div class="mobheader navbar" role="navigation">
       <div class="slidelogo moblogo">
            
            <p class="logo">
                <a href="<?=Yii::$app->urlManager->createUrl('/')?>"><img src="/images/logo.png" alt="" title=""></a>
            </p>
        </div>
    <div class="slidecross" id="close-navigation">
            <i class="fa fa-times"></i>
        </div>
</div>
        <div class="slidemenu">
         <div class="navbar-collapse" >
                <ul class="nav navbar-nav">
                    <li>
                        <a href="<?=Yii::$app->urlManager->createUrl('site/collection')?>">Collection</a>
                        <ul>
                            <li><span>Shop</span></li>
                           <li><a href="<?=Yii::$app->urlManager->createUrl(['catalog/view','id'=>3])?>">Bags</a></li>
                                    <li><a href="<?=Yii::$app->urlManager->createUrl(['catalog/view','id'=>5])?>">Shoes</a></li>
                                    <li><a href="<?=Yii::$app->urlManager->createUrl(['product/view','id'=>35])?>">Dunia Dress</a></li>
                                    <li><a href="<?=Yii::$app->urlManager->createUrl(['product/view','id'=>34])?>">Moto Jacket</a></li>
                                                  
                        </ul>
                    </li>
                    <li>
                        <a href="<?=Yii::$app->urlManager->createUrl(['catalog/view','id'=>9])?>">Small accessories</a>
                    </li>
                    <li>
                        <a href="<?=Yii::$app->urlManager->createUrl(['artisan'])?>">The artisans</a>
                    </li>
                    <li>
                        <a href="<?=Yii::$app->urlManager->createUrl(['pages/lookbook'])?>">Lookbook</a>
                    </li>
                    <li>
                        <a href="<?=Yii::$app->urlManager->createUrl(['pages/story'])?>">Our story</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
  

    
      <div class="container-fluid head-part">
        <span class="ico-separ"></span>
        <div class="col-md-12">
            <header>
                 <div class="mobheader navbar" role="navigation">
              <div class="mobmenu" id="open-navigation">
                  <i class="fa fa-bars"></i>
              </div>
             
            <div class="moblogo">
            <p class="logo">
                <a href="<?=Yii::$app->urlManager->createUrl('/')?>"><img src="/images/Logo_White_1.png" alt="" title=""></a>
            </p>
            </div>
            <div class="mobilerigthlink">
                  <p class="top-right">
                <!--<a href="#">Sign in</a>-->
                <a class="bag-btn" href="<?=Url::to(['/cart'])?>"></a>
            </p>
            </div>
          </div>
          
        <nav class="navbar big-navbar" role="navigation">
            <p class="top-right">
                <!--<a href="#">Sign in</a>-->
                <a class="bag-btn" href="<?=Url::to(['/cart'])?>"><?=Yii::t('main','Bag')?> <?=(Yii::$app->cart->isEmpty)?'<span></span>':'<span>('.Yii::$app->cart->count.')</span>'?></a>
            </p>
            <p class="logo">
                <a href="<?=Yii::$app->urlManager->createUrl('/')?>"><img src="/images/Logo_White_1.png" alt="" title=""></a>
            </p>
            <!-- Brand and toggle get grouped for better mobile display -->
           
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="navbar-collapse" id="menu-show">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="<?=Yii::$app->urlManager->createUrl('site/collection')?>">Collection</a>
                        <ul>
                            <li><span>Shop</span></li>
                           <li><a href="<?=Yii::$app->urlManager->createUrl(['catalog/view','id'=>3])?>">Bags</a></li>
                                    <li><a href="<?=Yii::$app->urlManager->createUrl(['catalog/view','id'=>5])?>">Shoes</a></li>
                                    <li><a href="<?=Yii::$app->urlManager->createUrl(['product/view','id'=>35])?>">Dunia Dress</a></li>
                                    <li><a href="<?=Yii::$app->urlManager->createUrl(['product/view','id'=>34])?>">Moto Jacket</a></li>
                                                  
                        </ul>
                    </li>
                    <li>
                        <a href="<?=Yii::$app->urlManager->createUrl(['catalog/view','id'=>9])?>">Small accessories</a>
                    </li>
                    <li>
                        <a href="<?=Yii::$app->urlManager->createUrl(['artisan'])?>">The artisans</a>
                    </li>
                    <li>
                        <a href="<?=Yii::$app->urlManager->createUrl(['pages/lookbook'])?>">Lookbook</a>
                    </li>
                    <li>
                        <a href="<?=Yii::$app->urlManager->createUrl(['pages/story'])?>">Our story</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
            <!-- /.container -->
        </nav>
        </header>
            <p class="mini-text">Enter the workshop of</p>
            <h1>The finest italian leather artisans</h1>
            <p class="link-to-all"><a href="<?=Yii::$app->urlManager->createUrl(['site/collection'])?>">Shop now</a></p>
        </div>
    </div>
    

    <div class="content">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>

<div class="container-fluid footer-cont ">

        <!-- Footer -->
         <footer>
            <div class="row">
                <div class="col-sm-12 col-lg-9 col-md-9">
                    <div class="row foot-nav">
                        <div class="col-xs-6 col-sm-7 col-lg-7 col-md-7">
                            <div class="row">
                        <div class="col-xs-12 col-sm-5 col-lg-5 col-md-5">
                            <div>
                                <p>Shop</p>
                                 <p><a href="<?=Yii::$app->urlManager->createUrl(['catalog/view','id'=>3])?>">Bags</a></p>
                                <p><a href="<?=Yii::$app->urlManager->createUrl(['catalog/view','id'=>5])?>">Shoes</a></p>
                                <p><a href="<?=Yii::$app->urlManager->createUrl(['product/view','id'=>35])?>">Dunia Dress</a></p>
                                <p><a href="<?=Yii::$app->urlManager->createUrl(['product/view','id'=>34])?>">Moto Jacket</a></p>
                                <p><a href="<?=Yii::$app->urlManager->createUrl(['catalog/view','id'=>9])?>">Small Accessories</a></p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-7 col-lg-7 col-md-7" id="help-info">
                            <div>
                                <p>Need help?</p>
                                <p>Call: +1 415 990 4072</p>
                                <p>hello@armadiofashion.com</p>
                            </div>
                        </div>
                            </div>
                    </div>
                        <div class="col-xs-6 col-sm-3 col-lg-3 col-md-3">
                            <div>
                                <p>Company</p>
                                <p><a href="<?=Yii::$app->urlManager->createUrl(['artisan'])?>">Artisans</a></p>
                                <p><a href="<?=Yii::$app->urlManager->createUrl(['pages/story'])?>">Our Story</a></p>
                                <p><a href="<?=Yii::$app->urlManager->createUrl(['pages/lookbook'])?>">Lookbook</a></p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-2 col-lg-2 col-md-2">
                            <div class="footersocial">
                                 <p>Socials</p>
                                <a href="https://www.facebook.com/myarmadio/" target="_blank"><i class="fa fa-facebook"></i></a>
                                <a href="https://www.instagram.com/myarmadio/" target="_blank"><i class="fa fa-instagram"></i></a>
                                <a href="https://twitter.com/myarmadio" target="_blank"><i class="fa fa-twitter"></i></a>
                                <a href="http://myarmadio.tumblr.com/" target="_blank"><i class="fa fa-tumblr"></i></a>
                                
                               
                            </div>
                        </div>
                       
                    </div>
                </div>
                <div class="col-sm-12 col-lg-3 col-md-3 no-padding">
                    <div class="row">
                        <div class="col-sm-12 col-lg-12 col-md-12" id="access-main">
                            <div>
                                <p>
                                    Newsletter
                                </p>
                               
                                <p class="newsletter">Get early access to our next collections</p>                                 
                                 
                                    <?php 
                                         Pjax::begin(['id'=>'add-newsletter','enablePushState'=>false]);
                                        $form = ActiveForm::begin([  
                                        'options' => ['data-pjax' => true],  
                                            'action' => ['site/nesletter',]]); 
                                           echo Html::input('email','subject',null,['class'=>"form-control", 'placeholder'=>"Your email", 'id'=>"email"]);
                                           echo Html::submitButton(Yii::t('mail', 'SIGN UP') , ['class'=>"btn btn-default"]);
                                         ActiveForm::end();
                                         Pjax::end();
                                    ?>

                                   
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-lg-12 col-md-12" id="row-bottom-menu">
                    <ul class="bottom-menu">
                        <li>2016&nbsp;Armadio</li>
                        <li><a href="<?=Yii::$app->urlManager->createUrl(['pages/policy'])?>">Privacy&nbsp;Policy</a></li>
                        <li><a href="#">Terms&nbsp;&&nbsp;Conditions</a></li>
                        <li><a href="<?=Yii::$app->urlManager->createUrl(['pages/shipping_returns'])?>">Shipping&nbsp;&&nbsp;Returns</a></li>
                    </ul>

                </div>
            </div>
        </footer>

    </div>

<?php $this->endBody() ?>
</body>
</html>
<?php
$ShowMenu = <<<JS
   

        
            $('#open-navigation').click(function(event){
   
            $('.slidemenumain').animate({left:0});
            
            $('html').css('overflow-y', 'hidden');
            $('body').css('overflow-y', 'hidden');
          });
        $('#close-navigation').click(function(event){
   
        $('.slidemenumain').animate({left:-999});
            $('html').css('overflow-y', 'auto');
            $('body').css('overflow-y', 'auto');
       
            });
       
       
JS;
Yii::$app->view->registerJs($ShowMenu);
?>
<?php $this->endPage() ?>
