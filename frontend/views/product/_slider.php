<?php
use yii\bootstrap\Html;


/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
?>
<div id="carousel-example-generic" class="carousel slide">
    <div class="carousel-inner">
        <?php foreach ($model->sliderImages as $index=>$image): ?>
            <div class="item <?= ($index == 0? 'active':'')?>">
                <img class="slide-image" src="<?= $image->getUrl() ?>" alt="">
            </div>
        <?php
        endforeach; ?>
    </div>
    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
        <span class="lefticon glyphicon-chevron-left"></span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
        <span class="righticon glyphicon-chevron-right"></span>
    </a>
</div>
