<div class="row">
<?php
foreach($products as $product):
   ?>
    <div class="col-sm-4 col-lg-4 col-md-4">
        <div class="thumbnail">
            <img src="<?=$product->image->getUrl('280')?>" alt="">
            <a href="<?=Yii::$app->urlManager->createUrl(['product/select',['id'=>$product->id]])?>">Quick view</a>
            <p>Color camel</p>
            <p><span>$</span>369</p>
        </div>
    </div>
<?php
endforeach;
?>
</div>

