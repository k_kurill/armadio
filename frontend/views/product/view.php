<?php 
use yii\helpers\Url;
use yii\widgets\Pjax;
$this->title =$model->name;

$css = <<<CSS
    .inactive{
        color: #cbcbcb;
    }
CSS;
$this->registerCss($css);


?>

<?php Pjax::begin(['id'=>'product-view'])?>
<div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="row carousel-holder">

                    <div class="col-md-12">
                       <?= $this->render('_slider',['model'=>$model])?>
                    </div>

                </div>
            </div>
    </div>
    <div class="container-fluid">
                <div class="row product-row">
                    <div class="col-sm-4 col-lg-4 col-md-4 head-block">
                        <h1><?=$model->name?></h1>
                        <?php if($model->brand):?>
                        <p class="roman"><?=Yii::t('product','Handcrafted by {brand} in {location}',['brand'=>$model->brand->title,'location'=>$model->brand->location]);?></p>
                        <?php endif ?>
                        <p><?=Yii::$app->formatter->asCurrency($model->price)?></p>
                    </div>
                    <div class="col-sm-4 col-lg-4 col-md-4 var-block">
                        <div class="color">
                            <p>
                                <?php foreach ($model->relatedProducts as $variant):?>
                                <a href="<?=Url::to(['product/view', 'id'=>$variant->id])?>" class="round-button <?=($variant->id == $model->id)?'active-round':''?>" style="background: <?=$variant->color?>"></a>
                                <?php endforeach;?>
                            </p>
                        </div>
                        <?php if($model->sizes):?>
                        <div class="size">
                            <p>
                                <ul>
                                    <?php foreach ($model->sizes as $key=>$size):?>
                                    <li data-size="<?=$size->id?>" class="size-item <?=($size->count)?'':'inactive'?>"><?=$size->size?></li>
                                    <?php endforeach;?>
                                </ul>
                            </p>
                        </div>
                        <p class="info-link"><a href="#"><?= Yii::t('product','Size Guide')?></a></p>
                    <?php endif;?>
                    </div>
                    <div class="col-sm-4 col-lg-4 col-md-4 to-cart">
                        <div class="tocart-holder">
                            <span class="buy-warning" style="display: none"><?= Yii::t('product','Please select size');?></span>
                            <?php if($model->count):?>
                                <p class="add-cart-button" data-id="<?=$model->id?>">
                                    <span><?= Yii::t('product','Add to bag')?></span>
                                </p>
                            <?php else: ?>
                                <p class="add-cart-button solded">
                                    <span><?= Yii::t('product','sold out')?></span>
                                </p>
                            <?php endif; ?>                            
                            <p class="share-links"><a href="mailto:?subject='<?=$model->name?>'"><?= Yii::t('product','Share with a friend')?></a></p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="row decsription">
                    <div class="col-sm-12 col-lg-12 col-md-12">
                        <?php if($model->description):?>
                            <div class="row info-holder">
                                <div class="col-sm-12 col-lg-4 col-md-4">
                                    <h2 class="head"><?= Yii::t('product','Product info')?></h2>
                                </div>
                                <div class="col-sm-12 col-lg-8 col-md-8">
                                    <div class="desk"><?=$model->description?></div>
                                </div>
                            </div>
                        <?php endif;?>
                        <?php if($model->leather):?>
                            <div class="row info-holder">
                                <div class="col-sm-12 col-lg-4 col-md-4">
                                    <h2 class="head"><?= Yii::t('product','Leather')?></h2>
                                </div>
                                <div class="col-sm-12 col-lg-8 col-md-8">
                                    <div class="desk"><?=$model->leather?></div>
                                </div>
                            </div>
                        <?php endif;?>
                        <?php if($model->measurements):?>
                            <div class="row info-holder">
                                <div class="col-sm-12 col-lg-4 col-md-4">
                                    <h2 class="head"><?= Yii::t('product','Measurements')?></h2>
                                </div>
                                <div class="col-sm-12 col-lg-8 col-md-8">
                                    <div class="desk"><?=$model->measurements?></div>
                                </div>
                            </div>
                        <?php endif;?>
                        
                    </div>
                </div>

                <div class="row product-photos">
                    <?php if($model->getImagebyRole('image1')->role):?>
                    <div class="col-sm-12 col-lg-12 col-md-12">
                        <img src="<?=$model->getImagebyRole('image1')->getUrl()?>">
                    </div>
                <?php endif;?>
                <?php if($model->getImagebyRole('image2')->role):?>
                    <div class="col-sm-6 col-lg-6 col-md-6">
                        <img src="<?=$model->getImagebyRole('image2')->getUrl()?>">
                    </div>
                    <?php endif;?>
                <?php if($model->getImagebyRole('image3')->role):?>
                    <div class="col-sm-6 col-lg-6 col-md-6">
                        <img src="<?=$model->getImagebyRole('image3')->getUrl()?>">
                    </div>
                     <?php endif;?>
                <?php if($model->getImagebyRole('image4')->role):?>
                    <div class="col-sm-12 col-lg-12 col-md-12">
                        <img src="<?=$model->getImagebyRole('image4')->getUrl()?>">
                    </div>
                     <?php endif;?>
                
                </div>
                <div class="row product-crman">
                    <div class="col-sm-12 col-lg-12 col-md-12 product-crman-holder">
                        <p class="title"><span>-</span><?= Yii::t('product','Meet the craftsman')?><span>-</span></p>
                        <h2 class=""><?=$model->brand->title?></h2>
                        <img src="<?=$model->brand->image->getUrl()?>">
                        <a href="<?=Yii::$app->urlManager->createUrl(['artisan/profile','id'=>$model->brand->id])?>"><?=Yii::t('product','Enter {name}`s workshop',['name'=>$model->brand->title]);?><span></span></a>
                    </div>
                </div>
        </div>


    </div>
<?php Pjax::end()?>
<div class="modal fade" id="modalAddToCart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">          
            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h1></h1>
            </div>
            <div class="modal-body">
                <p><?=Yii::t('card','Product is added to the shopping cart')?></p>
            </div>
            <div class="modal-footer">
               
                <a href="<?=Url::to('/cart')?>" class="btn btn-primary to-card"><?=Yii::t('card','Go to the Shopping Cart')?></a> 
                <button class="btn" data-dismiss="modal" aria-hidden="true"><?=Yii::t('card','Continue with purchases')?></button>
            </div>
        </div>
    </div>
</div>
