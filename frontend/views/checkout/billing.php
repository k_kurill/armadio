<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

?>
<div class="title-card row">
    <div class="col-md-4 col-sm-4 ">
        <a href="<?=Url::to('shipping')?>"><?=Yii::t('checkout','shipping')?></a>
    </div>
     <div class="col-md-4 col-sm-4 active">
         <a href="<?=Url::to('billing')?>"><?=Yii::t('checkout','billing')?></a>
    </div>
     <div class="col-md-4 col-sm-4">
         <a href="#"><?=Yii::t('checkout','review & confirm')?></a>
    </div>


</div>
<div class="row" id="billing">
    
        <?= $this->render('_card_form', ['billing'=>$model]);?>

       
    <div class="col-md-6 col-sm-6" id="billing-info">
        <?= $this->render('_allsummary') ?>

    </div>
</div>

<?php
$Choos = <<<JS
      /*  
$('#use-new-address').css('display', 'none');
$('#use-old-address').css('display', 'block');
        
$('.choose-pay').change(function(event){
    if($('#choose-credit-card').prop('checked')){
        $('#to-card').show(500);
    }
    else{
        $('#to-card').hide(500);
    }});
        
    $('.choose-address').change(function(event){
        if($('#choose-shipping-address').prop('checked')){
            $('#use-new-address').hide(500);
            $('#use-old-address').show(500);
        }
        else{
            $('#use-new-address').show(500);
            $('#use-old-address').hide(500);
        }});
        */
       
JS;
Yii::$app->view->registerJs($Choos);
?>
