<?php 
use yii\bootstrap\ActiveForm;
?>
<style>
        #card-expires-mm,
        #card-expires-yy{
            width: 80px;
            border-radius: 3px !important;
            float: left;
        }
        #card-expires-yy{
            margin-right: 5px;
        }
        .card-expires div{
            padding-left: 0;
        }
        #sec-code{
            width: 140px;
            color: #fff;
            height: 32px;
            font-size: 14px;
        }
       
    </style>
    <div class="col-md-6 col-sm-6" id="shippng-form">
        <div class="form-title">
            <h3>Pay with credit card</h3>
        </div>
        <?php $form = ActiveForm::begin(); ?>
        
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <?= $form->field($billing, 'first_name')?>              
                </div>
                <div class="col-md-6 col-sm-6">
                    <?= $form->field($billing, 'last_name')?>              
                </div>
            </div>
        <?= $form->field($billing, 'card_num')?>
        <div class="form-group field-shipping-email required card-expires">
            
            <div class="col-md-3 col-sm-3">
            <?= $form->field($billing, 'month')->label('Expires(mm)')?>
            </div>
            <div class="col-md-3 col-sm-3">
            <?= $form->field($billing, 'year')->label('Expires(yyyy)')?>
            </div>
            <div class="col-md-4 col-sm-4">
                <?= $form->field($billing, 'cvv')->passwordInput(['style'=>'background-color: #b5b5b5 !important; height: 32px;'])->label('Security Code')?>
            </div>
            <div class="clearfix"></div>
            <div class="help-block"></div>
        </div>
        <div class="form-group" style="padding-top: 30px;">
                <button type="submit" class="btn btn-primary pull-right">Continue checkout</button>
        </div>
        
        <?php ActiveForm::end(); ?>
    </div>