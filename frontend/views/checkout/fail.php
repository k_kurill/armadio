<?php 
$this->title =Yii::t('fail', 'Fail');

?>
<div class="container-fluid ">

<div class="row">
    <div class="col-md-12 no-padding">
                <div id="cover">
                    <img class="slide-image catalog-cover" src="/images/success_cover.png" alt="">
                </div>      
    </div>
</div>
 </div>
<div class="container-fluid">
    <div class="text-row row" id="fail">
        <div class="col-md-12 col-sm-12 thank-you-col">
        <h1><?= Yii::t('fail', 'failed payment') ?></h1>
        <p><?= Yii::t('fail', 'The transaction failed and the payment has not been validated') ?></p>
        <p><?=Yii::t('fail', 'message: ');?> <?=$result->message?></p>
        </div>
        <div class="col-md-12 col-sm-12 check-order">
        <p><?= Yii::t('success', 'Please try again later') ?></p>
        </div>
        <div class="col-md-12 col-sm-12 contact-info">
            <p><?= Yii::t('fail', 'For any questions or concerns, we are always here for you.') ?></p>
            <p><?= Yii::t('fail', 'Dont hesitate to contact us at ') ?>hello@armadiofasnion.com</p>
            <p><?= Yii::t('fail', 'or give us a call at ') ?>+1&nbsp;415.990.4072</p>
        </div>
        <a href="/"><?= Yii::t('fail', 'go back to website') ?></a>
    </div>
</div>

