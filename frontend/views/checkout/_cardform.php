<div class="form-card" id="to-card">

            
            <div class="mandatory-form-card">
                <?= $form->field($model, 'card_type')->dropDownList(['visa' => 'Visa', 'mastercard' => 'MasterCard', 'discover' => 'Discover', 'amex' => 'American Express']) ?>
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <?= $form->field($model, 'first_name') ?>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <?= $form->field($model, 'last_name') ?>
                    </div>
                </div>
                <?= $form->field($model, 'card_num') ?>
                <div class="row">
                    <div class="col-md-3 col-sm-3">
                        <?= $form->field($model, 'month') ?>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <?= $form->field($model, 'year') ?>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <?= $form->field($model, 'cvv') ?>
                    </div>
                </div>
                <?= $form->field($model, 'phone') ?>
            </div>
            <input value='0' type="radio" id="choose-shipping-address" class="choose-address" checked="checked" name="address"/>
            <label class="label-address" for="choose-shipping-address"><?= Yii::t('billing', 'billing sddress same as the shipping'); ?></label>
            <div class="address-card" id="use-old-address">
                <p><?= $model->address ?></p>
                <p><?= $model->address2 ?></p>
                <p><?= $model->city ?></p>
                <p><?= $model->state ?></p>
                <p><?= $model->zip ?></p>
            </div>
            <input value='1' type="radio" id="choose-new-address" class="choose-address"  name="address"/>
            <label class="label-address" for="choose-new-address"><?= Yii::t('billing', 'use a new address') ?></label>
            <div class="address-card" id="use-new-address">
                <?= $form->errorSummary([$model]) ?>
                <div class="form-group">
                    <select size="1" class="form-control" multiple name="country[]">
                        <option value="USA">USA</option>
                    </select>
                </div>
                <?= $form->field($model, 'address') ?>
                <?= $form->field($model, 'address2') ?>
                <div class="form-group">
                    <?= $form->field($model, 'city') ?>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class="form-group">
                            <label class="control-label"><?= Yii::t('billing', 'State') ?></label>
                            <select size="1" class="form-control" multiple name="state[]">
                                <option value="AK">AK</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-8">
                        <?= $form->field($model, 'zip') ?>
                    </div>
                </div>

            </div>


        </div>