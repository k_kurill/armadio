<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \common\models\Shipping */
/* @var $form ActiveForm */
?>
<div class="title-card row">
    <div class="col-md-4 col-sm-4">
        <a href="<?=Url::to('shipping')?>"><?=Yii::t('checkout','shipping')?></a>
    </div>
     <div class="col-md-4 col-sm-4">
         <a href="<?=Url::to('billing')?>"><?=Yii::t('checkout','billing')?></a>
    </div>
     <div class="col-md-4 col-sm-4 active">
         <a href="<?=Url::to('review')?>"><?=Yii::t('checkout','review & confirm')?></a>
    </div>


</div>
<div class="row" id="confirm">
    <div class="col-md-6 col-sm-6" id="confirm-form">
        <div class="row">
            <div class="col-md-12 col-sm-12 main-title">
                <h3><?=yii::t('review','delivery options') ?></h3>
            </div>
            <div class="col-md-12 col-sm-12 info-body">
                <h3><?=yii::t('review','shipping address') ?></h3>
                <a href="<?=Url::to(['shipping'])?>"><?=yii::t('review','Edit')?></a>
        <div class="address-card" >
                <p><?=$shipping->address?></p>
                <p><?=$shipping->address2?></p>
                <p><?=$shipping->city?></p>
                <p><?=$shipping->state?></p>
                <p><?=$shipping->zip?></p>
                <p><?=$shipping->email?></p>
            </div>
            </div>
        </div>
         <div class="row">
            <div class="col-md-12 col-sm-12 main-title">
                <h3><?=yii::t('review','payment options') ?></h3>
            </div>
            <div class="col-md-12 col-sm-12 info-body">
                <h3><?=yii::t('review','payment type') ?></h3>
                <a href="<?=Url::to(['billing'])?>"><?=yii::t('review','Edit')?></a>
            </div>
            <div class="col-md-12 col-sm-12 info-body" id="choose-pay">
                <div class="address-card">
                    <span id="visa" style="display: none"></span>
                <span id="mastercard" style="display: none"></span>
                <span id="a-express" style="display: none"></span>
                <span id="discover" style="display: none"></span>
                <span id="paypal" style="display: none"></span>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 info-body" id="card-info" style="display: none">
                <div class="address-card">
                    
                </div>
            </div>
            <div class="col-md-12 col-sm-12 info-body" style="display:none;">
            <div class="address-card">
                
            </div>
            </div>
             <div class="col-md-12 col-sm-12 info-body" id="get-promo-code" style="display: none">
                <h3><?=yii::t('review','promo code') ?></h3>
                <a href="#"><?=yii::t('review','Edit')?></a>
             </div>
             <div class="col-md-12 col-sm-12 info-body">
                <a class="btn btn-primary" href="<?=Url::to(['confirm'])?>"><?=yii::t('review','place my order') ?></a> 
             </div>
        </div>
       
    </div>
    <div class="col-md-6 col-sm-6" id="shipping-info">
         <?=$this->render('_allsummary')?>     
    </div>
</div>

