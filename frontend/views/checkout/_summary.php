
<?php foreach(Yii::$app->cart->getPositions() as $model):?>
 <div class="order-summary-row row">
     <div class="col-md-5 col-sm-5">
         <img src="<?=$model->image->getUrl('150x')?>">
     </div>
     <div class="col-md-7 col-sm-7">
         <p> <?= $model->name;?></p>
         <p><?=Yii::t('shipping', 'Color:')?><span><?= $model->color_title?></span></p>
         <p><?=Yii::t('shipping', 'Size:')?><span><?= $model->size?></span></p>
         <p><?=Yii::t('shipping', 'Quantity:')?><span><?= $model->getQuantity()?></span></p>
         <p><?=Yii::t('shipping', 'Price:')?><span><?=Yii::$app->formatter->asCurrency($model->price * $model->getQuantity() )?></span></p>
 </div>

 </div>
<?php endforeach;?>