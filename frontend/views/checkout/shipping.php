<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \common\models\Shipping */
/* @var $form ActiveForm */
?>
<div class="title-card row">
    <div class="col-md-4 col-sm-4 active">
        <a href="<?=Url::to('shipping')?>"><?=Yii::t('checkout','shipping')?></a>
    </div>
     <div class="col-md-4 col-sm-4">
         <a href="#"><?=Yii::t('checkout','billing')?></a>
    </div>
     <div class="col-md-4 col-sm-4">
         <a href="#"><?=Yii::t('checkout','review & confirm')?></a>
    </div>


</div>
<div class="row" id="shippng">
    <div class="col-md-6 col-sm-6" id="shippng-form">
        <div class="form-title">
            <h3><?=Yii::t('shipping', 'Shipping Address')?></h3>
        </div>
        <?php $form = ActiveForm::begin(); ?>
            <?=$form->errorSummary([$model])?>
            <?= $form->field($model, 'name') ?>
            <?= $form->field($model, 'email') ?>
            <?= $form->field($model, 'address') ?>
            <?= $form->field($model, 'address2') ?>
            <?= $form->field($model, 'city') ?>
        <div class="row">
            <div class="col-md-6 col-sm-6">
            <?= $form->field($model, 'state') ?>
            </div>
            <div class="col-md-6 col-sm-6">
            <?= $form->field($model, 'zip') ?>
                </div>
        </div>
            <?= $form->field($model, 'phone') ?>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('shiping', 'Continue checkout'), ['class' => 'btn btn-primary pull-right']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
    <div class="col-md-6 col-sm-6" id="shipping-info">
         <?=$this->render('_allsummary')?>     
    </div>
</div><!-- shippng --> 
