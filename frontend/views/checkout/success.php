<?php 
$this->title =Yii::t('success', 'Success');

?>
<div class="container-fluid ">

<div class="row">
    <div class="col-md-12 no-padding">
                <div id="cover">

                </div>      
    </div>
</div>
 </div>
<div class="container-fluid">
    <div class="text-row row" id="success">
        <div class="col-md-12 col-sm-12 thank-you-col">
        <h1><?= Yii::t('success', 'thank you') ?></h1>
        <p><?= Yii::t('success', 'Your order has been placed') ?></p>
        </div>
        <div class="col-md-12 col-sm-12 check-order">
        <p><?= Yii::t('success', 'You will soon receive a confirmation, your order is on its way ') ?>:)</p>
        <p>(<?= Yii::t('success', 'Make sure you check your spam folder if you don\'t find email, sometimes that happens ') ?>)</p>
        </div>
        <div class="col-md-12 col-sm-12 contact-info">
            <p><?= Yii::t('success', 'For any questions or concerns, we are always here for you.') ?></p>
            <p><?= Yii::t('success', 'Dont hesitate to contact us at ') ?>hello@armadiofasnion.com</p>
            <p><?= Yii::t('success', 'or give us a call at ') ?>+1&nbsp;415.990.4072</p>
        </div>
        <a href="/"><?= Yii::t('success', 'go back to website') ?></a>
    </div>
</div>