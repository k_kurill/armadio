<div class="order-summary">
    <h3><?= Yii::t('shipping', 'Order summary') ?></h3>

</div>
<?= $this->render('_summary') ?>
<div class="order-summary-row row">
    <div class="col-md-8 col-sm-8 row-title"><p><?= Yii::t('shipping', 'Subtotal:') ?></p></div>
    <div class="col-md-4 col-sm-4 row-val"><p><?= Yii::$app->formatter->asCurrency(Yii::$app->cart->cost) ?></p></div>
    <div class="col-md-8 col-sm-8 row-title"><p><?= Yii::t('shipping', 'Est. Sales Tax:') ?></p></div>
    <div class="col-md-4 col-sm-4 row-val"><p></p></div>
    <div class="col-md-8 col-sm-8 row-title"><p><?= Yii::t('shipping', 'Estimated Shipping:') ?></p></div>
    <div class="col-md-4 col-sm-4 row-val"><p><?= Yii::$app->formatter->asCurrency(0) ?></p></div>
</div>
<div class="row order-summary-row">
    <div class="col-md-8 col-sm-8 row-title"><p><?= Yii::t('shipping', 'Estimated Total:') ?></p></div>
    <div class="col-md-4 col-sm-4 row-val"><p><?= Yii::$app->formatter->asCurrency(Yii::$app->cart->cost) ?></p></div>

</div>

