<?php
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
?>

<div class="title-card row">
<h1><?=Yii::t('cart','shopping bag is empty')?></h1>
</div>
<div class="row" style="
    max-width: 1440px;
    margin-left: auto !important;
    margin-right: auto !important;
    padding: 170px 0;
    background: #fff;
    text-align: center;
">
  &lt;<a id="empty-cart-link" class="btn" href="<?=Url::to(['/'])?>" style="
    font-size: 14px;
    color: #4a4a4a;
    font-weight: 600;
    text-transform: uppercase;
"><?=Yii::t('cart', 'Continue shopping')?></a>
</div>