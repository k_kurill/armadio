<div class="col-md-3 col-sm-3 item-favorites">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <img src="/images/placeholder.gif">
        </div>
        <div class="col-md-12 col-sm-12">
            <p class="cart-item-name">tularosa</p>
            <p class="cart-item-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            </p>
            <p class="cart-item-price">$168.00</p>
            
        </div>
        <div class="col-md-12 col-sm-12">
                <a class="btn btn-default" data-pjax="0" href="#"><?= Yii::t('cart', 'move to bag') ?></a>
        </div>
        <div class="col-md-12 col-sm-12" id="remove-link">
                <a class="" href="#"><?= Yii::t('cart', 'Remove') ?></a>
        </div>
    </div>
</div>

