<?php 
use yii\helpers\Url;
?>
<div class="row " id="total-row">
    <div class="col-md-6 col-sm-6 cart-summry-notnow">
        <a class="btn pull-right" href="<?= Url::to(['/']) ?>"><span><</span><?= Yii::t('cart', 'Continue shopping') ?></a>
    </div>
    <div class="col-md-6 col-sm-6 all-price">
        <div class="row" id="individual-prices">
            <div class="col-md-8 col-sm-8">
                <p><?= Yii::t('cart', 'subtotal')?></p>
            </div>
            <div class="col-md-4 col-sm-4">
                <p><?=Yii::$app->formatter->asCurrency(Yii::$app->cart->cost);?></p>
            </div>
            <div class="col-md-8 col-sm-8">
                <p><?= Yii::t('cart', 'estimated shipping')?>
                <a href="#"><?= Yii::t('cart', 'Viev option')?>s</a>
                </p>
                
            </div>
            <div class="col-md-4 col-sm-4">
                <p>free</p>
            </div>
        </div>
        <div class="row" id ="sum-of-prices">
            <div class="col-md-8 col-sm-8">
                <p><?= Yii::t('cart', 'estimated total')?></p>
            </div>
            <div class="col-md-4 col-sm-4">
                <p><?=Yii::$app->formatter->asCurrency(Yii::$app->cart->cost);?></p>
            </div>
        </div>
        <div class="row buttons-row">
            <div class="col-md-12 col-sm-12 cart-checkout" >
                <a class="btn btn-default" data-pjax="0" href="<?= Url::to(['checkout/order']) ?>"><?= Yii::t('cart', 'Proceed to checkout') ?></a>
            </div>
            <div class="col-md-12 col-sm-12" id="paypal" style="display: none;" >
                <a class="btn btn-default " data-pjax="0" href="#"><?= Yii::t('cart', 'Check out with') ?></a>
            </div>
        </div>
    </div>



</div>