<?php
/* @var $this yii\web\View */
use frontend\widgets\CartGrid;
use yii\widgets\Pjax;

$removeItemUrl = Yii::$app->urlManager->createUrl(['cart/delete-item']);
$updateItemUrl = Yii::$app->urlManager->createUrl(['cart/update-item']);

$this->title = Yii::t('cart', 'shopping bag');
$this->registerCssFile('/css/cart.css');
$js = <<<JS
        $(document).ready(function(){            
            $('a.cart-item-delete').on('click',function(e){
                e.preventDefault();
                removeCartItem($(this).data('id'));
            });
            
        });
        function refreshCart(){
            $.pjax({container: '#cart'});
        }
        function removeCartItem(itemId){{
            $.get( "$removeItemUrl", { id: itemId}).done(function( data ){
                        refreshCart(); 
                    });
            };
        };
        function changeItemCount(itemId, iqty){{
            $.get( "$updateItemUrl", { id: itemId, qty: iqty}).done(function( data ){
                        refreshCart(); 
                    });
            };
        };
JS;
$this->registerJs($js, \yii\web\View::POS_END);
?>
<div class="title-card row">
<h1><?=$this->title?></h1>
</div>
<?php Pjax::begin(['id'=>'cart']);?>
<div class="row cart-item-title">
    <div class="col-md-2 col-sm-2 cart-item-image">
        <p>item</p>
    </div>
    <div class="col-md-10 col-sm-10 cart-item-properties">
        <div class="row">
            <div class="col-md-4 col-sm-4 cart-item-name">
                <p>description</p>
            </div>
            <div class="col-md-2 col-sm-2 cart-item-color">
                <p>color</p>
            </div>
            <div class="col-md-1 col-sm-1 cart-item-size">
                <p>size</p>
            </div>
            <div class="col-md-1 col-sm-1 cart-item-qty">
                <p>qty</p>
            </div>
            <div class="col-md-2 col-sm-2 cart-item-actions">
               <p>actions</p>
            </div>
            <div class="col-md-2 col-sm-2 ">
              <p>price</p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <?=CartGrid::widget([]);?>
    </div>
    <div class="col-md-12 col-sm-12">
        <?=$this->render('_cartSummary');?>
    </div>
</div>
<div class="row" style="display: none"  id="favorires-row">
    <div class="col-md-12 col-sm-12">
    <h1><?= Yii::t('cart', 'your favorites') ?></h1>
    </div>
    <?=$this->render('_yourFavorites');?>
</div>
<?php Pjax::end();?>
