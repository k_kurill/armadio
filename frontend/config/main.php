<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'user' => [
            'as frontend' => 'dektrium\user\filters\FrontendFilter',            
        ],
        'payment' => [
            'class' => 'achertovsky\paypal\Module',
            'cancelUrl' => '/checkout/fail',
            'cancelUrl' => '/checkout/fail',
            'modelMap'  => [
                'PaypalExpressPayment' => 'common\models\PaypalExpressPayment',
            ],
            'controllerMap' => [
                'payment' => [
                    'class' => 'frontend\controllers\PaymentController',
                ]
            ],
        ],
    ],
    'components' => [
        'request'=>[
            'baseUrl'=>'',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'class' => 'codemix\localeurls\UrlManager',
            'languages' => ['en'=>'en-US', 'hr'=>'hr-HR'],
            'rules' => [
                'pages/<page:[\w-]+>' => 'pages/default/index',
            ]
        ],
        'cart' => [
            'class' => 'yz\shoppingcart\ShoppingCart',
            'cartId' => 'my_application_cart',
        ],
        /*'translatemanager' => [
            'class' => 'lajax\translatemanager\Component'
        ],*/
    ],
    'params' => $params,
];
