<?php

use yii\db\Migration;

/**
 * Handles the creation for table `shipping_table`.
 */
class m160330_143413_create_shipping_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $sql = "CREATE TABLE `shipping` (
            `order_id` int(11) NOT NULL PRIMARY KEY,
            `user_id` int(11) NOT NULL,
            `name` varchar(255) NOT NULL,
            `email` varchar(255) NOT NULL,
            `address` varchar(255) NOT NULL,
            `city` varchar(255) NOT NULL,
            `state` varchar(255) NOT NULL,
            `zip` varchar(255) NOT NULL,
            FOREIGN KEY (`order_id`) REFERENCES `order` (`id`) ON DELETE RESTRICT,
            FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT
          ) ENGINE='InnoDB' COLLATE 'utf8_general_ci';";
        $this->execute($sql);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('shipping_table');
    }
}
