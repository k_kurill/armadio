<?php

use yii\db\Migration;

class m160327_190548_add_image_cropped_marker extends Migration
{
    public function up()
    {
        $this->addColumn('image', 'cropped', $this->boolean());
    }

    public function down()
    {
        echo "m160327_190548_add_image_cropped_marker cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
