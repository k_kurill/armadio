<?php

use yii\db\Migration;

class m160406_154519_add_billing_fields_adress extends Migration
{
    public function up()
    {
        $query = "ALTER TABLE `billing`
                ADD `address` varchar(255) COLLATE 'utf8_general_ci' NOT NULL AFTER `zip`,
                ADD `address2` varchar(255) COLLATE 'utf8_general_ci' NOT NULL AFTER `address`,
                ADD `city` varchar(255) COLLATE 'utf8_general_ci' NOT NULL AFTER `address2`,
                ADD `state` varchar(255) COLLATE 'utf8_general_ci' NOT NULL AFTER `city`;";
        $this->execute($query);
        $sql = "ALTER TABLE `billing`
                ADD `phone` varchar(255) COLLATE 'utf8_general_ci' NOT NULL AFTER `last_name`;";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m160406_154519_add_billing_fields_adress cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
