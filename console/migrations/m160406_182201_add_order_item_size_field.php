<?php

use yii\db\Migration;

class m160406_182201_add_order_item_size_field extends Migration
{
    public function up()
    {
        $sql = "ALTER TABLE `order_item`
            ADD `size` varchar(255) NOT NULL;";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m160406_182201_add_order_item_size_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
