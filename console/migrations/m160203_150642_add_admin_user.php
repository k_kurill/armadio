<?php

use yii\db\Schema;
use yii\db\Migration;

class m160203_150642_add_admin_user extends Migration
{
    public $sql = <<<'SQL'
                INSERT INTO `user` (`id`, `username`, `email`, `password_hash`, `auth_key`, `confirmed_at`, `unconfirmed_email`, `blocked_at`, `created_at`, `updated_at`) VALUES
                (1, 'admin', 'admin@admin.admin', '$2y$10$X3e1bSfdERuW7ibb83.nkOxKYphLEQreg974qhEHXEmRUVe4YrJBu', 'vfBltkS1esRKTKUdp8Obtw1h7e_grFQq', 1454510509, '0', NULL, 1454510503, 1454510503);
SQL;


    public function up()
    {
        $this->execute($this->sql);
    }

    public function down()
    {
        echo "m160203_150642_add_admin_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
