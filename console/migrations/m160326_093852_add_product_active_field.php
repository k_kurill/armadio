<?php

use yii\db\Migration;

class m160326_093852_add_product_active_field extends Migration
{
    public function up()
    {
        $sql = "ALTER TABLE `product`
                ADD `active` tinyint(1) NOT NULL DEFAULT '0' AFTER `id`;";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m160326_093852_add_product_active_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
