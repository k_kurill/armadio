<?php

use yii\db\Migration;

class m160409_093633_add_mail_auth_roles extends Migration
{
    public function up()
    {
        $sql = "SET foreign_key_checks = 0;
            SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';           

            INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
            ('MailMeneger',  1,  '', NULL,   NULL,   1459122172, 1459122203),
            ('edit_mail',  2,  '', NULL,   NULL,   1459104092, 1459104092);

            INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
           
            ('MailMeneger',  'edit_mail'),
            
            ('Administrator',   'MailMeneger');";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m160409_093633_add_mail_auth_roles cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
