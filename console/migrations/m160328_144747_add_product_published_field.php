<?php

use yii\db\Migration;

class m160328_144747_add_product_published_field extends Migration
{
    public function up()
    {
        $sql = "ALTER TABLE `product`
                ADD `published_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `language_id`;";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m160328_144747_add_product_published_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
