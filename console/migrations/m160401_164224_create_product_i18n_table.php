<?php

use yii\db\Migration;

/**
 * Handles the creation for table `product_i18n_table`.
 */
class m160401_164224_create_product_i18n_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $sql = "CREATE TABLE `product_i18n_data` (
            `id` bigint unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
            `product_id` int(11) NOT NULL,
            `language_id` varchar(5) COLLATE 'utf8_unicode_ci' NOT NULL,
            `name` varchar(255) NOT NULL,
            `meta_title` varchar(255) NOT NULL,
            `meta_keyword` varchar(255) NOT NULL,
            `meta_description` varchar(255) NOT NULL,
            `short_description` varchar(255) NOT NULL,
            `description` text NOT NULL,
            FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE,
            FOREIGN KEY (`language_id`) REFERENCES `language` (`language_id`) ON DELETE CASCADE
          ) ENGINE='InnoDB' COLLATE 'utf8_general_ci';

          (0.680 s)";
        $this->execute($sql);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('product_i18n_table');
    }
}
