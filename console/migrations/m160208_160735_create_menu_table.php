<?php

use yii\db\Schema;
use yii\db\Migration;

class m160208_160735_create_menu_table extends Migration
{
    public function up() {
        $path = realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..');
        exec('php ' . $path . DIRECTORY_SEPARATOR . 'yii migrate/up --migrationPath=@vendor/pceuropa/yii2-menu/migrations --interactive=0');
    }

    public function down() {
        echo "m160208_160735_create_menu_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
