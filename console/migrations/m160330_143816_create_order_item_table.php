<?php

use yii\db\Migration;

/**
 * Handles the creation for table `order_item_table`.
 */
class m160330_143816_create_order_item_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
       $sql = "CREATE TABLE `order_item` (
            `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
            `order_id` int(11) NOT NULL,
            `product_id` int(11) NOT NULL,
            `name` varchar(255) COLLATE 'utf8_general_ci' NOT NULL,
            `price` decimal(10,2) NOT NULL,
            `qty` int unsigned zerofill NOT NULL,
            FOREIGN KEY (`order_id`) REFERENCES `order` (`id`) ON DELETE RESTRICT,
            FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE RESTRICT
          ) ENGINE='InnoDB';";
       $this->execute($sql);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('order_item_table');
    }
}
