<?php

use yii\db\Schema;
use yii\db\Migration;

class m160208_151758_add_lng_field_to_page_table extends Migration
{
    public function up()
    {
        $table_name =  Yii::$app->getModule('pages')->tableName;
        $this->execute("ALTER TABLE `$table_name` ADD `lng` VARCHAR( 5 ) NOT NULL");
        $this->execute("UPDATE `$table_name` SET `lng`='en-GB' WHERE 1");
        $this->execute("ALTER TABLE `$table_name` DROP INDEX `alias`, ADD UNIQUE `alias` (`alias`, `lng`) USING BTREE;");
        $this->execute("ALTER TABLE `$table_name` DROP PRIMARY KEY, ADD PRIMARY KEY (`id`, `lng`) USING BTREE;");
        
    }

    public function down()
    {
        echo "m160208_151758_add_lng_field_to_page_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
