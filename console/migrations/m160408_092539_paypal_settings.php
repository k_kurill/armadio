<?php

use yii\db\Migration;

class m160408_092539_paypal_settings extends Migration
{
    public function up()
    {
        $this->dropTable('paypal_settings');
        
        $sql = "DELETE FROM `migration`
            WHERE ((`version` = 'm151006_125230_paypal_settings_table' AND `version` = 'm151006_125230_paypal_settings_table' COLLATE utf8mb4_bin) OR (`version` = 'm151007_080907_paypal_express_payments' AND `version` = 'm151007_080907_paypal_express_payments' COLLATE utf8mb4_bin) OR (`version` = 'm160210_070813_subscription_express_base' AND `version` = 'm160210_070813_subscription_express_base' COLLATE utf8mb4_bin) OR (`version` = 'm160212_120959_paypal_subscription_express_new_fields' AND `version` = 'm160212_120959_paypal_subscription_express_new_fields' COLLATE utf8mb4_bin) OR (`version` = 'm160212_120960_paypal_subscription_express_new_fields' AND `version` = 'm160212_120960_paypal_subscription_express_new_fields' COLLATE utf8mb4_bin) OR (`version` = 'm160212_120961_paypal_subscription_express_new_fields' AND `version` = 'm160212_120961_paypal_subscription_express_new_fields' COLLATE utf8mb4_bin) OR (`version` = 'm160212_120962_paypal_subscription_express_new_fields' AND `version` = 'm160212_120962_paypal_subscription_express_new_fields' COLLATE utf8mb4_bin) OR (`version` = 'm160212_120963_paypal_subscription_express_new_fields' AND `version` = 'm160212_120963_paypal_subscription_express_new_fields' COLLATE utf8mb4_bin));";
        $this->execute($sql);
        $path = realpath(dirname(__FILE__).DIRECTORY_SEPARATOR.'..' . DIRECTORY_SEPARATOR . '..');
        exec('php ' .$path.DIRECTORY_SEPARATOR.'yii migrate/up --migrationPath=@vendor/achertovsky/paypal-yii2/migrations --interactive=0');
        
    }

    public function down()
    {
        echo "m160408_092539_paypal_settings cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
