<?php

use yii\db\Migration;

/**
 * Handles the creation for table `drends_video_table`.
 */
class m160330_170424_create_drends_video_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $table = '{{%brands_video}}';
        $tableBrands = '{{%brands}}';
        $this->createTable($table, [
            'id' => $this->primaryKey(),
            'brand_id' => $this->integer(12),
            'link' => $this->string(255),
        ]);
        $this->addForeignKey('brands_id',$table,'brand_id',$tableBrands,'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('brands_video');
    }
}
