<?php

use yii\db\Migration;

/**
 * Handles adding cover to table `category`.
 */
class m160406_155643_add_cover_to_category extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%category}}','cover_image',$this->string(255));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    }
}
