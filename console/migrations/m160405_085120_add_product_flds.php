<?php

use yii\db\Migration;

class m160405_085120_add_product_flds extends Migration
{
    public function up()
    {
        $this->addColumn('product_i18n_data', 'leather', $this->text());
        $this->addColumn('product_i18n_data', 'measurements', $this->text());
    }

    public function down()
    {
        echo "m160405_085120_add_product_flds cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
