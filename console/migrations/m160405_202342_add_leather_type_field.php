<?php

use yii\db\Migration;

class m160405_202342_add_leather_type_field extends Migration
{
    public function up()
    {
        $sql = "ALTER TABLE `product_i18n_data`
            ADD `leather_type` varchar(255) COLLATE 'utf8_general_ci' NOT NULL AFTER `color_title`;";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m160405_202342_add_leather_type_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
