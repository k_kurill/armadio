<?php

use yii\db\Migration;

class m160330_155941_alter_qty_field extends Migration
{
    public function up()
    {
        $sql = "ALTER TABLE `order_item`
            CHANGE `qty` `qty` int(10) unsigned NOT NULL AFTER `price`;";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m160330_155941_alter_qty_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
