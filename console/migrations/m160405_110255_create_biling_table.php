<?php

use yii\db\Migration;

/**
 * Handles the creation for table `biling_table`.
 */
class m160405_110255_create_biling_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $sql = "CREATE TABLE `billing` (
            `id` int NOT NULL,
            `order_id` int(11) NOT NULL,
            `first_name` varchar(255) NOT NULL,
            `last_name` varchar(255) NOT NULL,
            `month` varchar(255) NOT NULL,
            `year` int NOT NULL,
            `cvv` int NOT NULL,
            `zip` varchar(255) NOT NULL,
            `created` varchar(255) NOT NULL,
            `responce` text NOT NULL,
            FOREIGN KEY (`order_id`) REFERENCES `order` (`id`) ON DELETE RESTRICT
          ) ENGINE='InnoDB';";
        $this->execute($sql);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('biling_table');
    }
}
