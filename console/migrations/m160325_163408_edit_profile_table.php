<?php

use yii\db\Migration;

class m160325_163408_edit_profile_table extends Migration
{
    public function up()
    {
        $table = '{{%profile}}';
        $columns = [
            'image'=>$this->string(256),
            'country'=>$this->string(256),
            'phone'=>$this->string(256),
            'state'=>$this->string(256),
            'zip'=>$this->integer(12),
            'address'=>$this->string(255),
            'last_name'=>$this->string(256)
        ];
        foreach($columns as $name=>$type){
            $this->addColumn($table,$name,$type);
        }
    }


}
