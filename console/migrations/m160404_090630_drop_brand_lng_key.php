<?php

use yii\db\Migration;

/**
 * Handles the dropping for table `brand_lng_key`.
 */
class m160404_090630_drop_brand_lng_key extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $sql = "ALTER TABLE `brands`
            DROP `name`,
            DROP `brand`,
            DROP `language_id`,
            DROP `meta_keyword`,
            DROP `meta_description`,
            DROP `description`;";
        $this->execute($sql);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->createTable('brand_lng_key', [
            'id' => $this->primaryKey(),
        ]);
    }
}
