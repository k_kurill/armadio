<?php

use yii\db\Migration;

class m160404_090019_add_color_title_fields extends Migration
{
    public function up()
    {
        $this->dropColumn('product', 'color_title');
        $sql = "ALTER TABLE `product_i18n_data`
            ADD `color_title` varchar(255) COLLATE 'utf8_general_ci' NOT NULL AFTER `short_description`;";
        $this->execute($sql);
        
    }

    public function down()
    {
        echo "m160404_090019_add_color_title_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
