<?php

use yii\db\Migration;

class m160330_170339_upgrate_brends_table extends Migration
{
    public function up()
    {
        $table = '{{%brands}}';
        $this->dropColumn($table,'video');
    }

    public function down()
    {
        echo "m160330_170339_upgrate_brends_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
