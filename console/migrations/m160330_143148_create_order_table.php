<?php

use yii\db\Migration;

/**
 * Handles the creation for table `order_table`.
 */
class m160330_143148_create_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $sql = "CREATE TABLE `order` (
            `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
            `user_id` int NOT NULL,
            `created` timestamp NOT NULL,
            `status` enum('completed','shipped','on_hold','canceled') NOT NULL,
            `customer_note` text COLLATE 'utf8_general_ci' NOT NULL,
            `order_note` text COLLATE 'utf8_general_ci' NOT NULL
          ) ENGINE='InnoDB' COLLATE 'utf8_general_ci';";
        $this->execute($sql);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('order_table');
    }
}
