<?php

use yii\db\Migration;

/**
 * Handles adding tree_description to table `artisans`.
 */
class m160406_081331_add_tree_description_to_artisans extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $table = '{{%brand_i18n_data}}';
        $this->addColumn($table,'description1',$this->text());
        $this->addColumn($table,'description2',$this->text());
        $this->addColumn($table,'description3',$this->text());
        $this->addColumn($table,'description4',$this->text());

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    }
}
