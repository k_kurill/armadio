<?php

use yii\db\Migration;

/**
 * Handles the dropping for table `shipping_user_index`.
 */
class m160330_145925_drop_shipping_user_index extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropForeignKey('shipping_ibfk_2', 'shipping');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        return false;
    }
}
