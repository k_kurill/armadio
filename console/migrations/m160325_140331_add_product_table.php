<?php

use yii\db\Migration;

class m160325_140331_add_product_table extends Migration
{
    public function up()
    {
        $sql = "CREATE TABLE `product` (
            `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
            `name` varchar(255) COLLATE 'utf8_general_ci' NOT NULL,
            `main_image` varchar(255) COLLATE 'utf8_general_ci' NOT NULL,
            `language_id` varchar(5) COLLATE 'utf8_general_ci' NOT NULL,
            `meta_title` varchar(255) COLLATE 'utf8_general_ci' NOT NULL,
            `meta_keyword` varchar(255) COLLATE 'utf8_general_ci' NOT NULL,
            `meta_description` varchar(255) COLLATE 'utf8_general_ci' NOT NULL,
            `short_description` varchar(511) COLLATE 'utf8_general_ci' NOT NULL,
            `description` text COLLATE 'utf8_general_ci' NOT NULL
          ) ENGINE='InnoDB' COLLATE 'utf8_general_ci';";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m160325_140331_add_product_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
