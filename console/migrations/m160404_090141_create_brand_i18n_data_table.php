<?php

use yii\db\Migration;

/**
 * Handles the creation for table `brand_i18n_data_table`.
 */
class m160404_090141_create_brand_i18n_data_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $sql = "CREATE TABLE `brand_i18n_data` (
            `id` bigint unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
            `brand_id` int(12) NOT NULL,
            `language_id` varchar(5) COLLATE 'utf8_unicode_ci' NOT NULL,
            `name` varchar(255) NOT NULL,
            `brand` varchar(255) NOT NULL,

            `meta_keyword` varchar(255) NOT NULL,
            `meta_description` varchar(255) NOT NULL,

            `description` text NOT NULL,
            FOREIGN KEY (`language_id`) REFERENCES `language` (`language_id`) ON DELETE CASCADE
          ) ENGINE='InnoDB' COLLATE 'utf8_general_ci';";
        $this->execute($sql);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('brand_i18n_data');
    }
}
