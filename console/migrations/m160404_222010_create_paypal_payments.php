<?php

use yii\db\Migration;

/**
 * Handles the creation for table `paypal_payments`.
 */
class m160404_222010_create_paypal_payments extends Migration
{
    public function up()
    {
        $this->createTable('{{%paypal_settings}}', [
                'id' => $this->primaryKey(),
                'clientId' => $this->string(500),
                'clientSecret' => $this->string(500),
                'merchant_email' => $this->string(500),
                'mode' => $this->string(255),
            ]);
        $this->insert('{{%paypal_settings}}', [
            'clientId' => 'ASgIzmCIXOofNFGFYFMOAtMmAzv9hHQvtl2C9kI3VlkMxoYH8UjJBk53rYoUIF_BrGzV3I_LTuk93C9X',
            'clientSecret' => 'ELyqmd9oKVtI4iobVPBVqXeou-SmcjxXMiTPThuvRP14MiERGMq_IPVw2m_V2KwyId2fddiI281vEMjE',
            'merchant_email' => 'devm@gmail.com',
            'mode' => 'sandbox',
        ]);
    }

    public function down()
    {
        
        $this->dropTable('{{%paypal_settings}}');
        
    }
}
