<?php

use yii\db\Migration;

/**
 * Handles the dropping for table `product_lng_key`.
 */
class m160401_164441_drop_product_lng_key extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $sql = "ALTER TABLE `product`
            CHANGE `brand_id` `brand_id` int(12) NULL AFTER `variant_of`,
            CHANGE `sku_code` `sku_code` varchar(255) COLLATE 'utf8_general_ci' NOT NULL AFTER `brand_id`,
            CHANGE `price` `price` decimal(10,2) NOT NULL AFTER `sku_code`,
            DROP `name`,
            DROP `language_id`,
            CHANGE `published_at` `published_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `price`,
            DROP `meta_title`,
            DROP `meta_keyword`,
            DROP `meta_description`,
            DROP `short_description`,
            DROP `description`;";
        $this->execute($sql);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        return false;
    }
}
