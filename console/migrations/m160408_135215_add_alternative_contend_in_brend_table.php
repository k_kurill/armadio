<?php

use yii\db\Migration;

class m160408_135215_add_alternative_contend_in_brend_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%brand_i18n_data}}','alternative_contend',$this->text());
    }

    public function down()
    {
        echo "m160408_135215_add_alternative_contend_in_brend_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
