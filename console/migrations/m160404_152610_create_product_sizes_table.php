<?php

use yii\db\Migration;

/**
 * Handles the creation for table `product_sizes_table`.
 */
class m160404_152610_create_product_sizes_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $sql = "CREATE TABLE `product_size` (
            `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
            `product_id` int(11) NOT NULL,
            `size` varchar(255) NOT NULL,
            `count` int NOT NULL,
            FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE
          ) ENGINE='InnoDB';";
        $this->execute($sql);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('product_sizes_table');
    }
}
