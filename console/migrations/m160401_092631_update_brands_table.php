<?php

use yii\db\Migration;

class m160401_092631_update_brands_table extends Migration
{
    public function up()
    {
        $table = '{{%brands}}';
        $this->addColumn($table,'language_id',$this->string(5));
        $this->addColumn($table,'variant_of',$this->integer(11));
    }

    public function down()
    {
        echo "m160401_092631_update_brands_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
