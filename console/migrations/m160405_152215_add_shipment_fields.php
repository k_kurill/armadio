<?php

use yii\db\Migration;

class m160405_152215_add_shipment_fields extends Migration
{
    public function up()
    {
        $sql = "ALTER TABLE `shipping`
            ADD `address2` varchar(255) COLLATE 'utf8_general_ci' NOT NULL AFTER `address`,
            ADD `phone` varchar(255) COLLATE 'utf8_general_ci' NOT NULL AFTER `city`;";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m160405_152215_add_shipment_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
