<?php

use yii\db\Migration;

class m160404_085439_add_color_prducts_fields extends Migration
{
    public function up()
    {
        $sql = "ALTER TABLE `product`
                ADD `color` varchar(255) COLLATE 'utf8_general_ci' NOT NULL AFTER `sku_code`,
                ADD `color_title` varchar(255) COLLATE 'utf8_general_ci' NOT NULL AFTER `color`;";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m160404_085439_add_color_prducts_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
