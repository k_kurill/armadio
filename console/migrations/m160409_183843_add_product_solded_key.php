<?php

use yii\db\Migration;

class m160409_183843_add_product_solded_key extends Migration
{
    public function up()
    {
        $this->addColumn('product', 'solded', $this->boolean());
    }

    public function down()
    {
        echo "m160409_183843_add_product_solded_key cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
