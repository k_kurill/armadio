<?php

use yii\db\Migration;

class m160326_080801_add_product_price extends Migration
{
    public function up()
    {
        $sql = "ALTER TABLE `product`
                ADD `price` decimal(10,2) NOT NULL AFTER `sku_code`;";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m160326_080801_add_product_price cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
