<?php

use yii\db\Migration;

class m160326_130807_remove_main_image_attr extends Migration
{
    public function up()
    {
        $this->dropColumn('product', 'main_image');
    }

    public function down()
    {
        echo "m160326_130807_remove_main_image_attr cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
