<?php

use yii\db\Migration;

class m160404_171945_add_image_role extends Migration
{
    public function up()
    {
        $sql ="ALTER TABLE `image`
            ADD `role` varchar(255) NULL;";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m160404_171945_add_image_role cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
