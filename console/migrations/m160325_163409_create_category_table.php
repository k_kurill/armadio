<?php
use yii\db\Migration;

class m160325_163409_create_category_table extends Migration
{
    public function up()
    {
        $table = '{{%category}}';
        $product = '{{%product}}';
        $product_table = '{{%product_category}}';
        $this->execute(file_get_contents(Yii::getAlias('@vendor/kartik-v/yii2-tree-manager/schema' . '/tree.sql')));
        $this->renameTable('tbl_tree', $table);
        $this->createTable($product_table, [
            'id' => $this->primaryKey(12),
            'product_id' => $this->integer(12),
            'category_id' => $this->integer(12),
        ]);
        $this->addForeignKey('product_id',$product_table,'product_id',$product,'id');
        $this->addForeignKey('category_id',$product_table,'category_id',$table,'id');
    }

}