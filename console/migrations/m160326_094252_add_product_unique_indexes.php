<?php

use yii\db\Migration;

class m160326_094252_add_product_unique_indexes extends Migration
{
    public function up()
    {
        $sql = "ALTER TABLE `product`
                ADD UNIQUE `sku_code_language_id` (`sku_code`, `language_id`),
                DROP INDEX `language_id`,
                DROP INDEX `sku_code`;";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m160326_094252_add_product_unique_indexes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
