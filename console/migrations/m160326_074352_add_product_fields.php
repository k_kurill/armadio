<?php

use yii\db\Migration;

class m160326_074352_add_product_fields extends Migration
{
    public function up()
    {
        $query = "ALTER TABLE `product`
                    ADD `variant_of` int(11) NULL DEFAULT NULL AFTER `id`,
                    ADD `sku_code` varchar(255) COLLATE 'utf8_general_ci' NOT NULL AFTER `variant_of`;";
        $this->execute($query);
        $this->createIndex('product_variant', 'product', 'variant_of');
        $this->createIndex('sku_code', 'product', 'sku_code');
    }

    public function down()
    {
        echo "m160326_074352_add_product_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
