<?php

use yii\db\Migration;

class m160404_120845_add_auth_rules extends Migration
{
    public function up()
    {
        $sql = "SET foreign_key_checks = 0;
            SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

            INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
            ('Administrator',	'1',	1459122384);

            INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
            ('Administrator',	1,	'Global site administrator',	NULL,	NULL,	1458754135,	1459122229),
            ('ContentManager',	1,	'',	NULL,	NULL,	1459122172,	1459122203),
            ('edit_pages',	2,	'',	NULL,	NULL,	1459104092,	1459104092),
            ('edit_translate',	2,	'',	NULL,	NULL,	1459121469,	1459121469),
            ('manage_shop',	2,	'',	NULL,	NULL,	1458925488,	1458925488),
            ('ShopManager',	1,	'',	NULL,	NULL,	1459122182,	1459122214);

            INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
            ('Administrator',	'ContentManager'),
            ('ContentManager',	'edit_pages'),
            ('ContentManager',	'edit_translate'),
            ('ShopManager',	'manage_shop'),
            ('Administrator',	'ShopManager');";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m160404_120845_add_auth_rules cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
