<?php

use yii\db\Schema;
use yii\db\Migration;

class m160204_081108_add_translate extends Migration
{
    public function up()
    {
        $path = realpath(dirname(__FILE__).DIRECTORY_SEPARATOR.'..' . DIRECTORY_SEPARATOR . '..');
        exec('php ' .$path.DIRECTORY_SEPARATOR.'yii migrate/up --migrationPath=@vendor/lajax/yii2-translate-manager/migrations --interactive=0');
    }

    public function down()
    {
        echo "m160204_081108_add_translate cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
