<?php

use yii\db\Migration;

/**
 * Handles the creation for table `tax_table`.
 */
class m160408_131010_create_tax_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tax_table', [
            'id' => $this->primaryKey(),
            'state_title' => $this->string('255'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tax_table');
    }
}
