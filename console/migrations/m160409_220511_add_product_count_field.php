<?php

use yii\db\Migration;

class m160409_220511_add_product_count_field extends Migration
{
    public function up()
    {
        $this->dropColumn('product', 'solded');
        $this->addColumn('product', 'count', $this->integer());
    }

    public function down()
    {
        echo "m160409_220511_add_product_count_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
