<?php

class m160329_144747_edit_product_category_table extends \dektrium\user\migrations\Migration{
    public function up(){
        $table = '{{%product_category}}';
        $this->createIndex('product_category', $table, ['product_id','category_id'], true);
    }
}