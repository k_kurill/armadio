<?php

use yii\db\Migration;

class m160404_081018_add_part_product_field extends Migration
{
    public function up()
    {
        $sql = "ALTER TABLE `product`
                ADD `part_of` int(11) NULL AFTER `variant_of`;";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m160404_081018_add_part_product_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
