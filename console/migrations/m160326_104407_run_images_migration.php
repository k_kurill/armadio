<?php

use yii\db\Migration;

class m160326_104407_run_images_migration extends Migration
{
    public function up()
    {
        $path = realpath(dirname(__FILE__).DIRECTORY_SEPARATOR.'..' . DIRECTORY_SEPARATOR . '..');
        exec('php ' .$path.DIRECTORY_SEPARATOR.'yii migrate/up --migrationPath=@vendor/costa-rico/yii2-images/migrations --interactive=0');
    }

    public function down()
    {
        echo "m160326_104407_run_images_migration cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
