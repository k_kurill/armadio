<?php

use yii\db\Migration;

class m160405_225746_add_billing_fields extends Migration
{
    public function up()
    {
        $sql = "ALTER TABLE `billing`
            ADD `card_num` varchar(255) COLLATE 'utf8_general_ci' NOT NULL AFTER `month`;";
        $this->execute($sql);
        
        $sql = "ALTER TABLE `billing`
            CHANGE `id` `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;";
        $this->execute($sql);
        $sql = "ALTER TABLE `billing`
            ADD `card_type` varchar(255) COLLATE 'utf8_general_ci' NOT NULL AFTER `card_num`;";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m160405_225746_add_billing_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
