<?php

use yii\db\Migration;

class m160409_150029_add_order_delete extends Migration
{
    public function up()
    {
        $this->addColumn('order', 'deleted', $this->boolean());
        $sql = "ALTER TABLE `order`
            CHANGE `created` `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `user_id`;";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m160409_150029_add_order_delete cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
