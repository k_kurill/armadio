<?php

use yii\db\Migration;

class m160325_163407_add_product_indexes extends Migration
{
    public function up()
    {
        $sql = "ALTER TABLE `product`
                ADD INDEX `language_id` (`language_id`);";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m160325_163407_add_product_indexes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
