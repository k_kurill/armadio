<?php
class m160330_163150_insert_brands_table extends \yii\db\Migration{
    public function up(){
        $table = '{{%brands}}';
        $columns = [
            'id'=>$this->primaryKey(12),
            'name'=>$this->string(255),
            'brand'=>$this->string(255),
            'location'=>$this->string(255),
            'video'=>$this->string(255),
            'description'=>$this->text(),
            'meta_keyword'=>$this->string(255),
            'meta_description'=>$this->string(255),


        ];
        $this->createTable($table,$columns);
    }
}