<?php
namespace common\models;

use dektrium\user\models\User as BaseUser;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends BaseUser {
    public function getProfile()
    {
        return $this->hasOne(Profiless::className(), ['user_id' => 'id']);
    }
}
