<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "shipping".
 *
 * @property integer $order_id
 * @property integer $user_id
 * @property string $name
 * @property string $email
 * @property string $address
 * @property string $city
 * @property string $state
 * @property string $zip
 *
 * @property Order $order
 * @property User $user
 */
class Shipping extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shipping';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'name', 'email', 'address', 'city', 'state', 'zip', 'phone'], 'required'],
            [['order_id', 'user_id'], 'integer'],
            [['name', 'email', 'address', 'address2', 'city', 'state', 'zip'], 'string', 'max' => 255],
            [['email'], 'email'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            //[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            ['user_id', 'integer', 'skipOnEmpty'=>true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id' => Yii::t('shipping', 'Order ID'),
            'user_id' => Yii::t('shipping', 'User ID'),
            'name' => Yii::t('shipping', 'Name'),
            'email' => Yii::t('shipping', 'Email'),
            'address' => Yii::t('shipping', 'Address'),
            'city' => Yii::t('shipping', 'City'),
            'state' => Yii::t('shipping', 'State'),
            'zip' => Yii::t('shipping', 'Zip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return ShippingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ShippingQuery(get_called_class());
    }
}
