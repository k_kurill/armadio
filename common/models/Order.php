<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $created
 * @property string $status
 * @property string $customer_note
 * @property string $order_note
 *
 * @property Shipping $shipping
 */
class Order extends \yii\db\ActiveRecord {

    /** Triggered on position put */
    const ON_HOLD = 'on_hold';
    const COMPLETED = 'completed';
    const SHIPPED = 'shipped';
    const CANCELED = 'canceled';

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['user_id', 'status'], 'required'],
            [['user_id'], 'integer'],
            [['created'], 'safe'],
            [['status', 'customer_note', 'order_note'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('order', 'ID'),
            'user_id' => Yii::t('order', 'User ID'),
            'created' => Yii::t('order', 'Created'),
            'status' => Yii::t('order', 'Status'),
            'customer_note' => Yii::t('order', 'Customer Note'),
            'order_note' => Yii::t('order', 'Order Note'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShipping() {
        return $this->hasOne(Shipping::className(), ['order_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBilling() {
        return $this->hasOne(Billing::className(), ['order_id' => 'id']);
    }
    
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getItems() {
        return $this->hasMany(OrderItem::className(), ['order_id' => 'id']);
    }
    public function getTotalCount(){
        $count = 0;
        foreach ($this->items as $position){
            $count += $position->qty;
        }
        return $count;
    }
    public function getTaxSum(){
        return 0;
    }


    public function getItemsPrice(){
        $price = 0;
        foreach ($this->items as $position){
            $price += $position->price * $position->qty;
        }
        return $price;
    }
    
    public function getUsername(){
        if(!$this->user){
            return 'Guest';
        }else{
            return $this->user->username;
        }
    }
    
    public function getShippingPrice(){
        return 0;
    }
    
    public function getTotalPrice(){
        return $this->getItemsPrice() + $this->getShippingPrice() + $this->getTaxSum();
    }
    
    public function getPaypal(){
        return $this->hasOne(PaypalExpressPayment::className(), ['user_id'=>'id']);
    }
    
    
    public function getStatusesArray(){
        return [
            self::ON_HOLD   => $this->getAttributeLabel(self::ON_HOLD),
            self::COMPLETED => $this->getAttributeLabel(self::COMPLETED),
            self::SHIPPED   => $this->getAttributeLabel(self::SHIPPED),
            self::CANCELED  => $this->getAttributeLabel(self::CANCELED),
        ];
    }
    
    public function getAdminCssClass(){
        switch ($this->status){
            case self::ON_HOLD:
                return 'warning';
            case self::COMPLETED:
                return 'success';
            case self::SHIPPED:
                return 'info';
            case self::CANCELED:
                return 'danger';
            default:
                return 'default';
        }
    }

    /**
     * @inheritdoc
     * @return OrderQuery the active query used by this AR class.
     */
    public static function find() {
        return new OrderQuery(get_called_class());
    }

    
    public function delete() {
        //parent::delete();
        $this->deleted = 1;
        $this->save();
    }
}
