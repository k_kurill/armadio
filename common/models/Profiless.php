<?php
namespace common\models;
use dektrium\user\models\Profile as BaseProfile;
use Yii;
class Profiless extends BaseProfile{
    public function rules()
    {
        return [
            //'bioString' => ['bio', 'string'],
            'publicEmailPattern' => ['public_email', 'email'],
            'phone'=>['phone','string','max'=>255],
            'zip'=>['zip','integer','max'=>12],
            'country'=>[['country','state','address'],'string','max'=>255],
            //'gravatarEmailPattern' => ['gravatar_email', 'email'],
           // 'websiteUrl' => ['website', 'url'],
            'nameLength' => [['name','last_name'], 'string', 'max' => 255],
            'publicEmailLength' => ['public_email', 'string', 'max' => 255],
            //'gravatarEmailLength' => ['gravatar_email', 'string', 'max' => 255],
            'locationLength' => ['location', 'string', 'max' => 255],
            //'websiteLength' => ['website', 'string', 'max' => 255],
        ];
    }

    /** @inheritdoc */
    public function attributeLabels()
    {
        return [
            'name'           => Yii::t('user', 'Name'),
            'last_name'           => Yii::t('user', 'Lasts name'),
            'public_email'   => Yii::t('user', 'Email'),
            'phone'=>Yii::t('user','Telephone'),
            'zip'=>Yii::t('user','Zip'),
            'country'=>Yii::t('user','Country'),
            'state'=>Yii::t('user','State'),
            'address'=>Yii::t('user','Address'),


            'location'       => Yii::t('user', 'Location'),

        ];
    }
}