<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "billing".
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $first_name
 * @property string $last_name
 * @property string $month
 * @property integer $year
 * @property integer $cvv
 * @property string $zip
 * @property string $created
 * @property string $responce
 * @property string $card_num
 *
 * @property Order $order
 */
class Billing extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'billing';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'month', 'year'], 'required'],
            [['year', 'month', 'cvv', 'card_type'], 'integer'],
            [['card_num', 'card_type', 'address2'], 'string'],
            ['card_num', 'frontend\components\CreditCardValidator'],
            [['year', 'month', 'cvv', 'card_type'], 'integer'],
            [['month', 'year'], 'validateDate'],
            [['first_name', 'last_name', 'month', 'zip'], 'string', 'max' => 255],
            //[['order_id'], 'exist', 'skipOnError' => false, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }
    
    public function beforeValidate() {
        parent::beforeValidate();
        if($this->validateDate($this->month, $this->year)){
            return parent::beforeValidate();
        }else{
            $this->addError('year', 'Wrong date');
            $this->addError('month', 'Wrong date');
            return false;
        }
        
    }
    
    public function validateDate($creditCardExpiredMonth, $creditCardExpiredYear)
    {
        $currentMonth = intval(date('n'));
        $currentYear = intval(date('Y'));
        if (is_scalar($creditCardExpiredMonth)) $creditCardExpiredMonth = intval($creditCardExpiredMonth);
        if (is_scalar($creditCardExpiredYear)) $creditCardExpiredYear = intval($creditCardExpiredYear);
        return
        is_integer($creditCardExpiredMonth) && $creditCardExpiredMonth >= 1 && $creditCardExpiredMonth <= 12 &&
        is_integer($creditCardExpiredYear) &&  ($creditCardExpiredYear > $currentYear || ($creditCardExpiredYear==$currentYear && $creditCardExpiredMonth>$currentMonth) ) &&  $creditCardExpiredYear < $currentYear + 10
        ;
    }


        /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('billing', 'ID'),
            'order_id' => Yii::t('billing', 'Order ID'),
            'first_name' => Yii::t('billing', 'First Name'),
            'last_name' => Yii::t('billing', 'Last Name'),
            'month' => Yii::t('billing', 'Month'),
            'year' => Yii::t('billing', 'Year'),
            'cvv' => Yii::t('billing', 'Cvv'),
            'zip' => Yii::t('billing', 'Zip'),
            'created' => Yii::t('billing', 'Created'),
            'responce' => Yii::t('billing', 'Responce'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}
