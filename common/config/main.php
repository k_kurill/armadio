<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language' => 'en-US',
    'modules'=>[
        'user' => [
            'class' => 'dektrium\user\Module',

        ],
        'rbac' => [
            'class' => 'dektrium\rbac\Module',
        ],
        'payment' => [
            'class' => 'achertovsky\paypal\Module',
        ],
        'yii2images' => [
            'class' => 'rico\yii2images\Module',
            //be sure, that permissions ok 
            //if you cant avoid permission errors you have to create "images" folder in web root manually and set 777 permissions
            'imagesStorePath' => '@frontend/web/images/store', //path to origin images
            'imagesCachePath' => '@frontend/web/images/cache', //path to resized copies
            'graphicsLibrary' => 'GD', //but really its better to use 'Imagick' 
            //'className'       => 'backend\models\Image'
            'placeHolderPath' => '@frontend/web/images/placeholder.gif', // if you want to get placeholder when image not exists, string will be processed by Yii::getAlias
        ],
        
        'menu' => [
            'class' => '\pceuropa\menu\Module',
            'controllerMap' => [
                'index' => [
                        'class' => '\pceuropa\menu\controllers\IndexController',
                        'as access' => [
                            'class' => yii\filters\AccessControl::className(),
                            'rules' => [
                                [
                                    'allow' => false,
                                    'roles' => ['*'],
                                ],
                            ],
                        ],
                    ],
                'default' => [
                        'class' => '\pceuropa\menu\controllers\DefaultController',
                        'as access' => [
                            'class' => yii\filters\AccessControl::className(),
                            'rules' => [
                                [
                                    'allow' => false,
                                    'roles' => ['*'],
                                ],
                            ],
                        ],
                    ],
            ],
        ],
        'pages' => [
            'class' => 'common\modules\pagemanager\Module',
            'tableName'=>'page',
            'controllerMap' => [
                'manager' => [
                    'class' => 'backend\modules\pagemanager\controllers\PagemanagerController',
                    'as access' => [
                        'class' => yii\filters\AccessControl::className(),
                        'rules' => [
                            [
                                'allow' => true,
                                'roles' => ['edit_pages'],
                            ],
                        ],
                    ],

                ],'default' => [
                    'class' => 'common\modules\pagemanager\controllers\DefaultController',

                    'viewPath' => '@frontend/views/pages/default',
               
                ],
            ],

            'viewPath' => '@vendor/bupy7/yii2-pages/views',
        ],
        
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'braintree' => [
            'class' => 'bryglen\braintree\Braintree',
            'environment' => 'production',
            'merchantId' => 'wq8db7nqnkxqqmz6',
            'publicKey' => 'pp3qh6wh3nvhvmm4',
            'privateKey' => 'ac81e770cca68f6a485c823fea48ae58',
        ],
        'urlManager' => [
            'class'=>'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\DbMessageSource',
                    'db' => 'db',
                    //'sourceLanguage' => 'xx-XX', // Developer language
                    'sourceMessageTable' => '{{%language_source}}',
                    'messageTable' => '{{%language_translate}}',
                    'cachingDuration' =>"19577",
                    'enableCaching' => !YII_DEBUG,
                ],
            ],
        ],
        'mailjet'=>[
            'class'=>'frontend\components\MailHelper',
            'API_KEY'=>'6a7fd291fe5b93d37d2ed0e42dfe3e8b',
            'SECRET_KEY'=>'a2ca1d35755bbaac6665e763d19ffab6',
            'email'=>'hello@armadiofashion.com',
            'config_pay_template_id'=>"19577",
            'newsletter_list_id'=>"24144",
            

        ],
    ],
];
