<?php

namespace common\modules\pagemanager;

use Yii;
use yii\base\InvalidConfigException;

/**
 * Module implements CRUD with static pages.
 * 
 * @author Vasilij Belosludcev http://mihaly4.ru
 * @since 1.0.0
 */
class Module extends \bupy7\pages\Module
{
    
}
