<?php

namespace common\modules\pagemanager\models;

use bupy7\pages\models\Page as BasePage;
//use lajax\translatemanager\helpers\Language;

class Page extends BasePage{
    
    
    public function afterFind() {
        parent::afterFind();
    }
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
           // Language::saveMessage($this->name);
            return true;
        }
        return false;
    }
    
    public function rules()
    {
        return [
            [['title', 'lng'], 'required'],
            [['published'], 'boolean'],
            [['content'], 'string', 'max' => 65535],
            [['title', 'alias', 'title_browser'], 'string', 'max' => 255],
            [['meta_keywords'], 'string', 'max' => 200],
            [['meta_description'], 'string', 'max' => 160],
            [['alias', 'lng'], 'unique', 'targetAttribute' => ['alias', 'lng']],
            [['lng'], 'safe'],
        ];
    }

}