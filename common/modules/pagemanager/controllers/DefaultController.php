<?php
namespace common\modules\pagemanager\controllers;
use bupy7\pages\controllers\DefaultController as BaseController;
use common\modules\pagemanager\models\Page;
use yii\web\NotFoundHttpException;

class DefaultController extends BaseController{

    protected function findModel($alias)
    {
        $model = Page::find()->where([
            'alias' => $alias,
            'published' => Page::PUBLISHED_YES,
            'lng'=>\Yii::$app->language,
        ])->one();
        if ($model !== null) {
            return $model;
        }
        $model = Page::find()->where([
            'alias' => $alias,
            'published' => Page::PUBLISHED_YES,
            'lng'=>\Yii::$app->sourceLanguage,
        ])->one();
        if ($model !== null) {
            return $model;
        }
        throw new NotFoundHttpException(\Yii::t('Pages','PAGE_NOT_FOUND'));
    }
}
