<?php
namespace common\modules\shop\models;

use lajax\translatemanager\models\Language;

class BrandI18nData extends \yii\db\ActiveRecord{
    public static function tableName(){
        return 'brand_i18n_data';
    }
    public function rules()
    {
        return [
            [['brand_id', 'language_id', 'name', 'meta_keyword', 'meta_description', 'description'], 'required'],
            [['brand_id'], 'integer'],
            [['description','description1','description2','description3','description4','alternative_contend'], 'string'],
            [['language_id'], 'string', 'max' => 5],
            [['name', 'meta_keyword', 'meta_description','brand'], 'string', 'max' => 255],
            [['brand_id'], 'exist', 'skipOnError' => true, 'targetClass' => Brands::className(), 'targetAttribute' => ['brand_id' => 'id']],
            [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'language_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'brand_id' => 'Brand ID',
            'language_id' => 'Language ID',
            'name' => 'Name',
            'brand' => 'Brand',

            'meta_keyword' => 'Meta Keyword',
            'meta_description' => 'Meta Description',

            'description' => 'Description',
            'description1' => 'Description 1',
            'description2' => 'Description 2',
            'description3' => 'Description 3',
            'description4' => 'Description 4',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::className(), ['language_id' => 'language_id']);
    }

    /**
     * @inheritdoc
     * @return BrandI18nDataQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BrandI18nDataQuery(get_called_class());
    }

    public static function createTranslate($brand_id, $lng=null){
        if (!$lng) {
            $lng = \Yii::$app->sourceLanguage;
        }
        $data = self::find()->andWhere(['brand_id'=>$brand_id])->andWhere(['language_id'=>$lng])->one();
        if($data){
            return $data;
        }
        $data = new self;
        $data->brand_id = $brand_id;
        $data->language_id = $lng;
        $data->save(false);
        return $data;
    }
}