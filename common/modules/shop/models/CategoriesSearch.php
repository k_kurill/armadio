<?php
namespace common\modules\shop\models;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class CategoriesSearch extends Categories{
    public static function tableName(){
        return 'category';
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    public function search($params)
    {        $query = Categories::find();
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,

        ]);


        $query
            ->andFilterWhere(['lvl'=> $this->lvl])
            ->andFilterWhere(['root'=> $this->root])
            ->andFilterWhere(['lft'=> $this->lft])
            ->andFilterWhere(['rgt'=> $this->rgt])
            ->andFilterWhere(['active'=> $this->active])
            ->andFilterWhere(['like','name', $this->name])
          ;

        return $dataProvider;

    }
}