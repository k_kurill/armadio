<?php
namespace common\modules\shop\models;


use backend\models\Product;
use kartik\tree\models\Tree;
use Yii;
use backend\models\Image;


use yii\helpers\Url;

class Categories extends Tree{
   public static function tableName(){
       return 'category';
   }
   public function behaviors()
    {
        return array_merge(parent::behaviors(),[
            'cover_image' => [
                'class' => \rico\yii2images\behaviors\ImageBehave::className(),
            ],
        ]);
    }

 public function rules(){
    return array_merge(parent::rules(),[['cover_image','string']]);
 }
    public function isDisabled()
    {
        if (Yii::$app->user->identity->username !== 'admin') {
            return true;
        }
        return parent::isDisabled();
    }
    public function getChildrens(){
        return $this->hasMany(Categories::className(),['lvl'=>'id']);
    }
    public function getProducts(){
        return $this->hasMany(Product::className(),['id'=>'product_id'])->viaTable('product_category',['category_id'=>'id']);
    }
    public function getBrands(){
        return Brands::find()->innerJoin('product','(brands.id = brand_id)')
            ->innerJoin('product_category','product.id=product_category.product_id')
            ->andWhere(['product_category.category_id'=>$this->id])->all();


    }
     public function getImages()
    {
        $finder = [
            'itemId' => $this->id,
            'modelName' => 'Categories',
        ];
        $imageQuery = Image::find();
        $imageQuery->where($finder);
        $imageQuery->orderBy(['isMain' => SORT_DESC, 'id' => SORT_ASC]);

        $imageRecords = $imageQuery->all();

        return $imageRecords;
    }

    public function getImage()
    {
        $finder = [
            'itemId' =>  $this->id,
            'modelName' => 'Categories',
        ];
        $imageQuery = Image::find();
        $imageQuery->where($finder);
        $imageQuery->orderBy(['isMain' => SORT_DESC, 'id' => SORT_ASC]);

        $img = $imageQuery->one();
        if (!$img) {
            return Yii::$app->getModule('yii2images')->getPlaceHolder();
        }
        return $img;
    }

}