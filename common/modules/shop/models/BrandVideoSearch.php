<?php

namespace common\modules\shop\models;

use common\modules\shop\models\BrandVideo;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "ss_product_video".
 *
 * @property integer $id
 * @property string $product_id
 * @property string $link
 *
 * @property SsProduct $product
 */
class BrandVideoSearch extends BrandVideo {
    
    public function search($params) {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $query->andFilterWhere([
            'brand_id' => $this->brand_id,
        ]);
        $query->orderBy(['id'=>SORT_DESC]); 

        return $dataProvider;
    }
    
}
