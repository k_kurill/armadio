<?php

namespace common\modules\shop\models;

use backend\models\Image;
use frontend\models\Product;
use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "artisans".
 *
 * @property integer $id
 * @property string $name
 * @property string $brand
 * @property string $location
 * @property string $video
 * @property string $description
 * @property string $meta_keyword
 * @property string $meta_description
 *
 * @property Product[] $products
 */
class Brands extends \yii\db\ActiveRecord
{   

    private $_name, $_brand, $_meta_keyword, $_meta_description, $_alternative_contend, $_description, $_description1, $_description2, $_description3, $_description4;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brands';
    }

    public function behaviors()
    {
        return [
            'image' => [
                'class' => \rico\yii2images\behaviors\ImageBehave::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required', 'on' => 'create'],
            [['name'], 'required', 'on' => ['update', 'default']],
            //[['variant_of', 'language_id'], 'unique', 'targetAttribute' => ['variant_of', 'language_id']],
            [['description','description1','description2','description3','description4','alternative_contend'], 'string'],
            [['name', 'brand', 'location', 'meta_keyword', 'meta_description'], 'string', 'max' => 255],
            [['variant_of'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('brands', 'ID'),
            'name' => Yii::t('brands', 'Full name'),
            'brand' => Yii::t('brands', 'Brand'),
            'language_id' => Yii::t('product', 'Language'),
            'location' => Yii::t('brands', 'Location'),
            'description' => Yii::t('brands', 'Description'),
            'meta_keyword' => Yii::t('brands', 'Meta Keyword'),
            'meta_description' => Yii::t('brands', 'Meta Description'),
        ];
    }

    public function getTitle()
    {
        if (!$this->brand) {
            return $this->name;
        }
        return $this->brand;
    }

    public function afterFind()
    {
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        return parent::beforeSave($insert);
        
    }

    public function afterSave($insert, $changedAttributes)
    {
       
        $this->createI18nRecord();
         if($this->_name)
        $this->data->name = $this->_name;
        if($this->_brand)
        $this->data->brand = $this->_brand;
    if($this->_meta_keyword)
        $this->data->meta_keyword = $this->_meta_keyword;
    if($this->_meta_description)
        $this->data->meta_description = $this->_meta_description;
       if($this->_description)
        $this->data->description = $this->_description;
    if($this->_description1)
        $this->data->description1 = $this->_description1;
    if($this->_description2)
        $this->data->description2 = $this->_description2;
    if($this->_description3)
        $this->data->description3 = $this->_description3;
    if($this->_description4)
        $this->data->description4 = $this->_description4;
    if($this->_alternative_contend)
        $this->data->alternative_contend = $this->_alternative_contend;

       
      
        $this->data->save(false);
        
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getData($lng = null)
    {
        return $this->hasOne(BrandI18nData::className(), ['brand_id' => 'id'])->andFilterWhere(['language_id' => $lng]);
    }

    public function getName()
    {
        if ($this->isNewRecord) {
            return $this->_name;
        }
        if (!$this->data) {
            $this->createI18nRecord();
        }
        if (!$this->data->name) {
            $this->data->name = "Brand {$this->id}";
        }
        return $this->data->name;
    }

    public function setName($value)
    {
        $this->_name = $value;
        if ($this->isNewRecord) {
            return;
        }
        $this->_name = $value;
        $this->createI18nRecord();
        $this->data->name = $value;
    }

    public function getBrand()
    {
        if ($this->isNewRecord) {
            return $this->_brand;
        }
        if (!$this->data) {
            $this->createI18nRecord();
        }

        return $this->data->brand;
    }

    public function setBrand($value)
    {
        $this->_brand = $value;
        if ($this->isNewRecord) {
            return;
        }
        $this->createI18nRecord();
        $this->data->brand = $value;
        return;
    }


    public function getMeta_Keyword()
    {
        if ($this->isNewRecord) {
            return $this->_meta_keyword;
        }
        if (!$this->data) {
            $this->createI18nRecord();
        }
        return $this->data->meta_keyword;
    }

    public function setMeta_Keyword($value)
    {
        $this->_meta_keyword = $value;
        if ($this->isNewRecord) {
            return;
        }
        $this->createI18nRecord();
        $this->data->meta_keyword = $value;
        return;
    }

    public function getMeta_Description()
    {
        if ($this->isNewRecord) {
            return $this->_meta_description;
        }
        if (!$this->data) {
            $this->createI18nRecord();
        }
        return $this->data->meta_description;
    }

    public function setMeta_Description($value)
    {
        $this->_meta_description = $value;
        if ($this->isNewRecord) {
            return;
        }
        $this->createI18nRecord();
        $this->data->meta_description = $value;
        return;
    }

    public function getDescription()
    {
        if ($this->isNewRecord) {
            return $this->_description;
        }
        if (!$this->data) {
            $this->createI18nRecord();
        }
        return $this->data->description;
    }

    public function setDescription($value)
    {
        $this->_description = $value;
        if ($this->isNewRecord) {
            return;
        }
        $this->createI18nRecord();
        $this->data->description = $value;
        return;
    }


    public function getDescription1()
    {
        if ($this->isNewRecord) {
            return $this->_description1;
        }
        if (!$this->data) {
            $this->createI18nRecord();
        }
        return $this->data->description1;
    }

    public function setDescription1($value)
    {
        $this->_description1 = $value;
        if ($this->isNewRecord) {
            return;
        }
        $this->createI18nRecord();

        $this->data->description1 = $value;
        return $this->data->save(false);
    }

    public function getDescription2()
    {
        if ($this->isNewRecord) {
            return $this->_description2;
        }
        if (!$this->data) {
            $this->createI18nRecord();
        }
        return $this->data->description2;
    }

    public function setDescription2($value)
    {
        $this->_description2 = $value;
        if ($this->isNewRecord) {
            return;
        }
        $this->createI18nRecord();
        $this->data->description2 = $value;

        return $this->data->save(false);
    }

    public function getDescription3()
    {
        if ($this->isNewRecord) {
            return $this->_description3;
        }
        if (!$this->data) {
            $this->createI18nRecord();
        }
        return $this->data->description3;
    }

    public function setDescription3($value)
    {
        $this->_description3 = $value;
        if ($this->isNewRecord) {
            return;
        }
        $this->createI18nRecord();
        $this->data->description3 = $value;
        return $this->data->save(false);
    }

    public function getDescription4()
    {
        if ($this->isNewRecord) {
            return $this->_description4;
        }
        if (!$this->data) {
            $this->createI18nRecord();
        }
        return $this->data->description4;
    }

    public function setDescription4($value)
    {
        $this->_description4 = $value;
        if ($this->isNewRecord) {
            return;
        }
        $this->createI18nRecord();
        $this->data->description4 = $value;
        return $this->data->save(false);
    }


    private function createI18nRecord($lng = null)
    {
        if ($this->getData($lng)->one()) {
            return true;
        } else {
            BrandI18nData::createTranslate($this->id, $lng);
            $this->refresh();
        }
    }


    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['brand_id' => 'id']);
    }

    public function getImages()
    {
        $finder = [
            'itemId' => $this->id,
            'modelName' => 'Brands',
        ];
        $imageQuery = Image::find();
        $imageQuery->where($finder);
        $imageQuery->orderBy(['isMain' => SORT_DESC, 'id' => SORT_ASC]);

        $imageRecords = $imageQuery->all();

        return $imageRecords;
    }
    public function getSliderImages()
    {
        $finder = [
            'itemId' => $this->id,
            'modelName' => 'Brands',
        ];
        $imageQuery = Image::find();
        $imageQuery->where($finder);
        $imageQuery->andWhere(['role'=>null]);
        $imageQuery->orderBy(['isMain' => SORT_DESC, 'id' => SORT_ASC]);

        $imageRecords = $imageQuery->all();
        
        return $imageRecords;
    }
    public function getImage()
    {
        $finder = [
            'itemId' => $this->variant_of ? $this->variant_of : $this->id,
            'modelName' => 'Brands',
        ];
        $imageQuery = Image::find();
        $imageQuery->where($finder);
        $imageQuery->orderBy(['isMain' => SORT_DESC, 'id' => SORT_ASC]);

        $img = $imageQuery->one();
        if (!$img) {
            return Yii::$app->getModule('yii2images')->getPlaceHolder();
        }
        return $img;
    }

    public function getBrandVariansForMenu()
    {
        $items = [];
        foreach ($this->variants as $product) {
            $items[] = ['linkOptions' => ['data-pjax' => 0], 'label' => $product->name, 'url' => Url::to(['brand/update', 'id' => $product->id])];
        }
        $items[] = ['linkOptions' => ['data-pjax' => 0], 'label' => 'Create...', 'url' => Url::to(['brand/create', 'parent' => $this->id])];
        return $items;
    }


    public function getVariants()
    {
        return $this->hasMany(self::className(), ['variant_of' => 'id'])
            ->andWhere(['!=', 'variant_of', new \yii\db\Expression('id')]);
    }

    public function getLngSubitems()
    {
        $languages = \lajax\translatemanager\models\Language::findAll(['status' => 1]);
        $submenu = [];
        foreach ($languages as $language) {
            $variant = self::find()->andWhere(['id' => $this->id])->andWhere(['language_id' => $language->language_id])->one();

            if ($variant) {
                $submenu[] = ['label' => $language->name_ascii . ' <i class="fa fa-edit"></i>', 'url' => \yii\helpers\Url::to(['brands/update', 'id' => $variant->id])];
            } else {
                $submenu[] = ['label' => $language->name_ascii . ' <i class="fa fa-plus"></i>', 'format' => 'html', 'url' => \yii\helpers\Url::to(['brands/create', 'lng' => $language->language_id, 'parent' => $this->id])];
            }
        }
        return $submenu;
    }
    public function getVideo(){
        return $this->hasMany(BrandVideo::className(),['brand_id'=>'id']);
    }
    public function getImageByRole($role){

        $finder = [
            'itemId' => $this->id,
            'modelName' => 'Brands',
            'role'=>$role,
        ];
        $imageQuery = Image::find();
        $imageQuery->where($finder);

        $img = $imageQuery->one();
        if(!$img){
            return Yii::$app->getModule('yii2images')->getPlaceHolder();
        }
        return $img;
    }
    public function setAlternative_contend($value)
    {
        $this->_alternative_contend = $value;
        if ($this->isNewRecord) {
            return;
        }
        $this->createI18nRecord();
        $this->data->alternative_contend = $value;
        return $this->data->save(false);
    }

    public function getAlternative_contend()
    {
        if ($this->isNewRecord) {
            return $this->_alternative_contend;
        }
        if (!$this->data) {
            $this->createI18nRecord();
        }
        return $this->data->alternative_contend;
    }
}
