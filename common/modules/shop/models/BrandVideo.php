<?php

namespace common\modules\shop\models;

use Yii;

/**
 * This is the model class for table "ss_product_video".
 *
 * @property integer $id
 * @property string $product_id
 * @property string $link
 *
 * @property SsProduct $product
 */
class BrandVideo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brands_video';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['brand_id', 'link'], 'required'],
            [['id', 'brand_id'], 'integer'],
            [['link'], 'string', 'max' => 1024]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('brands_video', 'ID'),
            'brand_id' => Yii::t('brands_video', 'Brand ID'),
            'link' => Yii::t('brands_video', 'Link'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brands::className(), ['id' => 'brand_id']);
    }
    protected function getVideoId(){
        if(strpos($this->link, 'youtube')!==false){
            $parsed_url = parse_url($this->link);
            if(!isset($parsed_url['query'])){
                return '';
            }
            $query = $parsed_url['query'];
            parse_str($query);
            if(!isset($v)){
                return '';
            }
            return $v;
        }else{
            
        }
    }
    
    public function getVideoEmbedLnk(){
        
        $class = new \frontend\components\VideoThumb([]);
        $video = $class->process($this->link);

        return $video['video'];
        /*
        $v = $this->getVideoId();
        if(strpos($this->link, 'youtube')!==false){
            return 'https://www.youtube.com/embed/'.$v;
        }else{
            return $this->link; 
        }*/
    }
    
    public function getVideoThumb(){
        $class = new \frontend\components\VideoThumb([]);
        $video = $class->process($this->link);
        return $video['image'];
        /*$v = $this->getVideoId();
        return 'http://img.youtube.com/vi/'.$v.'/'.'0.jpg';
         * 
         */
    }
}
