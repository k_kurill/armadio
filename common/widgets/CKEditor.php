<?php
namespace common\widgets;

use iutbay\yii2kcfinder\KCFinder;
use yii\helpers\ArrayHelper;

use iutbay\yii2kcfinder\KCFinderAsset;

class CKEditor extends \dosamigos\ckeditor\CKEditor
{
    public $enableKCFinder = true;

    /**
     * Registers CKEditor plugin
     */
    protected function registerPlugin()
    {
        if ($this->enableKCFinder) {
            $this->registerKCFinder();
        }

        parent::registerPlugin();
    }

    /**
     * Registers KCFinder
     */
    protected function registerKCFinder()
    {
        $_SESSION['KCFINDER'] = [

            'disabled' => false,
            'uploadURL'=>'/images/pages',
            'uploadDir'=>\Yii::getAlias('@frontend/web/images/pages')

        ];

        $register = KCFinderAsset::register($this->view);
        $kcfinderUrl = $register->baseUrl;

        $browseOptions = [
            'filebrowserBrowseUrl' => $kcfinderUrl . '/browse.php?opener=ckeditor&type=files',
            'filebrowserUploadUrl' => $kcfinderUrl . '/upload.php?opener=ckeditor&type=files',
            'filebrowserImageBrowseUrl' => $kcfinderUrl . '/browse.php?opener=ckeditor&type=images',
            'filebrowserImageUploadUrl' => $kcfinderUrl . '/upload.php?opener=ckeditor&type=images',
            'allowedContent' => true
        ];

        $this->clientOptions = ArrayHelper::merge($browseOptions, $this->clientOptions);

    }

}