<?php

namespace backend\controllers;

use backend\models\ProductI18nData;
use yii\web\NotFoundHttpException;
use backend\models\ProductSearch;
use yii\filters\AccessControl;
use backend\models\Product;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\web\Controller;
use yii\helpers\Json;
use yii\web\Response;
use yii\helpers\Url;
use Yii;

class ProductController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'upload', 'cropImage', 'delete-image', 'update-image','update-translate','create-variant', 'create-size', 'update-size', 'delete-size'],
                        'allow' => true,
                        'roles' => ['manage_shop'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    public function actionUpdateTranslate($product_id, $lng){
        $model = ProductI18nData::createTranslate($product_id, $lng);
        
        if($model->load(Yii::$app->request->post()) && $model->save()){
            \Yii::$app->session->setFlash('success', 'Saved');
        }
        $response = Yii::$app->response;
        $response->getHeaders()->set('Vary', 'Accept');
        $response->format = Response::FORMAT_JSON;
        return $this->renderAjax('translate_form', ['model'=>$model]);
    }
    
    

    public function getLngSubitems($product_id){
        $languages = \lajax\translatemanager\models\Language::find()->andWhere(['>','status', 0])->andWhere(['!=','language_id', \Yii::$app->sourceLanguage])->all();
        $submenu = [];
        foreach ($languages as $language) {
            $submenu[] = ['encode'=>false,
                'content'=> \yii\bootstrap\Alert::widget(['options' => ['class' => 'alert-info',],
                        'body' => 'Please select the language from the menu',]),
                'label' => $language->name_ascii . ' <i class="fa fa-edit"></i>',
                'linkOptions'=>['data-url'=>Url::to(['product/update-translate', 'product_id' => $product_id, 'lng'=>$language->language_id])],
            ];
        }
        return $submenu;
    }

    public function actions()
    {
        return [
            'cropImage' => [
                'class' => \demi\image\CropImageAction::className(),
                'modelClass' => \backend\models\Image::className(),
                'redirectUrl' => function ($model) {
                        /* @var $model Post */
                        // triggered on !Yii::$app->request->isAjax, else will be returned JSON: {status: "success"}
                        return ['update', 'id' => $model->id];
                    },
                'afterCrop' => function ($model) {
                        $model->cropped = '1';
                        $model->save();
                        $response = Yii::$app->response;
                        $response->getHeaders()->set('Vary', 'Accept');
                        $response->format = Response::FORMAT_JSON;

                        return ['status' => 'success'];
                    },
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($parent=null)
    {
        $model = new Product();
        $model->scenario = 'create';
        $model->variant_of = $parent;
        if($model->variant_of){
            $model->brand_id = Product::findOne($model->variant_of)->brand_id;
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
   
    
    public function actionCreateSize($id){
        $product = $this->findModel($id);
        $size = new \backend\models\ProductSize;
        $size->product_id = $id;
        if($size->load(Yii::$app->request->post()) && $size->save()){
            $size->product->updateCount();
            $size = new \backend\models\ProductSize;
        }
        $this->renderAjax('form/sizes',['model'=>$product, 'newsize'=>$size]);
    }
    public function actionUpdateSize($id){
        $size = \backend\models\ProductSize::find()->andWhere(['id'=>$id])->one();
        if($size->load(Yii::$app->request->post()) && $size->save()){
            
            $size->product->updateCount();
        }else{
            die(var_dump($size->getErrors()));
        }
        $newsize = new \backend\models\ProductSize;
        return $this->renderAjax('form/sizes',['model'=>$size->product, 'newsize'=>$newsize]);
    }
    
    public function actionDeleteSize($id){
        $size = \backend\models\ProductSize::find()->andWhere(['id'=>$id])->one();
        if(!$size || !$size->product){
            return $this->redirect(\Yii::$app->request->referrer);
        }
        $product = clone($size->product);
        $size->delete();
        $product->updateCount();
        $newsize = new \backend\models\ProductSize;
        return $this->renderAjax('form/sizes',['model'=>$product, 'newsize'=>$newsize]);
    }
    

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'update';
        if(isset($_POST['Product']) && isset($_POST['Product']['count'])){
            $model->updCount($_POST['Product']['count']);
            return $this->render('update', [
                'model' => $model,
            ]);
        }
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $model->setCategories(Yii::$app->request->post('categories'));
            $model->save();
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDeleteImage($id, $product_id)
    {
        $model = $this->findModel($product_id);
        $image = \backend\models\Image::findOne(['id' => $id]);
        $model->removeImage($image);
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->getHeaders()->set('Vary', 'Accept');
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['status' => 'success', 'message' => 'Image deleted'];
        } else {
            return Yii::$app->response->redirect(['product/update', 'id' => $model->primaryKey]);
        }
    }
    
    public function actionUpdateImage($id, $role){
        
        $image = \backend\models\Image::find()->andWhere(['id'=>$id])->one();
        if($role=='default'){
            $role = null;
        }
        $images = \backend\models\Image::find()
                ->andWhere(['itemId'=>$image->itemId])
                ->andWhere(['modelName'=>$image->modelName])
                ->andWhere(['role'=>$role])->all();
        foreach ($images as $cimage){
            $cimage->role = '';
            $cimage->save();
        }
        
        $image->role = $role;
        $image->save();
        return $this->redirect(\Yii::$app->request->referrer);
    }

    public function actionUpload($id)
    {
        $model = $this->findModel($id);
        $imageFile = UploadedFile::getInstanceByName('Product[image]');
        $directory = \Yii::getAlias('@frontend/web/images/product') . '/' . $model->id . '/';
        if (!is_dir($directory)) {
            mkdir($directory);
        }
        if ($imageFile) {
            $uid = uniqid(time(), true);
            $fileName = $uid . '.' . $imageFile->extension;
            $filePath = $directory . $fileName;
            if ($imageFile->saveAs($filePath)) {
                $path = '/images/product/' . $model->id . '/' . $fileName;
                $image = $model->attachImage($filePath);
                copy($image->getPathToOrigin(), \demi\image\ImageUploaderBehavior::addPostfixToFile($image->getPathToOrigin(), "_original"));
                return Json::encode([
                    'files' => [[
                        'name' => $fileName,
                        'size' => $imageFile->size,
                        "url" => $path,
                        "thumbnailUrl" => $path,
                        "deleteUrl" => 'image-delete?name=' . $fileName,
                        "deleteType" => "POST"
                    ]]
                ]);
            }
        }
        return Json::encode([
            'error' => 'File not uloaded',
        ]);
    }

}
