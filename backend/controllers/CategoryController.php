<?php
namespace backend\controllers;

use common\modules\shop\models\Categories;

use kartik\tree\TreeView;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\helpers\Json;
use Yii;
use yii\web\Response;
class CategoryController extends Controller{
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'upload', 'cropImage', 'delete-image'],
                        'allow' => true,
                        'roles' => ['manage_shop'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

public function actions()
    {
        return [
            'cropImage' => [
                'class' => \demi\image\CropImageAction::className(),
                'modelClass' => \backend\models\Image::className(),
                'redirectUrl' => function ($model) {
                        /* @var $model Post */
                        // triggered on !Yii::$app->request->isAjax, else will be returned JSON: {status: "success"}
                        return ['update', 'id' => $model->id];
                    },
                'afterCrop' => function ($model) {
                        $model->cropped = '1';
                        $model->save();
                        $response = Yii::$app->response;
                        $response->getHeaders()->set('Vary', 'Accept');
                        $response->format = Response::FORMAT_JSON;

                        return ['status' => 'success'];
                    },
            ],
        ];
    }
    public function actionIndex(){
        return $this->render('index');
    }

    public function actionUpload($id)
    {
        $model = $this->findModel($id);
        $imageFile = UploadedFile::getInstanceByName('Categories[cover_image]');
        $directory = \Yii::getAlias('@frontend/web/images/categories') . '/' . $model->id . '/';
        if (!is_dir($directory)) {
            mkdir($directory);
        }
        if ($imageFile) {
            $uid = uniqid(time(), true);
            $fileName = $uid . '.' . $imageFile->extension;
            $filePath = $directory . $fileName;
            foreach ($model->getImages() as $image) {
                $model->removeImage($image);
            }
            if ($imageFile->saveAs($filePath)) {
                $path = '/images/categories/' . $model->id . '/' . $fileName;
                $image = $model->attachImage($filePath);
                copy($image->getPathToOrigin(), \demi\image\ImageUploaderBehavior::addPostfixToFile($image->getPathToOrigin(), "_original"));
                return Json::encode([
                    'files' => [[
                        'name' => $fileName,
                        'size' => $imageFile->size,
                        "url" => $path,
                        "thumbnailUrl" => $path,
                        "deleteUrl" => 'image-delete?name=' . $fileName,
                        "deleteType" => "POST"
                    ]]
                ]);
            }
        }
        return Json::encode([
            'error' => 'File not uloaded',
            'mes' => ''
        ]);
    }
      protected function findModel($id)
    {
        if (($model = Categories::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionDeleteImage($id, $category_id)
    {
        $model = $this->findModel($category_id);
        $image = \backend\models\Image::findOne(['id' => $id]);
        $model->removeImage($image);
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->getHeaders()->set('Vary', 'Accept');
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['status' => 'success', 'message' => 'Image deleted'];
        } else {
            return Yii::$app->response->redirect(['brands/update', 'id' => $model->primaryKey]);
        }
    }

}