<?php

namespace backend\controllers;

use common\modules\shop\models\BrandI18nData;
use common\modules\shop\models\BrandVideo;
use common\modules\shop\models\BrandVideoSearch;
use Yii;
use common\modules\shop\models\Brands;
use common\modules\shop\models\BrandsSearch;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\Json;
use yii\web\Response;


/**
 * BrandsController implements the CRUD actions for Brands model.
 */
class BrandsController extends Controller
{
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [

                        'actions' => ['index', 'create', 'update', 'delete', 'upload', 'cropImage', 'delete-image', 'update-image', 'update-translate', 'add-video', 'delete-video'],

                        'allow' => true,
                        'roles' => ['manage_shop'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'cropImage' => [
                'class' => \demi\image\CropImageAction::className(),
                'modelClass' => \backend\models\Image::className(),
                'redirectUrl' => function ($model) {
                        /* @var $model Post */
                        // triggered on !Yii::$app->request->isAjax, else will be returned JSON: {status: "success"}
                        return ['update', 'id' => $model->id];
                    },
                'afterCrop' => function ($model) {
                        $model->cropped = '1';
                        $model->save();
                        $response = Yii::$app->response;
                        $response->getHeaders()->set('Vary', 'Accept');
                        $response->format = Response::FORMAT_JSON;

                        return ['status' => 'success'];
                    },
            ],
        ];
    }
    public function actionUpdateTranslate($brand_id, $lng)
    {

        $model = BrandI18nData::createTranslate($brand_id, $lng);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash('success', 'Saved');
        }
        $response = Yii::$app->response;
        $response->getHeaders()->set('Vary', 'Accept');
        $response->format = Response::FORMAT_JSON;
        return $this->renderAjax('translate_form', ['model' => $model]);
    }
    public function getLngSubitems($brand_id){
        $languages = \lajax\translatemanager\models\Language::find()->andWhere(['>','status', 0])->andWhere(['!=','language_id', \Yii::$app->sourceLanguage])->all();
        $submenu = [];
        foreach ($languages as $language) {
            $submenu[] = ['encode'=>false,
                'content'=> \yii\bootstrap\Alert::widget(['options' => ['class' => 'alert-info',],
                        'body' => 'Please select the language from the menu',]),
                'label' => $language->name_ascii . ' <i class="fa fa-edit"></i>',
                'linkOptions'=>['data-url'=>Url::to(['brands/update-translate', 'brand_id' => $brand_id, 'lng'=>$language->language_id])],
            ];
        }
        return $submenu;
    }
    /**
     * Lists all Brands models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BrandsSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

     public function actionUpdateImage($id, $role){
        
        $image = \backend\models\Image::find()->andWhere(['id'=>$id])->one();
        if($role=='default'){
            $role = null;
        }

        $images = \backend\models\Image::find()
                ->andWhere(['itemId'=>$image->itemId])
                ->andWhere(['modelName'=>$image->modelName])
                ->andWhere(['role'=>$role])->all();
        foreach ($images as $cimage){
            $cimage->role = null;
            $cimage->save();
        }
        
        $image->role = $role;
        $image->save();
        
        return $this->redirect(\Yii::$app->request->referrer);
    }
    /**
     * Displays a single Brands model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Brands model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($parent = null)
    {
        $model = new Brands();
        $model->scenario = 'create';
        $model->variant_of = $parent;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,

            ]);
        }
    }

    /**
     * Updates an existing Brands model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'update';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            $videoModel = new \common\modules\shop\models\BrandVideoSearch();
            $videoModel->brand_id = $model->id;
            return $this->render('update', [
                'model' => $model,
                'videoModel' => $videoModel,
            ]);
        }
    }

    public function actionAddVideo()
    {
        $brand_id = Yii::$app->request->post('brand_id');
        $model = $this->findModel($brand_id);
        if (isset($_POST['video_link'])) {
            $video = new BrandVideo();
            $video->brand_id = $model->id;
            $video->link = $_POST['video_link'];
            $video->save();
        }
        return $this->redirect(['update', 'id' => $model->id]);

    }

    public function actionDeleteVideo($id)
    {
        if (($video = BrandVideo::findOne($id)) !== null) {
            $model = $this->findModel($video->brand_id);
            $video->delete();
            $this->redirect(['update', 'id' => $model->id]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

    }


    /**
     * Deletes an existing Brands model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        Brands::deleteAll(['variant_of' => $id]);
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Brands model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Brands the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Brands::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDeleteImage($id, $brands_id)
    {
        $model = $this->findModel($brands_id);
        $image = \backend\models\Image::findOne(['id' => $id]);
        $model->removeImage($image);
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->getHeaders()->set('Vary', 'Accept');
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['status' => 'success', 'message' => 'Image deleted'];
        } else {
            return Yii::$app->response->redirect(['brands/update', 'id' => $model->primaryKey]);
        }
    }

    public function actionUpload($id)
    {
        $model = $this->findModel($id);
        $imageFile = UploadedFile::getInstanceByName('Brands[image]');
        $directory = \Yii::getAlias('@frontend/web/images/brands') . '/' . $model->id . '/';
        if (!is_dir($directory)) {
            mkdir($directory);
        }
        if ($imageFile) {
            $uid = uniqid(time(), true);
            $fileName = $uid . '.' . $imageFile->extension;
            $filePath = $directory . $fileName;
            if ($imageFile->saveAs($filePath)) {
                $path = '/images/brands/' . $model->id . '/' . $fileName;
                $image = $model->attachImage($filePath);
                copy($image->getPathToOrigin(), \demi\image\ImageUploaderBehavior::addPostfixToFile($image->getPathToOrigin(), "_original"));
                return Json::encode([
                    'files' => [[
                        'name' => $fileName,
                        'size' => $imageFile->size,
                        "url" => $path,
                        "thumbnailUrl" => $path,
                        "deleteUrl" => 'image-delete?name=' . $fileName,
                        "deleteType" => "POST"
                    ]]
                ]);
            }
        }
        return Json::encode([
            'error' => 'File not uloaded',
            'mes' => ''
        ]);
    }
   

}
