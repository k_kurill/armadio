<?php

namespace backend\controllers;

use yii\filters\AccessControl;
use backend\models\OrderSearch;
use yii\filters\VerbFilter;
use common\models\Order;
use Yii;

class OrderController extends \yii\web\Controller{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'delete', 'restore'],
                        'allow' => true,
                        'roles' => ['manage_shop'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    } 
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();
        return $this->redirect(['index']);
    } 
    public function actionRestore($id)
    {
        $model = $this->findModel($id);
        $model->deleted = '0';
        $model->save();
        return $this->redirect(['index']);
    } 
    
    
    
    
    
    public function actionUpdate($id){
        $model = $this->findModel($id);
        $model->load(Yii::$app->request->post());
        $model->save();
        if($model->shipping){
            $model->shipping->load(Yii::$app->request->post());
            $model->shipping->save();
        }
        return $this->render('update',['model'=>$model]);
    }
    
    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    } 
    
}