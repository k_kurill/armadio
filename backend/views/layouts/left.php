<?php $langItems = [
    ['label' => Yii::t('language', 'Language'), 'items' => [
        ['label' => Yii::t('language', 'List of languages'), 'url' => ['/translatemanager/language/list']],
        ['label' => Yii::t('language', 'Create'), 'url' => ['/translatemanager/language/create']],
    ]
    ],
    ['label' => Yii::t('language', 'Scan'), 'url' => ['/translatemanager/language/scan']],
    ['label' => Yii::t('language', 'Optimize'), 'url' => ['/translatemanager/language/optimizer']],
    ['label' => Yii::t('language', 'Im-/Export'), 'items' => [
        ['label' => Yii::t('language', 'Import'), 'url' => ['/translatemanager/language/import']],
        ['label' => Yii::t('language', 'Export'), 'url' => ['/translatemanager/language/export']],
    ]
    ]
];

$menuItems[] = ['label' => 'Store', 'options' => ['class' => 'header']];
$menuItems = [
    ['label' => 'Dashboard', 'icon' => 'fa fa-dashboard', 'url' => ['/site/index']],
];
$menuItems[] = ['label' => 'Orders', 'icon' => 'fa fa-shopping-cart', 'url' => ['/order'],];
$menuItems[] = ['label' => 'Customers', 'icon' => 'fa fa-user', 'url' => ['/customers'],];
$menuItems[] = ['label' => 'Products', 'icon' => 'fa fa-barcode', 'url' => ['/product'],];
$menuItems[] = ['label' => 'Discounts', 'icon' => 'fa fa-cut', 'url' => ['/discounts'],];
$menuItems[] = ['label' => 'Reports', 'icon' => 'fa fa-bar-chart', 'url' => ['/reports'],];

$menuItems[] = ['label' => 'Configuration', 'options' => ['class' => 'header']];

$menuItems[] = ['label' => 'Apps', 'icon' => 'fa fa-globe', 'url' => ['/apps'],];
$menuItems[] = ['label' => 'Settings', 'icon' => 'fa fa-gears',  'url' =>'#', 'items' => [
        ['label' => 'General',   'url' => ['#']],
        ['label' => 'Payments',  'url' => ['/payment/payment/configure']],
        ['label' => 'Taxes',     'url' => ['#']],
        ['label' => 'Checkout',  'url' => ['#']],
        ['label' => 'Shipping',  'url' => ['#']],
        ['label' => 'Accounts', 'url' => ['#']],
        ['label' => 'Notifications / Emails',  'url' => ['#']],
    ]];



$menuItems[] = ['label' => 'Must be somewhere (not sorted)', 'options' => ['class' => 'header']];
$menuItems[] = ['label' => 'Site users', 'icon' => 'fa fa-users', 'url' => Yii::$app->urlManager->createUrl(['/user/admin']),'visible'=>\common\models\User::find()->andWhere(['id'=>Yii::$app->user->id])->one()->username == 'admin'];
$menuItems[] = ['label' => 'Pages', 'icon' => 'fa fa-file-text', 'url' => ['/pages/manager'],'visible'=>Yii::$app->user->can('edit_pages')];
//$menuItems[] = ['label' => 'Menu', 'icon' => 'fa fa-sitemap', 'url' => ['/menu']];

$menuItems[] = [
    'label' => 'Transtlate',
    'icon' => 'fa fa-list-alt',
    'visible'=>Yii::$app->user->can('edit_translate'),
    'url' => ['#'],
    'items' => $langItems,
];
$menuItems[] = ['label' => 'Categories', 'icon' => 'fa fa-circle-o', 'url' => ['/category'],];
$menuItems[] = ['label' => 'Artisan', 'icon' => 'fa fa-circle-o', 'url' => ['/brands'],];


?>

<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?=
        dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => $menuItems,
            ]
        ) ?>

    </section>

</aside>
