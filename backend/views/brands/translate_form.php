<?php
use yii\bootstrap\ActiveForm;
use common\widgets\CKEditor;
use yii\bootstrap\Html;

?>
<?php
$JS = <<<JS

JS;
$this->registerJs($JS, yii\web\View::POS_READY);

Yii::$app->session->set('brand_translate', $model->language_id);

?>

    <h2>Translation to <?=$model->language->name_ascii?></h2>
<?php yii\widgets\Pjax::begin(['enablePushState'=>false,'id'=>"pjax_translate{$model->id}{$model->language_id}"])?>


<?php $form = ActiveForm::begin([
    'id'=>"translate{$model->id}{$model->language_id}",
    'options' => ['data-pjax' => true],
    'action' => yii\helpers\Url::to(['brands/update-translate', 'brand_id' => $model->brand_id, 'lng'=>$model->language_id]),
]); ?>

<?=\common\widgets\Alert::widget();?>

<?php if($model->errors):?>
    <div class="alert alert-error">
        <?= $form->errorSummary([$model]) ?>
    </div>
<?php endif;?>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'name') ?>
            <?= $form->field($model, 'brand') ?>

            <?= $form->field($model, 'meta_keyword') ?>
            <?= $form->field($model, 'meta_description') ?>
        </div>

        <div class="col-md-12">
            <?=CKEditor::widget([
                'id'=>"description-{$model->id}",
                'name'=>"BrandI18nData[description]",
                'value'=>$model->description,
            ]);?>
        </div>
        <div class="col-md-12">
            <?=CKEditor::widget([
                'id'=>"description1-{$model->id}",
                'name'=>"BrandI18nData[description1]",
                'value'=>$model->description1,
            ]);?>
        </div>
        <div class="col-md-12">
            <?=CKEditor::widget([
                'id'=>"description2-{$model->id}",
                'name'=>"BrandI18nData[description2]",
                'value'=>$model->description2,
            ]);?>
        </div>
        <div class="col-md-12">
            <?=CKEditor::widget([
                'id'=>"description3-{$model->id}",
                'name'=>"BrandI18nData[description3]",
                'value'=>$model->description3,
            ]);?>
        </div>
        <div class="col-md-12">
            <?=CKEditor::widget([
                'id'=>"description4-{$model->id}",
                'name'=>"BrandI18nData[description4]",
                'value'=>$model->description4,
            ]);?>
            <h4><?=Yii::t('artisans','Alternative contend')?></h4>
             <?=CKEditor::widget([
                'id'=>"alternative_contend-{$model->id}",
                'name'=>"BrandI18nData[alternative_contend]",
                'value'=>$model->alternative_contend,
            ]);?>
        </div>
    </div>
    <br>
    <div class="form-group text-right">
        <?= Html::submitButton(Yii::t('shiping', 'Update'), ['class' => 'btn btn-primary']) ?>
    </div>
<?php ActiveForm::end(); ?>
<?php yii\widgets\Pjax::end()?>