<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\tabs\TabsX;
$script = <<< JS
    $(function() {
        //save the latest tab (http://stackoverflow.com/a/18845441)
        $('a[data-toggle="tab"]').on('click', function (e) {
            localStorage.setItem('lastTab', $(e.target).attr('href'));

        });
        //go to the latest tab, if it exists:
        var lastTab = localStorage.getItem('lastTab');

        if (lastTab) {
            $('a[href="'+lastTab+'"]').click();
        }

    });
JS;
$this->registerJs($script, yii\web\View::POS_END);

/* @var $this yii\web\View */
/* @var $model common\modules\shop\models\Brands */
/* @var $form yii\widgets\ActiveForm */
$submenu = $this->context->getLngSubitems($model->id);
echo TabsX::widget([
    'items' => [
        [
            'label' => 'Attributes',
            'content' => $this->render('form/params', ['model'=>$model]),
            'options' => ['id' => 'attributes-tab'],
            'active' => true
        ],
        [
            'label' => 'SEO',
            'content' => $this->render('form/seo', ['model'=>$model]),
            'options' => ['id' => 'seo-tab'],
        ],

        [
            'label' => 'Images',
            'content' => $this->render('form/images', ['model'=>$model]),
            'options' => ['id' => 'images-tab'],
        ],
        [
            'label' => 'Video',
            'content' => $this->render('form/video', ['model'=>$model,'videoModel' => $videoModel]),
            'options' => ['id' => 'video-tab'],
        ],
        [
            'label' => 'Alternative content',
            'content' => $this->render('form/alternative_content', ['model'=>$model]),
            'options' => ['id' => 'alternative-content'],
        ],
        [
            'label' => 'Translation',
            'url'   => '#',
            'content'=> '',
            'items' => $submenu,
        ],
    ],
]);

?>


