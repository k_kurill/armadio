<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use lajax\translatemanager\models\Language;
use yii\helpers\ArrayHelper;
use common\widgets\CKEditor;

$languages = Language::findAll(['status'=>1]);

?>
<div class="clearfix"></div>
<div class="brands-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'brand')->textInput(['maxlength' => true]) ?>
    <?//= $form->field($model, 'language_id')->dropDownList(ArrayHelper::map($languages, 'language_id', 'name_ascii'),['disabled'=>'disabled']) ?>

    <?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->widget(\common\widgets\CKEditor::className(),[]) ?>

    <?= $form->field($model, 'description1')->widget(\common\widgets\CKEditor::className(),[]) ?>

    <?= $form->field($model, 'description2')->widget(\common\widgets\CKEditor::className(),[]) ?>

    <?= $form->field($model, 'description3')->widget(\common\widgets\CKEditor::className(),[]) ?>

    <?= $form->field($model, 'description4')->widget(\common\widgets\CKEditor::className(),[]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<div class="clearfix"></div>