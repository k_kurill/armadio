<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use lajax\translatemanager\models\Language;
use yii\helpers\ArrayHelper;
use common\widgets\CKEditor;

$languages = Language::findAll(['status'=>1]);

?>
<div class="clearfix"></div>
<div class="brands-form">
    <?php $form = ActiveForm::begin([]); ?>


   <?=$form->field($model, 'alternative_contend')->widget(\common\widgets\CKEditor::className(),[])?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<div class="clearfix"></div>