<?php
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use yii\bootstrap\Html;

?>

<div class="clearfix"></div>
<div class="video-form">
    <?php
    Modal::begin([
        'header' => '<h2>' . Yii::t('brand', 'Add video') . '</h2>',
        'toggleButton' => ['label' => Yii::t('brand', 'Add video'), 'class' => 'btn btn-primary', 'style' => 'margin-top: 15px;margin-bottom: 15px;'],
    ]);
    ?>
    <label><?= Yii::t('brand', 'Link to youtube/vimeo video') ?></label>
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data',],
        'action' => Yii::$app->urlManager->createUrl(['/brands/add-video'])]); ?>

    <div class="form-group">
        <?= Html::hiddenInput('brand_id', $model->id); ?>
        <?= Html::textInput('video_link', '', ['class' => 'form-control', 'required' => 'required']); ?>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('brand', 'Create') : Yii::t('brand', 'Update'), ['class' => 'btn blue-style-button']) ?>
    </div>
    <?php ActiveForm::end(); ?>
    <?php
    Modal::end();
        $form = ActiveForm::begin();
    echo yii\grid\GridView::widget([
        'dataProvider' => $videoModel->search(null),
        'filterModel' => false,
        //'options' => ['class' => 'blue-table'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['header' => Yii::t('brand', 'Thumb'), 'format' => 'html', 'content' => function ($model) {
                    if ($model->getVideoThumb()) {
                        return Html::img($model->getVideoThumb(),['style'=>'width: 150px']);
                    } else {
                        return Html::tag('span', Yii::t('brand', 'Video link is not vald'), ['style' => 'color: red']);
                    }
                }],
            'link',
            ['class' => 'yii\grid\ActionColumn'
                , 'template' => '{delete}'
                , 'buttons' => [
                'delete' => function ($url, $model) {
                        $url = Yii::$app->urlManager->createUrl(['/brands/delete-video', 'id' => $model->id]);
                        $text = '<a href="' . $url . '" title="' . Yii::t('brand_video', 'Delete') . '" aria-label="Delete" data-confirm="' . Yii::t('product_video', 'Are you sure you want to delete this item?') . '" data-method="post" data-pjax="0"><span class="glyphicon glyphicon-trash"></span></a>';
                        return Html::a($text, $url);
                    },
            ]
            ]
        ],
    ]);
    $form->end();
    ?>

</div>

<div class="clearfix"></div>