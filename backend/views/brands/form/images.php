<?php
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;
?>

<div class="clearfix"></div>
<div class="brend-form">


    
<?php $form = ActiveForm::begin([
    'options' => ['enctype' => 'multipart/form-data'],
    'action' => ['brands/upload', 'id' => $model->id],
]); ?>
    
<?= $form->field($model, 'image')->widget(FileInput::classname(), [
    'name' => 'image',
    'options'=>[
        'multiple'=>true,
        'accept' => 'image/*',
    ],    
    'pluginEvents' => [

    ],
    'pluginOptions' => [
        'uploadUrl' => Url::to(['brands/upload', 'id' => $model->id]),
        'uploadExtraData' => [
        ],
        'maxFileCount' => 20
    ]
])->label(false);?>    
 
<?php yii\widgets\Pjax::begin(['id'=>'pjax-images'])?>   
<div class="row">
<?php
foreach($model->getImages() as $img):
    if(!is_object($img))continue;
      
    ?>
    
        <div class="col-md-4">
            <div class="form-group file-preview-frame" style="width: 100%; <?=($img->cropped)?'':'box-shadow: 1px 1px 5px 0 #ef1010;'?>">
                <?= $form->field($img, 'image')->widget('backend\components\widgets\FormImageWidget', [
                    'imageSrc' => str_replace('/backend', '', $img->getImageSrc()),
                    'deleteUrl' => ['delete-image', 'id' => $img->id, 'brands_id'=>$model->id],
                    'cropUrl' => ['cropImage', 'id' => $img->id],
                    // cropper options https://github.com/fengyuanchen/cropper/blob/master/README.md#options
                    'cropPluginOptions' => [
                    ],
                    'imageRules'=>[
                        ['label' => 'In slider', 'url' => Url::to(['update-image', 'id' => $img->id, 'role'=>'default'])],
                        ['label' => 'Cover', 'url' => Url::to(['update-image', 'id' => $img->id, 'role'=>'cover'])],
                        ['label' => 'Main image', 'url' => Url::to(['update-image', 'id' => $img->id, 'role'=>'main'])],
                 
                            ]
                ])->label(false) ?>
            </div>
        </div>
<?php endforeach;?>  
</div>

<?php yii\widgets\Pjax::end()?>
<?php ActiveForm::end(); ?>

</div>

<div class="clearfix"></div>