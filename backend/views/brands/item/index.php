<?php
use yii\bootstrap\Html;
use yii\helpers\Url;

use kartik\dropdown\DropdownX;

$css = <<<CSS
        .item-row{
            padding: 5px;
            border-bottom: 1px solid #eee;
            min-height: 30px;
        }
CSS;
$this->registerCss($css);



?>
<div class="box box-solid box-default">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($model->name) ?></h3>

        <div class="box-tools pull-right">
            <a href="<?=Url::to(['update','id'=>$model->id])?>" class="btn btn-box-tool">
                <i class="fa fa-edit"></i>
            </a>
            <?=
            Html::a('<i class="fa fa-trash"></i>', Url::to(['/brands/delete', 'id' => $model->id]), [
                'class' => 'btn btn-box-tool',
                'data' => [
                    'confirm' => 'Are you sure you want to delete the product?',
                    'method' => 'post',
                ]
            ])?>

            <a href="#" class="btn btn-box-tool"></a>
        </div>
        <!-- /.box-tools -->
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-2">
                
               
                <?= Html::img($model->image->getUrl('150'));?>
            </div>
            <div class="col-md-8">
                <div class="row item-row">
                    <div class="col-md-6">
                        ID: <strong class="pull-right"><?= $model->id ?></strong><br>
                        Artisans: <strong class="pull-right"><?= $model->data->brand?></strong>
                    </div>
                    <div class="col-md-6">

                    </div>
                </div>


            </div>

        </div>
    </div>
</div>
<!--['update', 'id' => $model->id]-->