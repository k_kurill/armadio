<?php

use yii\helpers\Html;
use yii\grid\GridView;
use bupy7\pages\Module;
use bupy7\pages\models\Page;
use yii\helpers\ArrayHelper;
use lajax\translatemanager\models\Language;

/* @var $this yii\web\View */
/* @var $searchModel bupy7\pages\models\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Module::t('MODULE_NAME');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="page-header">
    <h1><?= Html::encode($this->title) ?></h1>
</div>
<p>
    <?= Html::a(Module::t('CREATE'), ['create'], ['class' => 'btn btn-success']); ?>
</p>
<?= GridView::widget([
    'tableOptions' => [
        'class' => 'table table-striped',
    ],
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'id',
        'title',
        'alias',
        'created_at:datetime',
        'updated_at:datetime',
        [
            'attribute' => 'published',
            'filter' => Page::publishedDropDownList(),
            'value' => function($model) {
                return Yii::$app->formatter->asBoolean($model->published);
            },
        ],
        [
            'attribute'=>'lng',
            'filter'   => ArrayHelper::map(Language::find()->andWhere(['in','language_id',$languages])->all(), 'language_id', 'name_ascii')
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => "{update}\n{delete}",
        ],
    ],
]); ?>
