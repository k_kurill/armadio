<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use bupy7\pages\Module;
use vova07\imperavi\Widget as Imperavi;
use yii\helpers\Url;
use lajax\translatemanager\models\Language;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

$update_url = Yii::$app->urlManager->createUrl(['/pages/manager/update', 'id' => $model->id]);
$js = <<<JS
        $('#language-page').change(function(){
            location = '$update_url&lng='+$(this).val();
        });
JS;
$this->registerJs($js);
/* @var $this yii\web\View */
/* @var $model bupy7\pages\models\Page */
/* @var $form yii\widgets\ActiveForm */

$module = Yii::$app->getModule('pages');


$form = ActiveForm::begin();
if (Yii::$app->request->get('id')) {
    echo $form->field($model, 'lng')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Language::find()->all(), 'language_id', 'name_ascii'),
        'options' => ['placeholder' => \Yii::t('user', 'Language'), 'id' => 'language-page'],
        'pluginOptions' => [
            'allowClear' => false
        ]]);
}
echo $form->field($model, 'title')->textInput(['maxlength' => 255]);
echo $form->field($model, 'alias')->textInput(['maxlength' => 255]);
echo $form->field($model, 'published')->checkbox();
$settings = [
    'lang' => 'en',
    //'langs' => [Yii::$app->language],
    'minHeight' => 200,
    'plugins' => [
        'fullscreen',
    ],
];
if ($module->addImage || $module->uploadImage) {
    $settings['plugins'][] = 'imagemanager';
}
if ($module->addImage) {
    $settings['imageManagerJson'] = Url::to(['images-get']);
}
if ($module->uploadImage) {
    $settings['imageUpload'] = Url::to(['image-upload']);
}
if ($module->addFile || $module->uploadFile) {
    $settings['plugins'][] = 'filemanager';
}
if ($module->addFile) {
    $settings['fileManagerJson'] = Url::to(['files-get']);
}
if ($module->uploadFile) {
    $settings['fileUpload'] = Url::to(['file-upload']);
}


echo $form->field($model, 'content')->widget(common\widgets\CKEditor::className(), [

    'preset' => 'full',
    'clientOptions' => [
        'language' => 'en',
        "filespath"=>Yii::getAlias('frontend').'/web/img/pages/',
        'filebrowserUploadUrl' => Yii::$app->getUrlManager()->createUrl('pages/manager/url'),
    ],

]);
echo $form->field($model, 'title_browser')->textInput(['maxlength' => 255]);
echo $form->field($model, 'meta_keywords')->textInput(['maxlength' => 200]);
echo $form->field($model, 'meta_description')->textInput(['maxlength' => 160]);
?>
<div class="form-group">
    <?=
    Html::submitButton($model->isNewRecord ? Module::t('CREATE') : Module::t('UPDATE'), [
        'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
    ]); ?>
</div>
<?php ActiveForm::end(); ?>
