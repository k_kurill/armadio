<?php
$this->title = Yii::t('app', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-index">
    <?php
    use kartik\tree\TreeView;
       use kartik\tree\Module;

    echo TreeView::widget([
        // single query fetch to render the tree
        // use the Product model you have in the previous step
        'query' => \common\modules\shop\models\Categories::find()->addOrderBy('lvl, lft'),
        'headingOptions' => ['label' => 'Categories'],
       'iconEditSettings'=>[
           'show'=>'none'
       ],
        'isAdmin' => false, // optional (toggle to enable admin mode)
        'displayValue' => 1, // initial display value
         'toolbar'=>['create-root'=>false],

        //'allowNewRoots'=>false,
        'allowNewRoots'=>false,
        'fontAwesome'=>true,
        'rootOptions'=>['label'=>''],
          'nodeAddlViews' => [

          Module::VIEW_PART_1=>'@backend/views/category/_part1'
          ],
          'softDelete' => true, // defaults to true
    'cacheSettings' => ['enableCache' => true]

    ]);
    ?>
</div>