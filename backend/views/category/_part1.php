<?php
use kartik\file\FileInput;
use yii\helpers\Url;
if(!$node->isNewRecord){
echo $form->field($node, 'cover_image')->widget(FileInput::classname(), [
    'name' => 'cover_image',
   
    'pluginEvents' => [
        "fileuploaded" => "function() { location.reload(); }",
    ],
    'pluginOptions' => [
        'uploadUrl' => Url::to(['/category/upload', 'id' => $node->id]),
        ]
])->label(false);
}

?>
<div class="row">
<?php
foreach($node->getImages() as $img):
    if(!is_object($img))continue;
    ?>
    
        <div class="col-md-4">
            <div class="form-group file-preview-frame" style="width: 100%; <?=($img->cropped)?'':'box-shadow: 1px 1px 5px 0 #ef1010;'?>">
                <?= $form->field($img, 'cover_image')->widget('backend\components\widgets\FormImageWidget', [
                    'imageSrc' => str_replace('/backend', '', $img->getImageSrc()),
                    'deleteUrl' => ['/category/delete-image', 'id' => $img->id, 'category_id'=>$node->id],
                    'cropUrl' => ['/category/cropImage', 'id' => $img->id],
                    // cropper options https://github.com/fengyuanchen/cropper/blob/master/README.md#options
                    'cropPluginOptions' => [],
                ])->label(false) ?>
            </div>
        </div>
<?php endforeach;?> 
</div> 