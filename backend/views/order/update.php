<?php
use kartik\datetime\DateTimePicker;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$this->title = "Order #{$model->id} details";

$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="box box-default">
    <div class="box-header">
        <div class="col-md-6 text-left">ID: <strong><?=$model->id?></strong></div>
        <div class="col-md-6 text-right">Status: <strong><?=$model->getAttributeLabel($model->status)?></strong></div>
    </div>
    <div class="box-body">
        <?php $form = ActiveForm::begin(); ?>
            <?=$form->errorSummary([$model])?>
            <?= $form->field($model, 'created')->widget(DateTimePicker::ClassName(),[
                
            ]) ?>
            <?= $form->field($model, 'customer_note')->textarea() ?>
            <?= $form->field($model, 'order_note')->textarea() ?>
            <?= $form->field($model, 'status')->dropDownList($model->getStatusesArray());?>
            
            <div class="form-group">
                <?= Html::submitButton(Yii::t('shiping', 'Update'), ['class' => 'btn btn-primary']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
    <div class="box-footer">
        <div class="row">
            <div class="col-md-6">
                <?php if($model->paypal):?>
                <h4>Billing details</h4>
                <?=$this->render('update/billing',['model'=>$model->paypal])?>
                <?php endif; ?>
                <?php if($model->billing):?>
                <h4>Billing details</h4>
                <?=$this->render('update/billing',['model'=>$model->billing])?>
                <?php endif; ?>
            </div>
            <div class="col-md-6">
                <h4>Shipping details</h4>
                <?php if($model->shipping):?>
                <?=$this->render('update/shipping',['model'=>$model->shipping])?>
                <?php endif ?>
            </div>
        </div> 
    </div>
</div>

<?=$this->render('update/orderItems',['model'=>$model])?>