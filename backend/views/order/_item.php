<?php
use yii\helpers\Url;
use yii\helpers\Html;

?>

<div class="box box-<?=$model->adminCssClass?> collapsed-box">
    <div class="box-header with-border">
        <div class="row">
            <div class="col-md-1 text-teft">ID: <strong><?=$model->id?></strong></div>
            <div class="col-md-2 text-left">User: <strong><?=$model->username?></strong></div>
            
            <div class="col-md-2 text-left">Total: <strong><?=Yii::$app->formatter->asCurrency($model->totalPrice)?></strong></div>
            <div class="col-md-2 text-left">Created: <strong><?=Yii::$app->formatter->asDatetime($model->created)?></strong></div>
            <div class="col-md-1 text-right">Items: <strong><?=$model->totalCount?></strong></div>
            <div class="col-md-2 text-right">Status: <strong><?=$model->getAttributeLabel($model->status)?></strong></div>
            <div class="col-md-2 box-tools pull-right text-right">
                <?php if(!$model->deleted):?>
                <?=Html::a('<i class="fa fa-trash"></i>', Url::to(['/order/delete','id'=>$model->id]), [
                'class'=>'btn btn-box-tool',
                'data' => [
                    'confirm' => 'Are you sure you want to delete the order?',
                    'method' => 'post',
                ]
                ])?>
                <?php else:?>
                <?=Html::a('<i class="fa fa-share-square-o"></i>', Url::to(['/order/restore','id'=>$model->id]), [
                'class'=>'btn btn-box-tool',
                'data' => [
                    'confirm' => 'Are you sure you want to restore the order?',
                    'method' => 'post',
                ]
                ])?>
                <?php endif;?>
                <a data-pjax="0" href="<?=Yii::$app->urlManager->createUrl(['order/update','id'=>$model->id])?>" class="btn btn-box-tool"><i class="fa fa-edit"></i></a>
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            </div><!-- /.box-tools -->
        </div>
    </div>
    <div class="box-body">
        <?php foreach($model->items as $item):?>
        <?=$this->render('item/product',['model'=>$item->product, 'item'=>$item]);?>
        <?php endforeach;?>
        <div class="col-md-12 text-right h4">
            Total: <strong><?=Yii::$app->formatter->asCurrency($model->totalPrice)?></strong>
        </div>
    </div>
    <div class="box-footer">
        <div class="row">
            <div class="col-md-6">
                <h4>Billing details</h4>
                <?php if($model->paypal):?>
                    <?=$this->render('item/billing',['model'=>$model->paypal])?>
                <?php elseif($model->billing):?>
                    <?=$this->render('item/billing_braintree',['model'=>$model->billing])?>
                <?php endif?>
            </div>
            <div class="col-md-6">
                <h4>Shipping details</h4>
                <?php if($model->shipping):?>
                <?=$this->render('item/shipping',['model'=>$model->shipping])?>
                <?php endif?>
            </div>
        </div> 
    </div>
</div>