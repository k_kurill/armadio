<?php
?>
<div class="row">
    <div class="col-md-12">
        <div class="pull-left">Status:</div> 
        <div class="pull-right"><strong><?= $model->status?></strong></div>
    </div>    
    <div class="col-md-12">
        <div class="pull-left">Payment token:</div> 
        <div class="pull-right"><strong><?= $model->payment_token?></strong></div> 
    </div>   
    <div class="col-md-12">   
        <div class="pull-left">Payment price:</div> 
        <div class="pull-right"><strong><?= $model->payment_price?></strong></div> 
    </div>
    <div class="col-md-12">   
        <div class="pull-left">Currency:</div> 
        <div class="pull-right"><strong><?= $model->currency?></strong></div> 
    </div>
    <div class="col-md-12">
        <div class="pull-left">Date:</div> 
        <div class="pull-right"><strong><?= Yii::$app->formatter->asDatetime($model->updated_at); ?></strong></div>
    </div>    

</div>