<?php
use yii\bootstrap\Html;
?>

<div class="row cart-item">
    <div class="col-md-2 cart-item-image">
        <?php if(is_object($model)):?>
            <?= Html::img($model->getImage()->getUrl('80x'));?>
        <?php endif;?>
    </div>
    <div class="col-md-10 cart-item-properties">
        <div class="row">
            <div class="col-md-12 cart-item-name">
                <h4><?= $item->name;?></h4>
            </div>
            <div class="col-md-2 cart-item-sku">
                <?php if(is_object($model)):?>
                    SKU: <?=$model->sku_code?>
                <?php endif;?>
            </div>
            <div class="col-md-2 cart-item-size">
                <?=($item->size)?'Size: '.$item->size:''?>
            </div>
            <div class="col-md-2 cart-item-qty">
                Qty: <?=$item->qty?>
            </div>
            <div class="col-md-2 cart-item-color">
                <?php if(is_object($model)):?>
                Color: <?=$model->color_title?>
                <?php endif;?>
            </div>
            <div class="col-md-4 cart-item-total text-right">
               Total: <?=Yii::$app->formatter->asCurrency($item->price * $item->qty )?>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="clearfix"></div>