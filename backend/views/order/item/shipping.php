<?php
?>
<div class="row">
    <div class="col-md-6">
        <div class="pull-left">Name:</div> 
        <div class="pull-right"><strong><?= $model->name?></strong></div>
    </div>    
    <div class="col-md-6">
        <div class="pull-left">Email:</div> 
        <div class="pull-right"><strong><?= $model->email?></strong></div> 
    </div>   
    <div class="col-md-6">   
        <div class="pull-left">Address:</div> 
        <div class="pull-right"><strong><?= $model->address?></strong></div> 
    </div>
    <div class="col-md-6">
        <div class="pull-left">City:</div> 
        <div class="pull-right"><strong><?= $model->city?></strong></div>
    </div>    
    <div class="col-md-6">
        <div class="pull-left">State:</div> 
        <div class="pull-right"><strong><?= $model->state?></strong></div>
    </div>    
    <div class="col-md-6">
        <div class="pull-left">Zip:</div> 
        <div class="pull-right"><strong><?= $model->zip ?></strong></div>
    </div>
</div>