<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
?>

<?php $form = ActiveForm::begin(); ?>
<?= $form->errorSummary([$model]) ?>
<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'name') ?>
        <?= $form->field($model, 'email') ?>
        <?= $form->field($model, 'address') ?>
        <?= $form->field($model, 'city') ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'state') ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'zip') ?>
    </div>
</div>
<div class="form-group text-right">
    <?= Html::submitButton(Yii::t('shiping', 'Update'), ['class' => 'btn btn-primary']) ?>
</div>
<?php ActiveForm::end(); ?>