<div class="box box-default">
    <div class="box-header with-border">
        <h4>Items: <strong><?=$model->totalCount?></strong></h4>
        <div class="col-md-1 box-tools pull-right text-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <?php foreach ($model->items as $item): ?>
            <?= $this->render('@backend/views/order/item/product', ['model' => $item->product, 'item' => $item]); ?>
        <?php endforeach; ?>
        <div class="col-md-12 text-right h4">
            Total: <strong><?= Yii::$app->formatter->asCurrency($model->totalPrice) ?></strong>
        </div>
    </div>
</div>