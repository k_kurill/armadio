<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use lajax\translatemanager\models\Language;
use yii\helpers\ArrayHelper;
use common\widgets\CKEditor;
use kartik\select2\Select2;
use common\modules\shop\models\Brands;
use backend\models\Product;
use kartik\color\ColorInput;

$languages = Language::findAll(['status' => 1]);

?>
<div class="clearfix"></div>
<div class="product-form">
    <?php $form = ActiveForm::begin(); ?>
    <div style="margin-top: 25px"></div>

        
        <?php if(!$model->isNewRecord):?>
        <div class="col-sm-12 box box-danger" style="padding-top: 10px">
            <div class="col-md-6">
                <?= $form->field($model, 'active')->checkbox(); ?>
            </div>
        </div>

        <?php endif ?>

        <div class="row">
            <div class="col-sm-6">
                <div class="box box-info" style="padding: 10px">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'price') ?>

                    <?= $form->field($model, 'sku_code')->textInput() ?>
                
                <?= $form->field($model, 'color')->widget(ColorInput::classname(), [
                    'options' => ['placeholder' => 'Select color ...'],
                ]);?>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="box box-info" style="padding: 10px">
                
                
                <?= $form->field($model, 'brand_id')->widget(Select2::className(),['options' => ['disabled' => $model->variant_of], 'data' => ArrayHelper::map(Brands::find()->all(), 'id', 'name')]) ?>
                <?php if(!$model->isNewRecord):?>
                <div class="form-group">
                <?php                        ?>
                <label>Category</label>
                    <?=\kartik\tree\TreeViewInput::widget([
                    'name' => 'categories',
                    'value' => \common\modules\shop\models\ProductCategory::selectCategories($model->id), // preselected values
                    'query' => \common\modules\shop\models\Categories::find()->addOrderBy('root, lft')->andWhere('lvl>0'),

                    'headingOptions' => ['label' => 'Categories'],
                    'rootOptions' => ['label'=>'<i class="fa fa-tree text-success"></i>'],
                    'fontAwesome' => true,
                    'asDropdown' => true,
                    'multiple' => false,
                    'options' => ['disabled' => $model->variant_of]
                    ]);?>
                </div>
                <?php endif;?>
                <?= $form->field($model, 'color_title')?>
                <?= $form->field($model, 'leather_type')?>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-info" style="padding: 10px">
                    <div class="col-md-12">
                    <?=$form->field($model, 'description')->widget(CKEditor::className(), []);?>
                    </div>
                    <div class="col-md-12">
                        <?=$form->field($model, 'leather')->widget(CKEditor::className(), []);?>
                    </div>
                    <div class="col-md-12">
                        <?=$form->field($model, 'measurements')->widget(CKEditor::className(), []);?>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-12">
                <div class="form-group text-right">
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('product', 'Create') : Yii::t('product', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>


</div>
<div class="clearfix"></div>
