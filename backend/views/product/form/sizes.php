<?php 
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\widgets\Pjax;

$js = <<<JS
        $('#sizes-form').on('pjax:end',   function() { $.pjax.reload({container: "sizes-form-wrapper"})});
JS;

//$this->registerJs($js);

?>
<div id="sizes-form-wrapper">
<?php Pjax::begin(['enablePushState'=>false,'id'=>'sizes-form']);?>

<div class="col-sm-12 box box-danger" style="padding-top: 10px">
<?php $formCount = ActiveForm::begin([
                    'options' => ['data-pjax' => true, 'style'=>'margin-top: 25px;margin-bottom: 25px'],
                    'layout'=>'inline',
                    'action'=>Url::to(['product/update', 'id'=>$model->id]),
            ]); ?>
    <div class="box-header with-border">
        <h3 class="box-title">Total remaining:</h3>
    </div>
    
    <div class="box-body">
        <?php if(empty($model->sizes)):?>
        <?=$formCount->field($model, 'count')->textInput() ?>
        <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
        <?php else:?>
        <?=$formCount->field($model, 'count')->textInput(['disabled'=>true]) ?>
        <?php endif?>
    </div>
    <div class="clearfix"></div>
<?php ActiveForm::end(); ?>
</div>

<div class="col-sm-12 box box-info" style="padding-top: 10px">
    <div class="box-header with-border">
        <h3 class="box-title">Sizes:</h3>
    </div>
    <div class="box-body">
<?php foreach ($model->sizes as $size):?>
    <?php $form = ActiveForm::begin([
                    'options' => ['data-pjax' => true, 'style'=>'margin-top: 25px;margin-bottom: 25px'],
                    'layout'=>'inline',
                    'action'=>Url::to(['product/update-size', 'id'=>$size->id]),
            ]); ?>
    <div class="col-md-4">
        <label>Size:</label>
        <?=$form->field($size, 'size')?>
    </div>
    <div class="col-md-4">
        <label>Remaining:</label>
        <?=$form->field($size, 'count')?>
    </div>
    <div class="col-md-4">
        <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', Url::to(['product/delete-size','id'=>$size->id]), ['class' => 'btn btn-danger']) ?>
    </div>
    <div class="clearfix"></div>
    <?php ActiveForm::end(); ?>
<?php endforeach; ?>
<?php 
    if(!isset($newsize)){
        $newsize = new backend\models\ProductSize;
    }
?>
<?php $form = ActiveForm::begin([
                    'options' => ['data-pjax' => true, 'style'=>'margin-top: 25px'],
                    'layout'=>'inline',
                    'action'=>Url::to(['product/create-size','id'=>$model->id]),
            ]); ?>
        <div class="col-md-4">
            <label>Size:</label>
            <?=$form->field($newsize, 'size')?>
        </div>
        <div class="col-md-4">
            <label>Remaining:</label>
            <?=$form->field($newsize, 'count')?>
        </div>
        <div class="col-md-4">
        <?= Html::submitButton('Create', ['class' => 'btn btn-success']) ?>
        </div>
<?php ActiveForm::end(); ?>
<div class="clearfix"></div>

</div>
</div>

<?php Pjax::end();?>
</div>