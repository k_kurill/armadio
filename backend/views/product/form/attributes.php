<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="clearfix"></div>
<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    
    <div class="form-group ">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('product', 'Create') : Yii::t('product', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>