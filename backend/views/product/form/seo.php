<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="clearfix"></div>
<div class="product-form">
<?php $form = ActiveForm::begin(); ?>
<div style="margin-top: 25px">
    <div class="col-sm-12 box box-success" style="padding-top: 10px">
        <?= $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'meta_keyword')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'meta_description')->textInput(['maxlength' => true]) ?>
    </div>
<div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? Yii::t('product', 'Create') : Yii::t('product', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>
</div>
<?php ActiveForm::end(); ?>
</div>