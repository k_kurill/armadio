<?php
use yii\bootstrap\Html;
use yii\helpers\Url;

use kartik\dropdown\DropdownX;

$css = <<<CSS
        .item-row{
            padding: 5px;
            border-bottom: 1px solid #eee;
            min-height: 30px;
        }
CSS;
$this->registerCss($css);

$varintsCount = count($model->variants);

$dropdown  = Html::beginTag('div', ['class'=>'dropdown', 'style'=>'display: inline-block;']);
$dropdown .= Html::button('Variants ('.$varintsCount.') <span class="caret"></span></button>', 
                          ['type'=>'button', 'class'=>'btn btn-default', 'data-toggle'=>'dropdown']);
$dropdown .= DropdownX::widget([
    'encodeLabels' => false,
    'items' => $model->getProductVariansForMenu(),
]); 
$dropdown .= Html::endTag('div');
?>

<div class="box box-solid box-default">
    <div class="box-header with-border">
        <h3 class="box-title"><?=Html::encode($model->name)?></h3>
        <span class="label label-default">
            <?=$dropdown?>
        </span>
        <div class="box-tools pull-right">
            <a href="<?=Url::to(['update','id'=>$model->id])?>" class="btn btn-box-tool">
            <i class="fa fa-edit"></i>
            </a>
            <?=Html::a('<i class="fa fa-trash"></i>', Url::to(['/product/delete','id'=>$model->id]), [
                'class'=>'btn btn-box-tool',
                'data' => [
                    'confirm' => 'Are you sure you want to delete the product?',
                    'method' => 'post',
                ]
            ])?>
            <a href="#" class="btn btn-box-tool"></a>
        </div><!-- /.box-tools -->
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-2">
                <?= Html::img($model->image->getUrl('150x'));?>
            </div>
            <div class="col-md-8">
                <div class="row item-row">
                    <div class="col-md-6">
                        ID: <strong class="pull-right"><?=$model->id?></strong>
                    </div>
                    <div class="col-md-6">
                        Stock: <strong class="pull-right"><?=$model->active?'In Stock':'Out of Stock' ?></strong>
                    </div>
                </div>
                <div class="row item-row">
                    <div class="col-md-6">
                        SKU: <strong class="pull-right"><?=$model->sku_code?></strong>
                    </div>
                    <div class="col-md-6">
                        Date (Published): <strong class="pull-right"><?=Yii::$app->formatter->asDate($model->published_at)?></strong>
                    </div>
                </div>
                
                <div class="row item-row">
                    <div class="col-md-6">
                        Artisan: <strong class="pull-right"><?= ($model->brand)?$model->brand->name:'(not set)'?></strong>
                    </div>
                    <div class="col-md-6">
                        Favorite: <strong class="pull-right">No</strong>
                    </div>
                </div>
                <div class="row item-row">
                    <div class="col-sm-2">
                        Categories: 
                    </div>
                    <div class="col-sm-4 text-right">
                        list of Categories: 
                    </div>
                    <div class="col-sm-2">
                        Tags: 
                    </div>
                    <div class="col-sm-4 text-right">
                        list of tags: 
                    </div>
                    
                </div>
            </div>
            <div class="col-md-2">
                <div class="h1"><?=Yii::$app->formatter->asCurrency($model->price)?></div>
            </div>
        </div>
    </div>
</div>
<!--['update', 'id' => $model->id]-->