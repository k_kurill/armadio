<?php
use yii\bootstrap\ActiveForm;
use common\widgets\CKEditor;
use yii\bootstrap\Html;

?>
<?php
$JS = <<<JS

JS;
$this->registerJs($JS, yii\web\View::POS_READY);

Yii::$app->session->set('product_translate', $model->language_id);

?>

<h2>Translation to <?=$model->language->name_ascii?></h2>
<?php yii\widgets\Pjax::begin(['enablePushState'=>false,'id'=>"pjax_translate{$model->id}{$model->language_id}"])?>


<?php $form = ActiveForm::begin([
                'id'=>"translate{$model->id}{$model->language_id}",
                'options' => ['data-pjax' => true],
                'action' => yii\helpers\Url::to(['product/update-translate', 'product_id' => $model->product_id, 'lng'=>$model->language_id]),
                ]); ?>

<?=\common\widgets\Alert::widget();?>

<?php if($model->errors):?>
<div class="alert alert-error">
    <?= $form->errorSummary([$model]) ?>
</div>
<?php endif;?>
<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'color_title') ?>
        <?= $form->field($model, 'leather_type') ?>
        <?= $form->field($model, 'name') ?>
        <?= $form->field($model, 'meta_title') ?>
        <?= $form->field($model, 'meta_keyword') ?>
        <?= $form->field($model, 'meta_description') ?>
    </div>
    <div class="col-md-12">
        <?= $form->field($model, 'short_description')->textarea() ?>
    </div>
    <div class="col-md-12">
        <label>Product Info</label>
        <?=CKEditor::widget([
            'id'=>"description{$model->id}",
            'name'=>"ProductI18nData[description]",
            'value'=>$model->description,
        ]);?>
    </div>
    <label>Leather</label>
    <div class="col-md-12">
        <?=CKEditor::widget([
            'id'=>"leather{$model->id}",
            'name'=>"ProductI18nData[leather]",
            'value'=>$model->leather,
        ]);?>
    </div>
    <div class="col-md-12">
        <label>Measurements</label>
        <?=CKEditor::widget([
            'id'=>"measurements{$model->id}",
            'name'=>"ProductI18nData[measurements]",
            'value'=>$model->measurements,
        ]);?>
    </div>
</div>
<br>
<div class="form-group text-right">
    <?= Html::submitButton(Yii::t('shiping', 'Update'), ['class' => 'btn btn-primary']) ?>
</div>
<?php ActiveForm::end(); ?>
<?php yii\widgets\Pjax::end()?>