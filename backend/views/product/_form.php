<?php
use kartik\tabs\TabsX;
use yii\bootstrap\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model backend\models\Product */

$submenu = $this->context->getLngSubitems($model->id);
?>
<?php
$script = <<< JS
    $(function() {
        //save the latest tab (http://stackoverflow.com/a/18845441)
        $('a[data-toggle="tab"]').on('click', function (e) {
            localStorage.setItem('lastTab', $(e.target).attr('href'));

        });
        //go to the latest tab, if it exists:
        var lastTab = localStorage.getItem('lastTab');

        if (lastTab) {
            $('a[href="'+lastTab+'"]').click();
        }
    });
JS;
$this->registerJs($script, yii\web\View::POS_END);
?>

<div class="row text-right">
    <div class="col-md-12">
    <?=Html::a('Delete', Url::to(['/product/delete','id'=>$model->id]), [
                'class'=>'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete the product?',
                    'method' => 'post',
                ]
            ])?>
    </div>
</div>

<?php
echo TabsX::widget([    
    'items' => [
        [
            'label' => 'Params',
            'content' => $this->render('form/params', ['model'=>$model]),
            'options' => ['id' => 'params-tab'],
        ],
        [
            'label' => 'SEO',
            'content' => $this->render('form/seo', ['model'=>$model]),
            'options' => ['id' => 'seo-tab'],
        ],
        [
            'label' => 'Images/Slider',
            'content' => $this->render('form/images', ['model'=>$model]),
            'options' => ['id' => 'images-tab'],
        ],
        [
            'label' => 'Sizes/Remaining',
            'content' => $this->render('form/sizes', ['model'=>$model]),
            'options' => ['id' => 'sizes-tab'],
        ],
        [
            'label' => 'Translation',
            'url'   => '#',
            'content'=> '',
            'items' => $submenu,
        ],
        [
            'label' => 'View on site',
            'url' => 'http://www.example.com',
            'linkOptions' => ['target' => '_blank']
        ],
    ],
]);
?>


