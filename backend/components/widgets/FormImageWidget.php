<?php
namespace backend\components\widgets;

use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\db\ActiveRecord;
use demi\cropper\Cropper;
use yii\web\JsExpression;
use yii\widgets\InputWidget;
use kartik\dropdown\DropdownX;
use demi\image\ImageUploaderBehavior;
use demi\image\FormImageWidget as BaseImageWidget;

class FormImageWidget extends BaseImageWidget{
    public $imageRules = [];
    public function run()
    {
        /* @var $model ActiveRecord|ImageUploaderBehavior */
        $model = $this->model;
        /* @var $behavior ImageUploaderBehavior */
        $behavior = $model->geImageBehavior();

        $wigetId = $this->id;
        /*$img_hint = '<div class="hint-block">';
        $img_hint .= 'Supported formats: ' . $behavior->getImageConfigParam('fileTypes') . '.
	Maximum file size: ' . ceil($behavior->getImageConfigParam('maxFileSize') / 1024 / 1024) . 'mb.';
        $img_hint .= '</div><!-- /.hint-block -->';*/
        $img_hint = '';
        $imageVal = $model->getAttribute($behavior->getImageConfigParam('imageAttribute'));
        if (!$model->isNewRecord && !empty($imageVal)) {
            $img_hint .= '<div id="' . $wigetId . '" class="row">';
            $img_hint .= $this->getOptions();
            $img_hint .= '<div class="col-md-12" style="height: 240px;">';
            $img_hint .= Html::img($this->imageSrc, ['class' => 'uploaded-image-preview', 'style'=>'max-width:260px;max-height:200px;']);
            // $img_hint .= '<div class="pull-left" style="margin-left: 5px;">';
            $img_hint .= '<div class="btn-group pull-left"  style="position: absolute; bottom: 0px; width: 200px; left: 30%;" role="group">';
            $img_hint .= Html::a('Delete <i class="glyphicon glyphicon-trash"></i>', '#',
                [
                    'onclick' => new JsExpression('
                        if (!confirm("Are you realy wont delete image?")) {
                            return false;
                        }

                        $.ajax({
                            type: "post",
                            cache: false,
                            url: "' . Url::to($this->deleteUrl) . '",
                            success: function() {
                                $("#' . $wigetId . '").remove();
                            }
                        });

                        return false;
                    '),
                    'class' => 'btn btn-danger',
                ]);

            if (!empty($this->cropUrl)) {
                Yii::$app->response->headers->add('Access-Control-Allow-Origin', '*');
                $pluginOptions = $this->cropPluginOptions;
                $validatorParams = $behavior->getImageConfigParam('imageValidatorParams');
                if (isset($validatorParams['minWidth'])) {
                    $pluginOptions['minCropBoxWidth'] = $validatorParams['minWidth'];
                }
                if (isset($validatorParams['minHeight'])) {
                    $pluginOptions['minCropBoxHeight'] = $validatorParams['minHeight'];
                }

                $img_hint .= Cropper::widget([
                    'modal' => true,
                    'cropUrl' => $this->cropUrl,
                    'image' => ImageUploaderBehavior::addPostfixToFile(str_replace('/backend', '', $model->getImageSrc()), '_original'),
                    'aspectRatio' => $behavior->getImageConfigParam('aspectRatio'),
                    'pluginOptions' => $pluginOptions,
                    'options' => ['style'=>'border-top-right-radius: 3px;border-bottom-right-radius: 3px;'],
                    'ajaxOptions' => [
                        'success' => new JsExpression(<<<JS
function(data) {
    // Refresh image src value to show new cropped image
    var img = $("#$wigetId img.uploaded-image-preview");
    img.attr("src", img.attr("src").replace(/\?.*/, '') + "?" + new Date().getTime());
    img.parents('.file-preview-frame').css('box-shadow', '1px 1px 5px 0 #a2958a');
}
JS
                            ),
                    ],
                ]);
            }
            $img_hint .= '</div><!-- /.btn-group -->';
            $img_hint .= '</div><!-- /.col-md-12 -->';
            $img_hint .= '</div><!-- /.row -->';
        }

        $imgAttr = $behavior->getImageConfigParam('imageAttribute');

        return $img_hint;
    }
    
    
    protected function getImageRole(){
        switch ($this->model->role){
            case 'image1':
                return '1st image';
            break;
            case 'image2':
                return '2nd image';
            break;
            case 'image3':
                return '3rd image';
            break;
            case 'image4':
                return '4th image';
            break;
            case 'main':
                return 'Main image';
            break;
             case 'cover':
                return 'Cover';
            break;
        }
        return 'In slider';
    }
    
    protected function getImageRoles(){
        $items = [
            ['label' => 'In slider', 'url' => Url::to(['update-image', 'id' => $this->model->id, 'role'=>'default'])],
            ['label' => 'Main image', 'url' => Url::to(['update-image', 'id' => $this->model->id, 'role'=>'main'])],
            ['label' => '1st image', 'url' => Url::to(['update-image', 'id' => $this->model->id, 'role'=>'image1'])],
            ['label' => '2nd image', 'url' => Url::to(['update-image', 'id' => $this->model->id, 'role'=>'image2'])],
            ['label' => '3rd image', 'url' => Url::to(['update-image', 'id' => $this->model->id, 'role'=>'image3'])],
            ['label' => '4th image', 'url' => Url::to(['update-image', 'id' => $this->model->id, 'role'=>'image4'])],
            
        ];
        return (!empty($this->imageRules))?$this->imageRules:$items;
    }


    protected function getOptions(){
        $text  = Html::beginTag('div', ['class'=>'dropdown', 'style'=>'display: inline-block;']);
        $text .= Html::button($this->getImageRole(), 
                          ['type'=>'button', 'class'=>'btn btn-default', 'data-toggle'=>'dropdown']);
        
        $text .= DropdownX::widget([
            'encodeLabels' => false,
            'items' => $this->getImageRoles(),
        ]); 
        $text .= Html::endTag('div');
        return $text;
    }
}
