<?php
namespace backend\models;

class Image extends \rico\yii2images\models\Image{
 /**
 * @inheritdoc
 */
use \rico\yii2images\ModuleTrait;    
    
public function behaviors()
{
    return [
        'imageUploaderBehavior' => [
            'class' => \demi\image\ImageUploaderBehavior::className(),
            'imageConfig' => [
                // Name of image attribute where the image will be stored
                'imageAttribute' => 'filePath',
                // Yii-alias to dir where will be stored subdirectories with images
                'savePathAlias' => '@frontend/web/images/store',
                // Yii-alias to root project dir, relative path to the image will exclude this part of the full path
                'rootPathAlias' => '@frontend/web',
                // Name of default image. Image placed to: webrooot/images/{noImageBaseName}
                // You must create all noimage files: noimage.jpg, medium_noimage.jpg, small_noimage.jpg, etc.
                'noImageBaseName' => 'noimage.jpg',
                // List of thumbnails sizes.
                // Format: [prefix=>max_width]
                // Thumbnails height calculated proportionally automatically
                // Prefix '' is special, it determines the max width of the main image
                'imageSizes' => [
                    '' => 1000,
                    'medium_' => 270,
                    'small_' => 70,
                    'my_custom_size' => 25,
                ],
                // This params will be passed to \yii\validators\ImageValidator
                'imageValidatorParams' => [
                    
                ],
                // Cropper config
                //'aspectRatio' => 4 / 3, // or 16/9(wide) or 1/1(square) or any other ratio. Null - free ratio
                // default config
                'imageRequire' => false,
                'fileTypes' => 'jpg,jpeg,gif,png',
                'maxFileSize' => 10485760, // 10mb
            ],
        ],
    ];
}
public function setMain($isMain = true){
        if($isMain){
            $this->isMain = 1;
        }else{
            $this->isMain = 0;
        }

    }
    
}