<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[ProductI18nData]].
 *
 * @see ProductI18nData
 */
class ProductI18nDataQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ProductI18nData[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ProductI18nData|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
