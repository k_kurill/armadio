<?php

namespace backend\models;

use common\modules\shop\models\Brands;
use common\modules\shop\models\ProductCategory;
use backend\models\ProductSize;
use Yii;
use yii\helpers\Url;
use backend\models\Image;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $name
 * @property string $main_image

 * @property string $meta_title
 * @property string $meta_keyword
 * @property string $meta_description
 * @property string $short_description
 * @property string $description
 */
class Product extends \yii\db\ActiveRecord
{

    private $_name, $_meta_title, $_meta_keyword, $_meta_description, 
            $_short_description, $_description, $_color_title, $_leather, 
            $_measurements, $_leather_type;

    /**
     * create_time, update_time to now()
     * crate_user_id, update_user_id to current login user id
     */
    public function behaviors()
    {
        return [
            'image' => [
                'class' => \rico\yii2images\behaviors\ImageBehave::className(),
            ],
        ];
    }

    public function init() {
        
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'sku_code'], 'required', 'on' => 'create'],
            [['name', 'sku_code', 'price', 'color_title', 'brand_id'], 'required', 'on' => ['update', 'default']],
            [['sku_code'], 'unique', 'targetAttribute' => ['sku_code']],
            [['price', 'variant_of','part_of', 'count'], 'number'],
            [['description', 'leather', 'measurements'], 'string'],
            [['active'], 'boolean'],
            //[['count'],'totalCount'],
            [['name', 'meta_title', 'meta_keyword', 'meta_description', 'color', 'color_title', 'leather_type'], 'string', 'max' => 255],
            [['short_description'], 'string', 'max' => 511],
            [['brand_id'],'integer'],
        ];
    }
    
    
    
    public function afterFind() {
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        if($this->variant_of){
            $this->brand_id = self::findOne($this->variant_of)->brand_id;
        }
        $this->updateCount();
        return parent::beforeSave($insert);
    }
    public function updCount($count){
        \Yii::$app->db->createCommand('update product set `count` = :remainind where id=:id',[':remainind'=>$count, ':id'=>$this->id])->execute();
    }
    
    public function updateCount(){
        if($this->sizes){
            $count = 0;
            foreach ($this->sizes as $size){
                $count += $size->count;
            }
            $this->updCount($count);
        }
    }
    
    public function afterSave($insert, $changedAttributes) {
        $this->createI18nRecord(); 
        $this->data->name = $this->_name;
        $this->data->meta_title = $this->_meta_title;
        $this->data->meta_keyword = $this->_meta_keyword;
        $this->data->meta_description = $this->_meta_description;
        $this->data->short_description = $this->_short_description;
        $this->data->description = $this->_description;
        $this->data->color_title = $this->_color_title;
        $this->data->measurements = $this->_measurements;
        $this->data->leather = $this->_leather;
        $this->data->leather_type = $this->_leather_type;
        
                
        $this->data->save(false);
        if($this->variant_of){
            $this->linkParentKategories();
        }
        $this->linkParentKategoriesToChild();
        parent::afterSave($insert, $changedAttributes);
    }
    
    protected function linkParentKategoriesToChild(){
        if($this->variant_of){
            return;
        }
        $prodCats = ProductCategory::findAll(['product_id'=>$this->id]);
        foreach($this->variants as $variant){
             ProductCategory::deleteAll(['product_id'=>$variant->id]);
             foreach ($prodCats as $cat){
                $newItem = new ProductCategory();
                $newItem->product_id = $variant->id;
                $newItem->category_id = $cat->category_id;
                $newItem->save();
            }
        }
    }
    
    public function linkParentKategories(){
        if(!$this->variant_of){
            return;
        }
        $prodCats = ProductCategory::findAll(['product_id'=>$this->variant_of]);
        ProductCategory::deleteAll(['product_id'=>$this->id]);
        foreach ($prodCats as $cat){
            $newItem = new ProductCategory();
            $newItem->product_id = $this->id;
            $newItem->category_id = $cat->category_id;
            $newItem->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('product', 'ID'),
            'name' => Yii::t('product', 'Name'),
            'meta_title' => Yii::t('product', 'Meta Title'),
            'meta_keyword' => Yii::t('product', 'Meta Keyword'),
            'meta_description' => Yii::t('product', 'Meta Description'),
            'short_description' => Yii::t('product', 'Short Description'),
            'description' => Yii::t('product', 'Product Info'),
            'brand_id' => Yii::t('product', 'Artisan'),
        ];
    }

    
    public function getName(){
        if($this->isNewRecord){
            return $this->_name;
        }
        if(!$this->data){
            $this->createI18nRecord();            
        }
        if(!$this->data->name){
            $this->data->name = "Product {$this->id}";
        }
        return $this->data->name;
    }
    public function setName($value){
        $this->_name = $value;
        if($this->isNewRecord){
            return;
        }
        $this->createI18nRecord();       
        $this->data->name = $value;
    }
    
    public function getColor_Title(){
        if($this->isNewRecord){
            return $this->_color_title;
        }
        if(!$this->data){
            $this->createI18nRecord();            
        }
        return $this->data->color_title;
    }
    public function setColor_Title($value){
        $this->_color_title = $value;
        if($this->isNewRecord){
            return;
        }
        $this->createI18nRecord();       
        $this->data->color_title = $value;
    }
    
    public function getLeather_Type(){
        if($this->isNewRecord){
            return $this->_leather_type;
        }
        if(!$this->data){
            $this->createI18nRecord();            
        }
        return $this->data->leather_type;
    }
    public function setLeather_type($value){
        $this->_leather_type = $value;
        if($this->isNewRecord){
            return;
        }
        $this->createI18nRecord();       
        $this->data->leather_type = $value;
    }
    
    
    public function getLeather(){
        if($this->isNewRecord){
            return $this->_leather;
        }
        if(!$this->data){
            $this->createI18nRecord();            
        }
        return $this->data->leather;
    }
    public function setLeather($value){
        $this->_leather = $value;
        if($this->isNewRecord){
            return;
        }
        $this->createI18nRecord();       
        $this->data->leather = $value;
    }
    
    public function getMeasurements(){
        if($this->isNewRecord){
            return $this->_measurements;
        }
        if(!$this->data){
            $this->createI18nRecord();            
        }
        return $this->data->measurements;
    }
    public function setMeasurements($value){
        $this->_measurements = $value;
        if($this->isNewRecord){
            return;
        }
        $this->createI18nRecord();       
        $this->data->measurements = $value;
    }
    
    public function getMeta_Title(){
        if($this->isNewRecord){
            return $this->_meta_title;
        }
        if(!$this->data){
        $this->createI18nRecord();      
        }
        return $this->data->meta_title;
    }
    public function setMeta_Title($value){
        $this->_meta_title = $value;
        if($this->isNewRecord){
            return;
        }
        $this->createI18nRecord();  
        $this->data->meta_title = $value;
    }
    
    public function getMeta_Keyword(){
        if($this->isNewRecord){
            return $this->_meta_keyword;
        }
        if(!$this->data){
            $this->createI18nRecord();            
        }
        return $this->data->meta_keyword;
    }
    public function setMeta_Keyword($value){
        $this->_meta_keyword = $value;
        if($this->isNewRecord){
            return;
        }
        $this->createI18nRecord();            
        $this->data->meta_keyword = $value;
    }
    
    public function getMeta_Description(){
        if($this->isNewRecord){
            return $this->_meta_description;
        }
        if(!$this->data){
            $this->createI18nRecord();            
        }
        return $this->data->meta_description;
    }
    public function setMeta_Description($value){
        $this->_meta_description = $value;
        if($this->isNewRecord){
            return;
        }
        $this->createI18nRecord(); 
        $this->data->meta_description = $value;
    }
    
    public function getShort_Description(){
        if($this->isNewRecord){
            return $this->_short_description;
        }
        if(!$this->data){
            $this->createI18nRecord();            
        }
        return $this->data->short_description;
    }
    public function setShort_Description($value){
        $this->_short_description = $value;
        if($this->isNewRecord){
            return;
        }
        $this->createI18nRecord();       
        $this->data->short_description = $value;
    }
    
    public function getDescription(){
        if($this->isNewRecord){
            return $this->_description;
        }
        if(!$this->data){
            $this->createI18nRecord();            
        }
        return $this->data->description;
    }
    public function setDescription($value){
        $this->_description = $value;
        if($this->isNewRecord){
            return;
        }
        $this->createI18nRecord();   
        $this->data->description = $value;
    }
    
    private function createI18nRecord($lng=null){
         if($this->getData($lng)->one()){
             return true;
         }else{
             ProductI18nData::createTranslate($this->id, $lng);
             $this->refresh();
         }
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getData($lng=null){
        return $this->hasOne(ProductI18nData::className(), ['product_id' => 'id'])->andFilterWhere(['language_id'=>$lng]);
    }
    
    
    public function getImages()
    {
        $finder = [
            'itemId' => $this->id,
            'modelName' => 'Product',
        ];
        $imageQuery = Image::find();
        $imageQuery->where($finder);
        $imageQuery->orderBy(['isMain' => SORT_DESC, 'id' => SORT_ASC]);

        $imageRecords = $imageQuery->all();

        return $imageRecords;
    }

    public function getBrand(){
        return $this->hasOne(Brands::className(),['id'=>'brand_id']);
    }


    public function getImage()
    {
        $finder = [
            'itemId' => $this->id,
            'modelName' => 'Product',
        ];
        $imageQuery = Image::find();
        $imageQuery->where($finder);
        $imageQuery->orderBy(['isMain' => SORT_DESC, 'id' => SORT_ASC]);

        $img = $imageQuery->one();
        if(!$img){
            return Yii::$app->getModule('yii2images')->getPlaceHolder();
        }
        return $img;
    }
    public function getProductVariansForMenu(){
        $items = [];
        foreach ($this->variants as $product) {
            $items[] = ['linkOptions'=>['data-pjax'=>0],'label' => $product->name, 'url' => Url::to(['product/update', 'id' => $product->id])];
        }
        $items[] = ['linkOptions'=>['data-pjax'=>0],'label' => 'Create...', 'url' => Url::to(['product/create', 'parent' => $this->id])];
        return $items;
    }
    
    
    public function getVariants(){
        return $this->hasMany(self::className(), ['variant_of' => 'id']);
    }

    public function getCategories(){
        return $this->hasMany(ProductCategory::className(), ['product_id' => 'id']);
    }

    public function setCategories($categories){
        if($this->variant_of){
            return;
        }
        ProductCategory::deleteAll(['product_id'=>$this->id]);
        foreach ($arrCategory = explode(',', $categories) as $category) {
            $productCategory = new ProductCategory();
            $productCategory->category_id = (int)$category;
            $productCategory->product_id = $this->id;
            $productCategory->save();
        }
    }
    public function getSizes(){
        return $this->hasMany(ProductSize::className(), ['product_id' => 'id']);
    }

}
