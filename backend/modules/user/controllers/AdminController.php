<?php
namespace backend\modules\user\controllers;
use common\models\User;
use dektrium\user\controllers\AdminController as BaseController;
use yii\web\NotFoundHttpException;

class AdminController extends BaseController{
    protected function findModel($id)
    {
        $user = User::find()->andWhere(['id'=>$id])->one();
        if ($user === null) {
            throw new NotFoundHttpException('The requested page does not exist');
        }

        return $user;
    }
}