<?php

namespace backend\modules\pagemanager\controllers;

use bupy7\pages\controllers\ManagerController;
use common\modules\pagemanager\models\PageSearch;
use common\modules\pagemanager\models\Page;
use Yii;
class PagemanagerController extends ManagerController {

    
    /**
     * Lists all Page models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->module->viewPath = '@backend/views/pagemanager';
        $languages_res = Page::find()->distinct('lng')->all();
        $languages = [];
        foreach ($languages_res as $languages_r){
            $languages[] = $languages_r->lng;
        }
        $searchModel = new PageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'languages'    => $languages,
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * Updates an existing Page model.
     * If update is successful, the browser will be redirected to the 'update' page.
     * @param integer|null $id
     * @return mixed
     */
    public function actionUpdate($id = null, $lng = null) {
        $this->module->viewPath = '@backend/views/pagemanager';
        $pagedata = Yii::$app->request->post('Page');
        if($lng!==null){
           
        }elseif(isset($pagedata['lng'])){
            $lng = $pagedata['lng'];
        }else{
            $lng = Yii::$app->language;
        }
        if ($id === null) {
            $model = new Page;
            $model->lng = $lng;
        } else {
            $model = $this->findModel($id, $lng);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $module = $this->module->className();
            Yii::$app->session->setFlash('success', $module::t('SAVE_SUCCESS'));
            return $this->redirect(['update', 'id' => $model->id, 'lng'=>$model->lng]);
        }
        return $this->render($id === null ? 'create' : 'update', [
                    'model' => $model,
        ]);
    }
    public function actionDelete($id)
    {

        if ($this->findModel($id,Yii::$app->request->get('lng'))->delete()) {
            Yii::$app->session->setFlash('success', Yii::t('Pages','DELETE_SUCCESS'));
        }
        return $this->redirect(['index']);
    }
    protected function findModel($id, $lng = null) {
        if (is_null($lng)) {
            $lng = \Yii::$app->language;
        }
        if (($model = Page::find()->andFilterWhere(['id' => $id, 'lng' => $lng])->one()) !== null) {
            return $model;
        }else{
            $model = new Page();
            $model->id = $id;
            $anowerModel = Page::find()->andFilterWhere(['id' => $id])->one();
            if($anowerModel){
                $model->attributes = $anowerModel->attributes;
            }
            $model->lng = $lng;
            return $model;
        }
        throw new NotFoundHttpException(Module::t('PAGE_NOT_FOUND'));
    }

}
