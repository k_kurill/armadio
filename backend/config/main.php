<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'backend',
    'name' => 'Armadio Fashion',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'user' => [
            // following line will restrict access to profile, recovery, registration and settings controllers from backend
            //'class'=>'backend\modules\user\Module',
            'as backend' => 'dektrium\user\filters\BackendFilter',
            'controllerMap'=>[
                'admin'=>[
                    'class'=>'backend\modules\user\controllers\AdminController'
                ],
            ],
            'admins' => ['admin'],

        ],
        'eav'=>[
            'class' => 'mirocow\eav\Module',
        ],
        'treemanager' =>  [
            'class' => '\kartik\tree\Module',
            // other module settings, refer detailed documentation
        ],
        'payment' => [
            'class' => 'achertovsky\paypal\Module',
            'controllerMap' => [
                'payment' => [
                    'class' => 'backend\controllers\PaymentController',
                ]
            ],
        ],
        'translatemanager' => [
            'class' => 'lajax\translatemanager\Module',
            'layout' => '@backend/views/layouts/main',
            'allowedIPs' => ['*'],
            'controllerMap' => [
                'language' => [
                    'class' => 'lajax\translatemanager\controllers\LanguageController',
                    'as access' => [
                        'class' => yii\filters\AccessControl::className(),
                        'rules' => [
                            [
                                'allow' => true,
                                //'actions' => ['list', 'change-status', 'optimizer', 'scan', 'translate', 'save', 'dialog', 'message', 'view', 'create', 'update', 'delete', 'delete-source'],
                                'roles' => ['edit_translate'],
                            ],
                            /*[
                                'allow' => true,
                                'actions'=>['translate'],
                                'roles'=>['@']
                            ]*/
                        ],
                    ]

                ]

            ],
        ],
        'pages' => [
            'class' => 'common\modules\pagemanager\Module',

            'viewPath' => '@backend/modules/pagemanager/views/manager',
            'pathToImages' => '@frontend/web/img/pages',
            'urlToImages' => '/img/pages',
            'pathToFiles' => '@frontend/web/files/pages',
            'urlToFiles' => '/files/pages',
            'uploadImage' => true,
            'uploadFile' => true,
            'addImage' => true,
            'addFile' => true,
        ],
        'menu' => [
            'class' => '\pceuropa\menu\Module',
            'controllerMap' => [
                'index' => [
                    'class' => '\pceuropa\menu\controllers\IndexController',
                    'as access' => [
                        'class' => yii\filters\AccessControl::className(),
                        'rules' => [
                            [
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                        ],
                    ],
                ],
                'default' => [
                    'class' => '\pceuropa\menu\controllers\DefaultController',
                    'as access' => [
                        'class' => yii\filters\AccessControl::className(),
                        'rules' => [
                            [
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'request' => [
            'baseUrl' => '/backend',
            'enableCsrfValidation' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'assetManager' => [
            'bundles' => [
                'dmstr\web\AdminLteAsset' => [
                    'skin' => 'skin-green',
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => '@backend/modules/user/views',
                    '@dektrium/rbac/views' => '@backend/modules/rbac/views',
                    '@lajax/translatemanager/views' => '@backend/modules/translatemanager/views',
                ],
            ],
        ],
    ],
    'params' => $params,
];
